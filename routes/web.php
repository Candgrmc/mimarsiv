<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/yonetici',function (){
	return view('admin.home');
});
Route::get('/company/{slug}','CompanyController@detail')->name('company_detail');
Route::group(['middleware' => ['checkUser']], function () {

    Route::post('image/update','ImageController@update');
    Route::post('update_credentials','UserController@update');

	Route::get('office/product/add','OfficeController@create');
	Route::post('office/product/store','OfficeController@store');
	Route::get('office/project/add','OfficeController@createProject');
	Route::post('office/project/store','OfficeController@store');
    Route::get('office/credentials','OfficeController@credentials');
    Route::post('office/update_credentials','OfficeController@updateCredentials');
    Route::get('/office/projects/{name}','ProjectsController@searchProjects');
    Route::get('/office/{slug}','OfficeController@show')->name('office_detail');
    Route::get('/office','OfficeController@index');


    Route::get('company/product/add','CompanyController@create');
    Route::post('company/product/store','CompanyController@store');
    Route::get('company/project/add','CompanyController@createProject');
    Route::post('company/product/store','CompanyController@store');
    Route::post('company/project/store','CompanyController@storeProject');
    Route::get('company/credentials','CompanyController@credentials');
    Route::post('company/update_credentials','CompanyController@updateCredentials');

    Route::get('/company','CompanyController@index');


	Route::get('/architect','ArchitectController@index');
	Route::get('/architect/credentials','ArchitectController@credentials');
	Route::get('/architect/{id}','ArchitectController@detail');

	Route::get('/student','StudentController@index');
	Route::get('/student/credentials','StudentController@credentials');
	Route::post('/student/update','StudentController@update')->name('student.update');
	Route::get('/architect','ArchitectController@index');

	Route::get('/consultant/add_reference/{id}','ConsultantController@add_reference');
    Route::get('/consultant/projects/{name}','ProjectsController@searchProjects');
    Route::get('/consultant/credentials','ConsultantController@credentials');
    Route::post('/consultant/update_credentials','ConsultantController@updateCredentials');
    Route::get('/consultant','ConsultantController@profile');


});
Route::get('/','HomeController@index');
Route::get('/products/{slug}','ProductsController@productsByBrand');
Route::get('/products','ProductsController@index');
Route::get('/product/{slug}','ProductsController@detail');
Route::get('/news/{slug}','NewsController@detail');
Route::get('/bulletin/{slug}','BulletinController@detail');
Route::get('/interviews','HomeController@index');
Route::get('/interviews/{slug}','InterviewController@detail');

Route::get('/artisans','ArtisanController@index');

Route::get('/artisan/product/add','ArtisanController@create');
Route::post('/artisan/product/store','ArtisanController@store');
Route::get('/artisan/credentials','ArtisanController@credentials');
Route::post('/artisan/update_credentials','ArtisanController@updateCredentials');
Route::get('/artisan_detail/{id}','ArtisanController@detail');
Route::get('/artisan/add_reference/{id}','ArtisanController@add_reference');
Route::get('/artisan','ArtisanController@profile');


Route::get('/projects','ProjectsController@index');
Route::get('/project_detail/{slug}','ProjectsController@detail');
Route::get('/brands','BrandController@index');
Route::get('/brand/{slug}','BrandController@detail');
Route::get('/consultants','ConsultantController@index');
Route::get('/consultant/{id}','ConsultantController@detail');
Route::get('/search/{q}','ProductsController@search');
Route::get('/category/{slug}','ProductsController@productsByCategory');
Route::get('/companies' , 'CompanyController@companies');

Route::get('/article/{slug}','ConsultantController@article');

Route::group(['prefix' => 'yonetici',  'middleware' => ['auth','admin']], function()
{
	Route::get('/', 'Admin\HomeController@index' );
	Route::resource('/sliders', 'Admin\SliderController' );
	Route::resource('/products', 'Admin\ProductController' );
	Route::get('/product/delete_image/{id}', 'Admin\ProductController@deleteImage' );
	Route::resource('/product_category', 'Admin\ProductCategoryController' );
	Route::resource('/products/events', 'Admin\ProductEventController' );
	Route::resource('/projects', 'Admin\ProjectController' );
	Route::resource('/project_category', 'Admin\ProjectCategoryController' );
	Route::resource('/project_type', 'Admin\ProjectTypeController' );
	Route::resource('/brands', 'Admin\BrandController' );
	Route::resource('/bulletins', 'Admin\BulletinController' );
	Route::resource('/certificates', 'Admin\CertificateController' );
	Route::resource('/companies', 'Admin\CompanyController' );
	Route::resource('/interviews', 'Admin\InterviewController' );
	Route::resource('/news', 'Admin\NewsController' );
	Route::resource('/articles', 'Admin\ArticleController' );
	Route::resource('/pages', 'Admin\PageController' );
	Route::resource('/partners', 'Admin\PartnerController' );
	Route::resource('/users', 'Admin\UserController' );
	Route::resource('/users/cv', 'Admin\UserCvController' );
	Route::resource('/users/type', 'Admin\UserTypeController' );
	Route::resource('/students', 'Admin\StudentController' );
	Route::resource('/architects', 'Admin\ArchitectController' );
	Route::resource('/artisans', 'Admin\ArtisanController' );
	Route::resource('/consultants', 'Admin\ConsultantController' );
	Route::resource('/investors', 'Admin\InvestorController' );
	Route::resource('/offices', 'Admin\OfficeController' );
	Route::get('/product_settings', 'Admin\ProductSettingsController@settings' );
	Route::post('/update_settings', 'Admin\ProductSettingsController@updateSettings' );
	Route::get('/project_settings', 'Admin\ProjectController@settings' );
	Route::post('/update_project_settings', 'Admin\ProjectController@updateSettings' );

	Route::get('/events','Admin\EventController@index');
	Route::get('/event/create','Admin\EventController@create');
	Route::get('/events/{id}/edit','Admin\EventController@edit');

	Route::get('/artisan_categories','Admin\ArtisanController@categories');
	Route::get('/artisan_category/create','Admin\ArtisanController@create_category');
	Route::post('/artisan_category/add','Admin\ArtisanController@add_category');
	Route::get('/artisan_category/{id}/edit','Admin\ArtisanController@edit_category');
	Route::post('/artisan_category/update/{id}','Admin\ArtisanController@update_category');
	Route::get('/artisan_category/delete/{id}','Admin\ArtisanController@delete_category');

	Route::get('/consultant_categories','Admin\ConsultantController@categories');
	Route::get('/consultant_category/create','Admin\ConsultantController@create_category');
	Route::post('/consultant_category/add','Admin\ConsultantController@add_category');
	Route::get('/consultant_category/{id}/edit','Admin\ConsultantController@edit_category');
	Route::post('/consultant_category/update/{id}','Admin\ConsultantController@update_category');
	Route::get('/consultant_category/delete/{id}','Admin\ConsultantController@delete_category');


	Route::get('/activate/{id}', 'Admin\UserController@activateUser' );
	Route::get('/deactivate/{id}', 'Admin\UserController@deActivateUser' );

	Route::get('/change_password/{id}', 'Admin\UserController@changeUserPassword' );
	Route::patch('/set_password/{id}', 'Admin\UserController@setUserPassword' );

	Route::get('/setFeatured/{id}','Admin\ProductController@setFeatured');
	Route::get('/deSetFeatured/{id}','Admin\ProductController@deSetFeatured');

	Route::get('/instagram','Admin\HomeController@instagram_index');
	Route::get('/delete_photo/{id}','Admin\HomeController@delete_instagram');

	Route::post('/save_photo','Admin\HomeController@instagram');
	Route::post('/push-concept','Admin\HomeController@pushConcept');

});

Route::get('like_product/{productID}','HomeController@likeProduct');

Route::get('like_project/{projectID}','HomeController@likeProject');
Route::get('unlike_product/{productID}','HomeController@dislikeProduct');
Route::get('unlike_project/{projectID}','HomeController@dislikeProject');
Route::post('update_product_pivot','HomeController@update_product_pivot');
Route::post('update_project_pivot','HomeController@update_project_pivot');



Route::get('language/{lang}', function ($lang) {
	return redirect()->back()->withCookie(cookie()->forever('language', $lang));
})->name('language');

Route::get('pages/{page}','HomeController@pages');
