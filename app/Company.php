<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
    	'user_id',
	    'name',
	    'slug',
	    'address',
	    'url',
	    'phone',
	    'isActive',
	    'banner',
	    'logo'
    ];

    public function artisan(){
    	return $this->hasOne('App\Artisan');
    }
	public function architect(){
		return $this->hasOne('App\Architect');
	}
	public function consultant(){
		return $this->hasOne('App\Consultant');
	}
	public function investor(){
		return $this->hasOne('App\Investor');
	}
	public function user(){
		return $this->belongsTo('App\User');
	}
	public function projects(){
		return $this->hasMany('App\Projects','company_id','id');
	}

	public function images(){
    	return $this->hasMany(CompanyGallery::class,'company_id');
	}
}
