<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
	protected $fillable = [
		'title',
		'slug',
		'category_id'
	];

	public function projects(){
		return $this->hasMany('App\Project');
	}
	public function category()
	{
		return $this->belongsTo(ProjectCategory::class, 'category_id');
	}
	public function hasSub(){
    $categories = self::all();

    foreach ($categories as $category) {
        if($category->category_id == $this->id){
          return true;
        }
    }

    return false;
  }

  public function subCategories(){
    $categories = self::all();
    $subCategories = [];
    foreach ($categories as $category) {
        if($category->category_id == $this->id){
          array_push($subCategories , $category);
        }
    }

    return collect($subCategories);

  }
}
