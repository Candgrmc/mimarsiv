<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
	    'email',
	    'password',
	    'user_type_id',
	    'bio',
	    'approved'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function references(){
        return $this->belongsToMany('App\Projects','colsultant_reference');
    }
    public function Areferences(){
		return $this->belongsToMany('App\Projects','artisan_projects');
	}
    public function cv()
    {
    	return $this->hasOne('App\UserCv');
    }
    public function userType()
    {
    	return $this->belongsTo('App\UserType');
    }
	public function articles(){
    	return $this->hasMany('App\Article');
	}
    public function events(){
        return $this->hasMany('App\Event');
    }
	public function bulletins(){
		return $this->hasMany('App\Bulletin');
	}
	public function company(){
		return $this->hasOne('App\Company');
	}
	public function interviews(){
		return $this->hasMany('App\Interview');
	}
	public function news(){
		return $this->hasMany('App\News');
	}
	public function offices(){
		return $this->hasMany('App\Office');
	}
	public function products(){
		return $this->hasMany('App\Product');
	}
    public function projects(){
        return $this->hasMany('App\Projects');
    }
	public function socialContents(){
		return $this->hasMany('App\SocialContent');
	}
	public function selectedProducts(){
	    return $this->belongsToMany('App\Product','user_selected_products')->withPivot('note');
    }

    public function likedProjects(){
	    return $this->belongsToMany('App\Projects','user_liked_projects','user_id','projects_id')->withPivot('note');
    }
    public function likedProducts(){
      return $this->belongsToMany('App\Product','user_likes','user_id','product_id')->withPivot('note');
    }

}
