<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	protected $fillable = [
		'user_id',
		'title',
		'slug',
		'content',
		'image',
		'keywords',
		'meta_description',
		'isFeatured',
		'isActive'
	];

	public function user(){
		return $this->belongsTo('App\User');
	}
}
