<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model
{
    protected $fillable = [
    	'user_id',
	    'product_id',
	    'body',
	    'requester_ip',
	    'email'
    ];
	public function user(){
		return $this->belongsTo('App\User');
	}
	public function product(){
		return $this->belongsTo('App\Product');
	}
}
