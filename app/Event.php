<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
    	'name',
	    'slug',
	    'start_date',
	    'end_date'
    ];

    public function architectEvents(){
    	return $this->hasMany('App\ArchitectSelectedEvents');
    }
	public function productEvent(){
		return $this->hasMany('App\ProductEvent');
	}

	public function users(){
	    return $this->belongsToMany('App\User');
    }
}
