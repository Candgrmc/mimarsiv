<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtisanCategory extends Model
{
	protected $fillable = [
		'title',
		'slug'
	];

	public function artisans(){
		return $this->hasMany(Artisan::class);
	}
}
