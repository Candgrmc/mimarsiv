<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialContent extends Model
{
    protected $fillable = [
    	'share_id',
	    'user_id',
	    'imagePath',
	    'isActive',
	    'isFeatured'
    ];

    public function share(){
    	return $this->belongsTo('App\Share');
    }
	public function user(){
		return $this->belongsTo('App\User');
	}
}
