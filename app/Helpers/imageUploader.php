<?php


namespace App\Helpers;


use Illuminate\Http\Request;
use Cocur\Slugify\Slugify;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class imageUploader
{
	public static function storeImage(Request $request)
	{
		$randomString = str_random(8);
		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');
		$hashedName = md5($randomString.'-'.Carbon::now()->format('d-m-Y'));

		$image = $request->file('image');
		$filename = $hashedName. '.'. $image->getClientOriginalExtension();
		$filetype = $image->getMimeType();
		if($filetype=='image/gif' || $filetype =='image/jpeg' || $filetype =='image/png' || $filetype == 'image/bmp' || $filetype == 'application/pdf') {
			//Begin upload
			$disk = Storage::disk('public');
			$disk->put($filename, fopen($image, 'r+'));
			return $filename;
		} else {
			return false;
		}
	}
    public static function storeFile($file)
    {
        $randomString = str_random(8);
        $slugify = new Slugify();
        $slugify->activateRuleset('turkish')->addRule('_', '2');
        $hashedName = md5($randomString.'-'.Carbon::now()->format('d-m-Y'));

        $image = $file;
        $filename = $hashedName. '.'. $image->getClientOriginalExtension();
        $filetype = $image->getMimeType();
        if($filetype=='image/gif' || $filetype =='image/jpeg' || $filetype =='image/png' || $filetype == 'image/bmp' || $filetype == 'application/pdf') {
            //Begin upload
            $disk = Storage::disk('public');
            $disk->put($filename, fopen($image, 'r+'));
            return $filename;
        } else {
            return false;
        }
    }
	public static function multiImage(array $images)
	{
		$names = [];
		$i = 0;
		foreach ($images as $image)
		{
			$randomString = str_random(8);
			$slugify = new Slugify();
			$slugify->activateRuleset('turkish')->addRule('_', '2');
			$hashedName = md5($randomString.'-'.Carbon::now()->format('d-m-Y'));

			$filename = $hashedName. '.'. $image->getClientOriginalExtension();
			$filetype = $image->getMimeType();
			if($filetype=='image/gif' || $filetype =='image/jpeg' || $filetype =='image/png' || $filetype == 'image/bmp' || $filetype == 'application/pdf') {
				//Begin upload
				$disk = Storage::disk('public');
				$disk->put($filename, fopen($image, 'r+'));
				$names[$i] = $filename;

			} else {
				return false;
			}
			$i++;

		}
		return $names;
	}

}