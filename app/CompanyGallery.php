<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyGallery extends Model
{
	protected $fillable = [
		'company_id',
		'path'
	];

	public function company(){
		return $this->belongsTo(Company::class,'company_id');
	}
}
