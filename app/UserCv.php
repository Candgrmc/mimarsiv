<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCv extends Model
{
    protected $fillable = [
    	'user_id',
	    'file_path'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
