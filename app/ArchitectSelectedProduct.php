<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchitectSelectedProduct extends Model
{
	protected $fillable = [
		'architect_id',
		'product_id'
	];

	public function architect(){
		return $this->belongsTo('App\Architect');
	}
	public function product(){
		return $this->belongsTo('App\Product');
	}
}
