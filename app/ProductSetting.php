<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSetting extends Model
{
    protected $fillable = ['id','price'];
}
