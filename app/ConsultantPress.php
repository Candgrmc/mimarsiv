<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsultantPress extends Model
{
    protected $fillable = [
    	'consultant_id',
	    'title',
	    'slug',
	    'content',
	    'keywords'
    ];

    public function consultant(){
    	return $this->belongsTo('App\Consultant');
    }
}
