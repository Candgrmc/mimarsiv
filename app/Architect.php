<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Architect extends Model
{
    protected $fillable = [
    	'user_id',
    	'company_id'
    ];

    public function user (){
    	return $this->belongsTo('App\User');
    }
    public function company(){
    	return $this->hasOne('App\Company','id','company_id');
    }
	 public function events(){
    	return $this->hasMany('App\ArchitectSelectedEvent');
	 }
	public function products(){
		return $this->hasMany('App\ArchitectSelectedProduct');
	}
	public function projects(){
		return $this->hasMany('App\ArchitectSelectedProject');
	}
}
