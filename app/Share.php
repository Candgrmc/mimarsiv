<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    protected $fillable = [
    	'hashtag',
	    'description',
	    'isActive',
	    'closed_date'
    ];

    public function socialContent(){
    	return $this->hasMany('App\SocialContent');
    }
}
