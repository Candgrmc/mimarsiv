<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductEvent extends Model
{
	protected $fillable = [
		'product_id',
		'event_id'
	];

	public function product(){
		return $this->belongsTo('App\Product');
	}
	public function event(){
		return $this->belongsTo('App\Event');
	}
}
