<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultant extends Model
{
	protected $fillable = [
		'user_id',
		'company_id'
	];

	public function user (){
		return $this->belongsTo('App\User');
	}
	public function company(){
		return $this->hasOne('App\Company','id','company_id');
	}
	public function press(){
		return $this->hasMany('App\ConsultantPress');
	}
	public function references(){
		return $this->belongsToMany('App\Projects','consultant_reference','project_id','id');
	}
	public function projects(){
		return $this->hasMany('App\ProjectConsultant');
	}
}
