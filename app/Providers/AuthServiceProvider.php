<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

	    $user = Auth::user();

	    // Auth gates for: Admin Access
	    Gate::define('admin_access', function ($user) {
		    return in_array($user->user_type_id, [99]);
	    });
        Gate::define('student_access', function ($user) {
            return in_array($user->user_type_id, [1]);
        });
	    // Auth gates for: Architect Access
	    Gate::define('architect_access', function ($user) {
		    return in_array($user->user_type_id, [2]);
	    });
        Gate::define('consultant_access', function ($user) {
            return in_array($user->user_type_id, [4]);
        });
        Gate::define('artisan_access', function ($user) {
            return in_array($user->user_type_id, [5]);
        });
	    Gate::define('office_access', function ($user) {
		    return in_array($user->user_type_id, [6]);
	    });
        Gate::define('company_access', function ($user) {
            return in_array($user->user_type_id, [7]);
        });




    }
}
