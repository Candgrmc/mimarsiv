<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    protected $fillable = [
    	'project_type_id',
	    'project_category_id',
	    'company_id',
	    'consultant_id',
	    'investor_id',
	    'name',
	    'slug',
	    'story',
	    'year',
	    'country',
	    'image'
    ];
    public function user(){
      return $this->belongsTo('App\User');
    }
    public function type(){
    	return $this->belongsTo('App\ProjectType');
    }
    public function category(){
    	return $this->belongsTo('App\ProjectCategory','project_category_id','id');
    }
    public function company(){
    	return $this->belongsTo('App\Office','company_id');
    }
    public function consultants(){
    	return $this->belongsToMany('App\User','colsultant_reference');
    }

    public function artisans(){
        return $this->belongsToMany('App\User','artisan_projects','user_id','id');
    }

	public function investors(){
		return $this->hasOne('App\Investor','id','investor_id');
	}
	public function likedUser(){
	    return $this->belongsToMany('App\User','user_project_likes');
    }
    public function products(){
        return $this->belongsToMany('App\Product','project_products');
    }
    public function certificates(){
        return $this->hasMany('App\Certificate','project_certificates');
    }
}
