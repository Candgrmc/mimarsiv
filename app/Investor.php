<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
	protected $fillable = [
		'user_id',
		'company_id'
	];

	public function user (){
		return $this->belongsTo('App\User');
	}
	public function company(){
		return $this->hasOne('App\Company','id','company_id');
	}
	public function projects(){
		return $this->hasMany('App\Project');
	}
}
