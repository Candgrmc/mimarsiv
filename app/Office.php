<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Office extends Model
{
    protected $fillable = [
    	'user_id',
	    'name',
	    'slug',
	    'address',
	    'url',
	    'phone',
	    'isActive'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
