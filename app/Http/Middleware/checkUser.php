<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;


class checkUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

    	$user = Auth::user();

    	if( Gate::allows('admin_access') || $user->approved == 1){
		    return $next($request);
	    } else {
    		abort(402,'Unauthorized Action');
	    }

    }
}
