<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Office;
use App\Prize;
use App\ProjectGallery;
use App\ProjectProduct;
use App\Projects;
use App\Certificate;
use App\Company;
use App\ProductCategory;
use App\User;
use App\Investor;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cocur\Slugify\Slugify;
use App\Product;
use App\Helpers\imageUploader;
use App\ProductGallery;
use App\ProductCertificate;
use App\ProjectCategory;
use Illuminate\Support\Facades\Input;

class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('office_access')) {
            return abort(404);
        }



        $company = Office::where('user_id',Auth::user()->getAuthIdentifier())->first();
        return view('office.index',compact('company'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Office::where('user_id',Auth::user()->getAuthIdentifier())->first();
        $categories = ProductCategory::all();
        $certificates = Certificate::all();
        $brands = Brand::all();


        return view('office.create_product',compact('company','categories','certificates','brands'));
    }
    public function createProject()
    {
        $company = Company::where('user_id',Auth::user()->getAuthIdentifier())->first();
        $categories = ProjectCategory::all();
        $certificates = Certificate::all();
        $brands = Brand::all();
        $investors = Company::get();
        $products = Product::all();

        return view('office.create_project',compact('company','categories','certificates','brands','investors','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->products){
            $products = explode(',',$request->products);
            $productIDS = [];
            foreach ($products  as $product){
                if($product && $product != null){
                    $p = explode('-',$product);
                    array_push($productIDS , $p[1]);
                }
            }
        }


        $slugify = new Slugify();
        $slugify->activateRuleset('turkish')->addRule('_', '2');

        $user = Auth::user()->getAuthIdentifier();
        $image = $request->file('image');
        $company = Office::where('user_id',$user)->first();


        $imageStatus = imageUploader::multiImage($image);


        if($imageStatus != false){
            $gallery = $imageStatus;
        }

        if($request->brand_id){
            $brand= $request->brand_id;
        }else{

            if($request->brand_name){
                $brand = new Brand();
                $brand->name = $request->brand_name;
                if(Input::hasFile('brand_img')){
                    if($image){
                        $image = imageUploader::storeFile(Input::file('brand_img'));
                        $brand->image = '/uploads/'.$image;
                    }
                }
                $brand->save();
                $brand = $brand->id;
            }
        }


        $project = new Projects();
        $project->name = $request->title;

        $project->user_id = $user;
        $project->company_id = $company->id;
        $project->project_category_id = intval($request->category);
        $project->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
        $project->image = '/uploads/'.$gallery[0];
        $project->story = $request->story;
        $project->catalogue = $request->catalogue;
        $project->video = $request->video;
        $project->overview = $request->overview;
        $project->bim = $request->bim;

        $project->country = $request->country;
        $project->year = $request->year;
        $project->image = 'uploads/'.$gallery[0];

        $project->price = $request->price_start.'-'.$request->price_end;
        $project->tag = $request->tag;
        $project->featured = 0;
        $project->save();

        foreach ($productIDS as $productID){
            $pp = new ProjectProduct();
            $pp->projects_id = $project->id;
            $pp->product_id = $productID;
            $pp->save();
        }

        if(Input::hasFile('certificates')){
            $certificates = $request->file('certificates');

            $certificates = imageUploader::multiImage($certificates);

        }
        if(Input::hasFile('prizes')){
            $prizes = $request->file('prizes');

            $prizes = imageUploader::multiImage($prizes);

        }
        if(Input::hasFile('legals')){
            $legals = $request->file('legals');

            $legals = imageUploader::multiImage($legals);

        }





        foreach ($gallery as $item){
            $productGallery = new ProjectGallery();
            $productGallery->project_id = $project->id;
            $productGallery->image = '/uploads/'.$item;
            $productGallery->save();
        }

        if(isset($certificates)){
            $count = 1;
            foreach ($certificates as $item2){
                $certificate = new Certificate();
                $certificate->name = 'Sertifika-'.$count;
                $certificate->slug = $slugify->slugify($project->name).'-'.'Sertifika-'.$count;
                $certificate->img = '/uploads/'.$item2;
                if($certificate->save()){
                    $productCertificate = new ProjectCertificate();
                    $productCertificate->project_id = $project->id;
                    $productCertificate->certificate_id = $certificate->id;
                    $productCertificate->save();
                }
                $count+=1;

            }
        }




        return redirect()->to('/office')->with('success', 'Ürün başarıyla eklenmiştir.');
    }

    public function storeProject(Request $request)
    {
        $company = Office::where('user_id',Auth::user()->getAuthIdentifier())->first();
        $slugify = new Slugify();
        $slugify->activateRuleset('turkish')->addRule('_', '2');

        $user = Auth::user()->getAuthIdentifier();
        $image = $request->file('image');

        $imageStatus = imageUploader::multiImage($image);

        if($imageStatus != false){
            $gallery = $imageStatus;
        }

        $project = new Projects();
        $project->name = $request->title;
        $project->project_category_id = $request->category;
        $project->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
        $project->story = $request->story;
        $project->company_id = $company->id;
        $project->country = $request->country;
        $project->year = $request->start_year.'-'.$request->end_year;
        $project->image = 'uploads/'.$gallery[0];


        $project->save();

        //$productID = Product::where('image','/storage/'.$gallery[0])->select('id')->first();


        foreach ($gallery as $item){
            $productGallery = new ProjectGallery();
            $productGallery->project_id = $project->id;
            $productGallery->image = 'uploads/'.$item;
            $productGallery->save();
        }




        return back()->with('success', 'Proje başarıyla eklenmiştir.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $office = Office::whereSlug($slug)->first();
        $user = User::find($office->user_id);

        return view('office.detail',compact('office','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
    public function credentials(){
        $office = Office::where('user_id',request()->user()->id)->first();

        return view('office.credentials',compact('office'));
    }
    public function updateCredentials(Request $req){
        try{
            $user = User::find($req->user()->id);
            $user->name = $req->name;
            $user->phone = $req->phone;
            if($req->password  && $req->password == $req->password_repeat){
                $user->password = bcrypt($req->password);
            }
            $user->save();
            $office = Office::where('user_id',$user->id)->first();
            $office->url = $req->url;
            $office->save();

            $req->session()->flash('success','Başarıyla Güncellendi');
            return redirect()->back();

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }
}
