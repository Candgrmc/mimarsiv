<?php

namespace App\Http\Controllers;

use App\Company;
use App\Architect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;


class ArchitectController extends Controller
{


    public function index()
    {

        if (! Gate::allows('architect_access')) {
            return abort(404);
        }
        $company = Company::where('user_id',request()->user()->id)->first();


        return view('architects.index',compact('company'));
    }
    public function credentials()
    {

        if (! Gate::allows('architect_access')) {
            return abort(404);
        }
        $architect = Architect::where('user_id',request()->user()->id)->first();

        return view('architects.credentials',compact('architect'));
    }
    public function update(Request $req){
        if (! Gate::allows('student_access')) {
            return abort(404);
        }
        try{
            $student = User::find($req->user()->id);
            $student->name = $req->name;
            if($student->password && $student->password == $student->password_repeat){
                $student->password = bcrypt($student->password);
            }
            $student->phone = $req->phone;
            $student->save();
            $req->session()->flash('success','Bilgileriniz başarıyla güncellendi');
            return redirect()->back();
        }catch (\Exception $e){
            return $e->getMessage();
        }



    }
}
