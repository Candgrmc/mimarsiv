<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Company;
use App\Legal;
use App\Product;
use App\ProductCategory;
use App\ProductGallery;
use App\ProjectProduct;
use App\User;
use App\Prize;
use App\Slider;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
	    $products = Product::all();
	    $categories = ProductCategory::all();
      $sliders = Slider::get();
	    return view('products.index',compact('products','categories','sliders'));
    }

    public function detail ($slug)
    {
	    $product = Product::where('slug',$slug)->first();
	    $company = Company::where('user_id',$product->user_id)->first();
	    $images = ProductGallery::where('product_id',$product->id)->get();
	    $legals = Legal::where('product_id',$product->id)->get();
	    $projects = $product->projects;
	    $brand = Brand::find($product->brand_id);

	    return view('products.detail',compact('product','company','images','projects','legals','brand'));
    }

    public static function getCompanyProducts($id)
    {


    	$product = Product::where('user_id',$id)->get();

    	return $product;
    }

    public function productsByBrand(Request $req){
      $sliders = Slider::get();
        $brand = Brand::where('slug',$req->slug)->first();
        $products = Product::where('brand_id',$brand->id)->get();
        $categories = ProductCategory::all();

        return view('products.index',compact('products','categories','sliders'));
    }

    public function search(Request $req){

        $products = Product::where('name','like','%'.$req->q.'%')->get();
        $categories = ProductCategory::all();

        return view('products.index',compact('products','categories'));
    }
    public function productsByCategory(Request $req){
        $sliders = Slider::get();
        $category = ProductCategory::where('slug',$req->slug)->first();
        $products = Product::where('product_category_id',$category->id)->get();
        $categories = ProductCategory::all();

        return view('products.index',compact('products','categories','sliders'));
    }


}
