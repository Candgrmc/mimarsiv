<?php

namespace App\Http\Controllers;

use App\Artisan;
use App\Slider;
use App\ArtisanCategory;
use App\Brand;
use App\Legal;
use App\ProductSetting;
use App\CompanyGallery;
use App\Projects;
use App\Certificate;
use App\Company;
use App\ProductCategory;
use App\User;
use App\Investor;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cocur\Slugify\Slugify;
use App\Product;
use App\Helpers\imageUploader;
use App\ProductGallery;
use App\ProductCertificate;
use App\ProjectCategory;
use App\ProjectProduct;
use App\Prize;
use Illuminate\Support\Facades\Input;

class ArtisanController extends Controller
{
    public function index(){
    	$artisans = User::where('user_type_id',5)->get();
    	$sliders = Slider::all();
    	$categories = ArtisanCategory::all();
		return view('artisans.index',compact('artisans','sliders','categories'));

    }

    public function detail($id){


    	$artisan = User::find($id);

    	$company = Artisan::where('user_id',$artisan->id)->first();



    	return view('artisans.detail',compact('company','artisan'));


    }

    public function profile(){
        if (! \Gate::allows('artisan_access')) {
            return abort(404);
        }
        $company = Artisan::where('user_id',request()->user()->id)->first();


        return view('artisans.profile',compact('company'));

    }

    public function create(Artisan $artisan)
    {

        $categories = ProductCategory::all();
        $certificates = Certificate::all();
        $brands = Brand::all();

        return view('artisans.create_product',compact('company','categories','certificates','brands'));
    }

    public function store(Request $request)
    {
        $certificates = null;
        $prizes = null;
        $legals = null;

        $slugify = new Slugify();
        $slugify->activateRuleset('turkish')->addRule('_', '2');

        $user = Auth::user()->getAuthIdentifier();
        $image = $request->file('image');


        $imageStatus = imageUploader::multiImage($image);
        $settings = ProductSetting::first();
        $startPrice= $request->start_price;
        $endPrice= $request->end_price;
        if($settings){
            if($request->start_price < $settings->start_price){
                $startPrice = $settings->start_price;
            }
            if($request->end_price > $settings->end_price){
                $endPrice = $settings->end_price;
            }
        }

        if($imageStatus != false){
            $gallery = $imageStatus;
        }

        $product = new Product();
        $product->name = $request->title;
        $product->user_id = $user;
        $product->product_category_id = intval($request->category);
        $product->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
        $product->color = $request->color;
        $product->image = '/uploads/'.$gallery[0];
        $product->brand_id = $request->brand;
        $product->video = $request->video;
        $product->overview = $request->overview;
        if(Input::file('bim')){
            $bim = '/uploads/'.imageUploader::storeFile($request->bim);
            $product->bim = $bim;
        }
        if(Input::file('catalogue')){
            $catalogue = '/uploads/'.imageUploader::storeFile($request->catalogue);
            $product->catalogue = $catalogue;
        }

        if($request->brand_id){
            $product->brand_id = $request->brand_id;
        }else{
            $image = imageUploader::storeFile(Input::file('brand_img'));
            $brand = new Brand();
            $brand->name = $request->brand_name;
            if($image){
                $brand->image = '/uploads/'.$image;
            }
            $brand->save();

        }

        $product->price = $startPrice.'-'.$endPrice;
        $product->featured = 0;
        if($product->save() && $request->project_id){
            $projectProduct = new ProjectProduct();
            $projectProduct->product_id = $product->id;
            $projectProduct->projects_id = $request->project_id;
            $projectProduct->save();
        }

        if(Input::hasFile('certificates')){
            $certificates = $request->file('certificates');

            $certificates = imageUploader::multiImage($certificates);

        }
        if(Input::hasFile('prizes')){
            $prizes = $request->file('prizes');

            $prizes = imageUploader::multiImage($prizes);

        }
        if(Input::hasFile('legals')){
            $legals = $request->file('legals');

            $legals = imageUploader::multiImage($legals);

        }





        foreach ($gallery as $item){
            $productGallery = new ProductGallery();
            $productGallery->product_id = $product->id;
            $productGallery->image = '/uploads/'.$item;
            $productGallery->save();
        }

        if($certificates){
            $count = 1;
            foreach ($certificates as $item2){
                $certificate = new Certificate();
                $certificate->name = 'Sertifika-'.$count;
                $certificate->slug = $slugify->slugify($product->name).'-'.'Sertifika-'.$count;
                $certificate->img = '/uploads/'.$item2;
                if($certificate->save()){
                    $productCertificate = new ProductCertificate();
                    $productCertificate->product_id = $product->id;
                    $productCertificate->certificate_id = $certificate->id;
                    $productCertificate->save();
                }
                $count+=1;

            }
        }

        if($prizes != null){

            foreach ($prizes as $prize){
                $mPrize = new Prize();
                $mPrize->name = 'Prize-'.$count;

                $mPrize->img = '/uploads/'.$prize;
                $mPrize->product_id = $product->id;
                $mPrize->save();


            }
        }
        if($legals != null){

            foreach ($legals as $legalimg){
                $legal = new Legal();
                $legal->name = 'Legal-'.time();
                $legal->slug = 'Legal-'.time();
                $legal->path = '/uploads/'.$legalimg;
                $legal->product_id = $product->id;
                $legal->save();


            }
        }


        return back()->with('success', 'Ürün başarıyla eklenmiştir.');
    }


    public function credentials(){
        $company = Artisan::where('user_id',request()->user()->id)->first();

        return view('artisans.credentials',compact('company'));
    }
    public function updateCredentials(Request $req){
        try{

            $user = User::find($req->user()->id);
            $artisan = Artisan::where('user_id',$user->id)->first();

            $user->name = $req->name;
            $user->phone = $req->phone;
            if($req->password  && $req->password == $req->password_repeat){
                $user->password = bcrypt($req->password);
            }
            $user->save();

            $artisan->description = $req->description;
            $artisan->company_url = $req->url;
            $artisan->save();
            if($req->image){
              $image = $req->file('image');

              $imageStatus = imageUploader::multiImage($image);



              if($imageStatus != false){
                  $gallery = $imageStatus;
              }
              foreach ($gallery as $item){
                  $companyGallery = new CompanyGallery();
                  $companyGallery->company_id = $user->id;
                  $companyGallery->path = '/uploads/'.$item;
                  $companyGallery->save();
              }
            }

            $req->session()->flash('success','Başarıyla Güncellendi');
            return redirect()->back();

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function add_reference(Request $req){

            $req->user()->Areferences()->sync($req->id,false);
            return redirect()->back();
    }
}
