<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Bulletin;
use App\News;
use App\Page;
use App\Partner;
use App\Product;
use App\ProductCategory;
use App\ProjectCategory;
use App\Projects;
use App\ProjectType;
use App\Slider;
use App\User;
use App\Interview;
use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $sliders = DB::table('sliders')
		    ->orderBy('order', 'asc')
		    ->get();

	    $products = Product::where('featured',1)->inRandomOrder()->take(3)->get();
	    $news = News::where('isActive',1)->where('isFeatured',1)->inRandomOrder()->take(3)->get();
	    $partners = Partner::take(6)
    ->get();
	    $brands = Brand::inRandomOrder()
    ->take(4)
    ->get();
	    $interviews = Interview::inRandomOrder()->take(3)->get();
	    $bulletins = Bulletin::take(1)->get();
	    $projects = Projects::inRandomOrder()->take(3)->get();
      $article = Article::where('type',1)->orderby('created_at', 'desc')->first();
      $architect_article = Article::where('type',2)->orderBy('created_at','desc')->first();
      $office_article = Article::where('type',3)->orderBy('created_at','desc')->first();
      $insta_photos = DB::table('insta_photos')->inRandomOrder()->get();
      $concept = DB::table('share_concept')->first();
  		$concept = $concept->concept;



        return view('home',compact('sliders','productCategories','projectTypes','projectCategories','products','news','brands','partners','interviews','bulletins','projects','user','student','architect','architect_article','office_article','article','insta_photos','concept'));
    }

    public function likeProduct(Request $req){
      $req->user()->likedProducts()->attach($req->productID);
      return redirect()->back();
    }
    public function dislikeProduct(Request $req){
      $req->user()->likedProducts()->detach($req->productID);
      return redirect()->back();
    }
    public function likeProject(Request $req){
      $req->user()->likedProjects()->attach($req->projectID);
      return redirect()->back();
    }
    public function dislikeProject(Request $req){
      $req->user()->likedProjects()->detach($req->projectID);
      return redirect()->back();
    }

    public function update_product_pivot(Request $req){
      try {
        $req->user()->likedProducts()->updateExistingPivot($req->product_id,[
          'note'=> $req->product_note
        ]);
        return redirect()->back();
      } catch (\Exception $e) {
        return $e->getMessage();
      }

    }

    public function update_project_pivot(Request $req){
      try {
        $req->user()->likedProjects()->updateExistingPivot($req->project_id,[
          'note'=> $req->project_note
        ]);
        return redirect()->back();
      } catch (\Exception $e) {
        return $e->getMessage();
      }

    }

    public function pages(Request $req){
      switch ($req->page) {
        case 'iletisim':
        case 'contact':
          return view('pages.page_contact');
          break;
        case 'hakkimizda':
        case 'about':
            $page = Page::whereSlug($req->page)->first();

            return view('pages.page_about')->with('page',$page);

        default:
          # code...
          break;
      }


    }

}
