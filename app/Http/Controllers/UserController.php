<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;


class UserController extends Controller
{
    public function update(Request $req){
        if(!isset($req->password)){
            User::find($req->user()->id)->update($req->all());
        }
        elseif ($req->password == $req->password-repeat) {
            $user = User::find($req->user()->id);
            $user->email = $req->email;
            $user->phone = $req->phone;
            $user->password = bcrypt($req->password);
        }

    }
}
