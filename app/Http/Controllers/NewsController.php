<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
   public function index(){
   	$news = News::all();

   }

   public function detail($slug){
   	$news = News::where('slug',$slug)->first();

   	$otherNews = News::where('id','!=',$news->id)->get()->take(3);

   	return view('news.detail',compact('news','otherNews'));
   }
}
