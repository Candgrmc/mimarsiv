<?php

namespace App\Http\Controllers;

use App\Bulletin;
use Illuminate\Http\Request;

class BulletinController extends Controller
{
    public function index(){
    	$bulletins = Bulletin::all();
    }
    public function detail($slug){
    	$bulletin = Bulletin::where('slug',$slug)->first();

    	$bulletins = Bulletin::where('id','!=',$bulletin->id)->get()->take(5);

    	return view('bulletins.detail',compact('bulletin','bulletins'));
    }
}
