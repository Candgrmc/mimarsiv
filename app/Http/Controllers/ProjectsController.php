<?php

namespace App\Http\Controllers;

use App\ProjectCategory;
use App\ProjectConsultant;
use App\ProjectGallery;
use App\ProjectProduct;
use App\Projects;
use App\Slider;
use App\Company;
use App\Office;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index()
    {
    	$sliders = Slider::all();
    	$projects = Projects::all();
    	$categories = ProjectCategory::all();


    	return view('projects.list',compact('sliders','projects','categories'));
    }

    public function detail($slug){
    	$project = Projects::where('slug',$slug)->first();
        $company = Office::where('user_id',$project->user_id)->first();
	    $gallery = ProjectGallery::where('project_id',$project->id)->get();
	    $projectProducts = ProjectProduct::where('projects_id',$project->id)->get();
	    $consultants = ProjectConsultant::where('project_id',$project->id)->get();


    	return view('projects.detail',compact('project','gallery','projectProducts','consultants'));
    }

    public function searchProjects(Request $req){
        $id=$req->user()->id;

        $projects = Projects::where('name','like','%'.$req->name.'%')->get();



        return response()->json($projects);
    }
}
