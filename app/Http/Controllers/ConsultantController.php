<?php

namespace App\Http\Controllers;

use App\Consultant;
use App\ProjectCategory;
use App\Slider;
use App\User;
use App\Article;
use DB;
use Illuminate\Http\Request;

class ConsultantController extends Controller
{
    public function index(){
    	$consultants = User::where('user_type_id',4)->get();
    	$categories = DB::table('consultant_categories')->get();
		$sliders = Slider::all();
    	return view('consultants.index',compact('consultants','categories','sliders'));
    }

    public function detail($id){
    	$consultant = User::find($id);
    	return view('consultants.detail',compact('consultant'));
    }
    public function profile(){
        return view('consultants.profile');
    }
    public function add_reference(Request $req){

            $req->user()->references()->sync($req->id,false);
            return redirect()->back();
    }
    public function credentials(){
        $company = Consultant::where('user_id',request()->user()->id)->first();
        return view('consultants.credentials', compact('company'));
    }
    public function updateCredentials(Request $req){
        try{
            $user = User::find($req->user()->id);
            $user->name = $req->name;
            $user->phone = $req->phone;
            if($req->password  && $req->password == $req->password_repeat){
                $user->password = bcrypt($req->password);
            }
            $user->save();
            $cons = Consultant::where('user_id',$req->user()->id)->first();
            $cons->description = $req->description;
            $cons->company_url = $req->url;
            $cons->save();

            $req->session()->flash('success','Başarıyla Güncellendi');
            return redirect()->back();

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function article(Request $req){
      $user = null;
      $consultant = null;
      $article = Article::where('slug',$req->slug)->first();
      $articles = Article::where('id','!=',$article->id)->where('type',$article->type)->get();
      if($article->user_id){
        $user = User::find($article->user_id);
        $consultant = Consultant::where('user_id',$user->id)->first();
      }
      return view('article.detail',compact('user','consultant','article','articles'));
    }


}
