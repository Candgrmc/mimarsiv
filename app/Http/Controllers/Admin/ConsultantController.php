<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Consultant;
use App\User;
use Illuminate\Http\Request;
use DB;
use Cocur\Slugify\Slugify;

class ConsultantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $consultants = User::where('user_type_id',4)->get();
       return view('admin.consultants.index',compact('consultants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Consultant  $consultant
     * @return \Illuminate\Http\Response
     */
    public function show(Consultant $consultant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Consultant  $consultant
     * @return \Illuminate\Http\Response
     */
    public function edit(Consultant $consultant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Consultant  $consultant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Consultant $consultant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Consultant  $consultant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Consultant $consultant)
    {
        //
    }

    public function categories(){
        $categories = DB::table('consultant_categories')->get();


        return view('admin.consultantCategories.index',compact('categories'));
    }
    public function create_category(){
        return view('admin.consultantCategories.create');
    }

    public function add_category(Request $req){
        $slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');
        DB::table('consultant_categories')->insert([
            'title' => $req->title,
            'slug' => $slugify->slugify($req->title)
        ]);

        return redirect()->to('yonetici/consultant_categories')->with('success','Kategori başarıyla eklendi');
    }
    public function edit_category($id){
        $category = DB::table('consultant_categories')->where('id',$id)->first();
        return view('admin.consultantCategories.edit',compact('category'));
    }
    public function update_category(Request $req){
        $category = DB::table('consultant_categories')->where('id',$req->id)->update($req->except('_token'));

        return redirect()->to('yonetici/consultant_categories')->with('success','Başarıyla güncellendi');
    }

    public function delete_category($id){
        DB::table('consultant_categories')->where('id',$id)->delete();
        return back()->with('success','Kategori başarıyla silindi');
    }
}
