<?php

namespace App\Http\Controllers\Admin;

use App\Architect;
use App\Article;
use App\Brand;
use App\Http\Controllers\Controller;
use App\News;
use App\Partner;
use App\Product;
use App\ProductCategory;
use App\ProjectCategory;
use App\Projects;
use App\ProjectType;
use App\Slider;
use App\Student;
use App\User;
use App\Interview;
use DB;
use Illuminate\Http\Request;


class HomeController extends Controller
{

	public function index(){

		$user = User::all()->count();
		$student = Student::all()->count();
		$architect = Architect::all()->count();
		$products = Product::all()->count();
		$projects = Projects::all()->count();
		$productCategories = ProductCategory::all()->count();
		$projectCategories = ProjectCategory::all()->count();
		$projectTypes = ProjectType::all()->count();
		$news = News::all()->count();
		$articles = Article::all()->count();
		$interviews = Interview::all()->count();
		$sliders = Slider::all()->count();
		$brands = Brand::all()->count();
		$partners = Partner::all()->count();
		$approval = User::where('approved',0)->count();

		return view('admin.home',compact('user','student','architect','products','productCategories','projects','projectCategories','projectTypes','news','articles','interviews','sliders','brands','partners','approval'));

	}
public function instagram_index(){
	$photos = DB::table('insta_photos')->get();
	  $concept = DB::table('share_concept')->first();
		$concept = $concept->concept;
	return view('admin.instagram',compact('photos','concept'));
}

public function delete_instagram(Request $req){
	DB::table('insta_photos')->where('id',$req->id)->delete();
	return redirect()->back();
}

	public function instagram(Request $req){
		try{
			$hashtag = ($req->hashtag) ? $req->hashtag : null;

			$owner = ($req->owner) ? '@'.$req->owner : null;

			DB::table('insta_photos')->insert(['url'=>$req->url,'type'=>$req->type,'hashtag'=>$hashtag,'owner'=>$owner]);
			$data['status'] = true;

			return response()->json($data);
		}
		catch(\Exception $e){
			$data = [
				'status' => false,
				'error' => $e->getMessage()
			];
			return response()->json($data);
		}

	}
	public function pushConcept(Request $req){
		$concept = DB::table('share_concept')->where('id',1)->first();
		if($concept){
			DB::table('share_concept')->where('id',1)->update(['concept'=>$req->concept]);
		}else{
			DB::table('share_concept')->insert(['concept' =>$req->concept]);
		}
		return redirect()->back()->with('success','Konsept Başarıyla Kaydedildi');

	}

}
