<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Interview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Cocur\Slugify\Slugify;
use App\Helpers\imageUploader;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = Interview::all();

        return view('admin.interviews.index',compact('interviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('user_type_id',6)->get();
        return view('admin.interviews.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $slugify = new Slugify();
	    $slugify->activateRuleset('turkish')->addRule('_', '2');


	    $imageStatus = imageUploader::storeImage($request);

	    if($imageStatus != false){
		    $image = $imageStatus;
	    }

	    $interview = new Interview();
	    $interview->title = $request->title;
	    $interview->user_id = intval($request->user);
	    $interview->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
	    $interview->image = '/uploads/'.$image;
	    $interview->content = $request->text;
	    $interview->keywords = $request->keywords;
	    $interview->meta_description = $request->meta_description;

	    $interview->save();

	    $interviews = Interview::all();
        $request->session()->flash('success','Başarıyla Güncellendi');
        return redirect()->to('yonetici/interviews');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function show(Interview $interview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function edit(Interview $interview)
    {
	    $users = User::all();
	    return view('admin.interviews.edit',compact('users','interview'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Interview $interview)
    {

	    $slugify = new Slugify();
	    $slugify->activateRuleset('turkish')->addRule('_', '2');

	    if($request->image){

	    $imageStatus = imageUploader::storeImage($request);

	    if(count($imageStatus) == 1){

		    $image = $imageStatus;
	    }
	        }

	    $interview->title = $request->title;
	    $interview->user_id = intval($request->user);
	    $interview->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
	    if($request->image){
		    $interview->image = '/uploads/'.$image;
	    }
	    $interview->content = $request->text;
	    $interview->keywords = $request->keywords;
	    $interview->meta_description = $request->meta_description;

	    $interview->save();

	    $interviews = Interview::all();
        $request->session()->flash('success','Başarıyla Eklendi');
        return redirect()->to('yonetici/interviews');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy(Interview $interview)
    {
	    Storage::disk('public')->delete(substr($interview->image,'9'));
	    $interview->forceDelete();

	    return back()->with('success','Partner Başarıyla Silinmiştir.');
    }
}
