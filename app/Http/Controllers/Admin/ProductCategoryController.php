<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProductCategory;
use Illuminate\Http\Request;
use Cocur\Slugify\Slugify;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_categories = ProductCategory::all();

        return view ('admin.product_category.index',compact('product_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$categories = ProductCategory::all();
        return view ('admin.product_category.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');
        $product_category = new ProductCategory();
	    $product_category->title = $request->title;
	    $product_category->category_id = $request->category_id;
	    $product_category->slug = $slugify->slugify($request->title);
	    $product_category->save();

	    $product_categories = ProductCategory::all();

	    return view ('admin.product_category.index',compact('product_categories'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $productCategory)
    {
	    $categories = ProductCategory::all();
		return view('admin.product_category.edit',compact('productCategory','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $productCategory)
    {
	    $productCategory->title = $request->title;
	    $productCategory->slug = $request->slug;
	    $productCategory->category_id = $request->category_id;
	    $productCategory->save();

	    return back()->with('success','Kategori Başarıyla Düzenlenmiştir.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $productCategory)
    {
	    $productCategory->delete();

	    return back()->with('success','Kategori Başarıyla Silinmiştir.');
    }
}
