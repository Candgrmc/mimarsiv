<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;
use Cocur\Slugify\Slugify;
use App\Helpers\imageUploader;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$pages = Page::all();

		return view('admin.pages.index',compact('pages'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{

		return view('admin.pages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');


		$imageStatus = imageUploader::storeImage($request);

		if($imageStatus != false){
			$image = $imageStatus;
		}

		$page = new Page();
		$page->title = $request->title;
		$page->isActive = $request->isActive == "on" ? true:false;
		$page->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
		$page->image = '/uploads/'.$image;
		$page->content = $request->text;
		$page->keywords = $request->keywords;
		$page->meta_description = $request->meta_description;

		$page->save();

		$pages = Page::all();
		return view('admin.pages.index',compact('pages'))->with('success','Makale Başarıyla Eklenmiştir.');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Page  $page
	 * @return \Illuminate\Http\Response
	 */
	public function show(Page $page)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Page  $page
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Page $page)
	{
		return view('admin.pages.edit',compact('page'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Page  $page
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Page $page)
	{
		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');

		if($request->file){
			$imageStatus = imageUploader::storeImage($request);

			if($imageStatus != false){
				$image = $imageStatus;
			}
		}

		$page->title = $request->title;
		$page->isActive = $request->isActive;
		$page->slug = $slugify->slugify($request->title);
		if($request->file){
			$page->image = '/uploads/'.$image;
		}
		$page->content = $request->text;
		$page->keywords = $request->keywords;
		$page->isActive = $request->isActive == "on" ? true:false;
		$page->meta_description = $request->meta_description;

		$page->save();

		$pages = Page::all();
		return view('admin.pages.index',compact('pages'))->with('success','Sayfa Başarıyla Eklenmiştir.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Page  $page
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Page $page)
	{
		Storage::disk('public')->delete(substr($page->image,'9'));
		$page->forceDelete();

		return back()->with('success','Sayfa Başarıyla Silinmiştir.');
	}
}
