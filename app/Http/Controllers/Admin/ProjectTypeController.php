<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProjectType;
use Illuminate\Http\Request;
use Cocur\Slugify\Slugify;

class ProjectTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $project_types = ProjectType::all();

	    return view ('admin.project_type.index',compact('project_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view ('admin.project_type.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');
	    $project_type = new ProjectType();
	    $project_type->title = $request->title;
	    $project_type->slug = $slugify->slugify($request->title);
	    $project_type->save();

	    $project_categories = ProjectType::all();

	    return view ('admin.project_type.index',compact('project_type'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectType $projectType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectType $projectType)
    {
	    return view('admin.project_type.edit',compact('projectType'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectType $projectType)
    {
	    $projectType->title = $request->title;
	    $projectType->slug = $request->slug;
	    $projectType->save();

	    return back()->with('success','Tip Başarıyla Düzenlenmiştir.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectType $projectType)
    {
	    $projectType->delete();

	    return back()->with('success','Kategori Başarıyla Silinmiştir.');
    }
}
