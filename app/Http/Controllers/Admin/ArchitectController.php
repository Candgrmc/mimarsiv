<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Architect;
use App\User;
use Illuminate\Http\Request;

class ArchitectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $architects = User::where('user_type_id',2)->get();

        return view('admin.architects.index',compact('architects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Architect  $architect
     * @return \Illuminate\Http\Response
     */
    public function show(Architect $architect)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Architect  $architect
     * @return \Illuminate\Http\Response
     */
    public function edit(Architect $architect)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Architect  $architect
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Architect $architect)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Architect  $architect
     * @return \Illuminate\Http\Response
     */
    public function destroy(Architect $architect)
    {
        //
    }
}
