<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Certificate;
use App\Company;
use App\Helpers\imageUploader;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductCertificate;
use App\ProductGallery;
use App\ProductSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\Storage;
use App\ProductCategory;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	   $products = Product::orderBy('created_at', 'desc')->get();


        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    	$categories = ProductCategory::select('id','title')->get();
    	$certificates = Certificate::select('id','name')->get();
    	$brands = Brand::all();
    	$company = Company::where('user_id',request()->user()->id)->first();

	    return view('admin.product.create',compact('categories','certificates','brands','company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $lastID = Product::orderBy('id','desc')->first();
	    $count =  $lastID->id+1;
	    $slugify = new Slugify();
	    $slugify->activateRuleset('turkish')->addRule('_', '2');

    	$user = Auth::user()->getAuthIdentifier();
	    $image = $request->file('image');

	    $imageStatus = imageUploader::multiImage($image);

	    if($imageStatus != false){
		    $gallery = $imageStatus;
	    }

	    $product = new Product();
	    $product->name = $request->title;
	    $product->user_id = $user;
	    $product->product_category_id = intval($request->category);

	    if($request->slug){
	    	if(count(Product::where('slug',$request->slug)->first()) == 1){

			    $product->slug = $request->slug.'-'.$count;
		    } else {

			    $product->slug = $request->slug;
		    }
	    } else {

	    	if(count(Product::where('slug',$slugify->slugify($request->title))->first()) ==1 ){

			    $product->slug = $slugify->slugify($request->title).'-'.$count;
		    } else {
			    $product->slug = $slugify->slugify($request->title);

		    }


	    }
	    $product->color = $request->color;
	    $product->image = '/uploads/'.$gallery[0];
	    $product->brand_id = intval($request->brand);
	    $product->catalogue = $request->catalogue;
	    $product->video = $request->video;
	    $product->overview = $request->overview;
	    $product->bim = $request->bim;
	    $product->price = $request->price;
	    $product->save();

	    $productID = Product::where('image','/uploads/'.$gallery[0])->select('id')->first();


	    foreach ($gallery as $item){
	    	$productGallery = new ProductGallery();
	    	$productGallery->product_id = $productID->id;
	    	$productGallery->image = '/uploads/'.$item;
	    	$productGallery->save();
	    }
		if(count($request->certificates) > 0){
			foreach ($request->certificates as $item2){
				$productCertificate = new ProductCertificate();
				$productCertificate->product_id = $productID->id;
				$productCertificate->certificate_id = intval($item2);
				$productCertificate->save();
			}
		}


	    $products = Product::all()->load('user');


	    return view('admin.product.index',compact('products'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
	    $products = Product::with('user','category','certificates')->get();
	    $categories = ProductCategory::select('id','title')->get();
	    $certificates = Certificate::select('id','name')->get();
	    $selectedCertificates = ProductCertificate::where('product_id',$product->id)->get();
		$brands = Brand::all();
	    $images = ProductGallery::where('product_id',$product->id)->get();

	    return view('admin.product.edit',compact('products','product','categories','certificates','selectedCertificates','images','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
	    $slugify = new Slugify();
	    $slugify->activateRuleset('turkish')->addRule('_', '2');

	    $user = Auth::user()->getAuthIdentifier();
	    $image = $request->file('image');

	    if(count($image) > 0){
	    $imageStatus = imageUploader::multiImage($image);

	    if($imageStatus != false){
		    $gallery = $imageStatus;
	    }
	    }

	    $product->name = $request->title;
	    $product->user_id = $user;
	    $product->product_category_id = intval($request->category);
	    $product->slug = $slugify->slugify($request->title);
	    $product->color = $request->color;
	    if(count($image) > 0){
		    $product->image = '/uploads/'.$gallery[0];
	    }
	    $product->brand_id = $request->brand;
	    $product->catalogue = $request->catalogue;
	    $product->video = $request->video;
	    $product->overview = $request->overview;
	    $product->bim = $request->bim;
	    $product->price = $request->price;
	    $product->save();



	    if(count($image) > 0) {

		    foreach ($gallery as $item){
			    $productGallery = new ProductGallery();
			    $productGallery->product_id = $product->id;
			    $productGallery->image = '/storage/'.$item;
			    $productGallery->save();
		    }
	    }

	   ProductCertificate::where('product_id',$product->id)->delete();

	    foreach ($request->certificates as $item2){
		    $productCertificate = new ProductCertificate();
		    $productCertificate->product_id = $product->id;
		    $productCertificate->certificate_id = intval($item2);
		    $productCertificate->save();
	    }

	    $products = Product::all()->load('user');

	    //dd($products);
	    return view('admin.product.index',compact('products'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
	    // Let's delete assets

	    Storage::disk('public')->delete(substr($product->image,'9'));
	    $product->delete();

	    return back()->with('success','Ürün Başarıyla Silinmiştir.');
    }

    public function deleteImage($id)
    {
	    $image = ProductGallery::find($id)->delete();

	    Storage::disk('public')->delete(substr($id,'9'));

	    return back()->with('success','Resim başarıyla silinmiştir');
    }

    public function setFeatured($id)
    {
    	$product = Product::find($id);
    	$product->featured = 1;
    	$product->save();

	    return back()->with('success','Ürün Sitede Gösteriliyor.');

    }

    public function deSetFeatured($id)
    {
	    $product = Product::find($id);
	    $product->featured = 0;
	    $product->save();

	    return back()->with('success','Ürün Sitede Gösterilmiyor.');
    }


}
