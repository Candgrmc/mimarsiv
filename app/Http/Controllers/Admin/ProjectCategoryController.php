<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProjectCategory;
use Illuminate\Http\Request;
use Cocur\Slugify\Slugify;

class ProjectCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $project_categories = ProjectCategory::all();

	    return view ('admin.project_category.index',compact('project_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$categories = ProjectCategory::all();
	    return view ('admin.project_category.create',compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slugify = new Slugify();
        $slugify->activateRuleset('turkish')->addRule('_', '2');
	    $project_category = new ProjectCategory();
	    $project_category->title = $request->title;
	    $project_category->category_id = $request->category_id;
	    $project_category->slug = $slugify->slugify($request->title);
	    $project_category->save();

	    $project_categories = ProjectCategory::all();

	    return redirect('/yonetici/project_category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectCategory $projectCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectCategory $projectCategory)
    {
	    $categories = ProjectCategory::all();

	    return view('admin.project_category.edit',compact('projectCategory','categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectCategory $projectCategory)
    {
        $slugify = new Slugify();
        $slugify->activateRuleset('turkish')->addRule('_', '2');

	    $projectCategory->title = $request->title;
	    $projectCategory->slug = $slugify->slugify($request->title);
	    $projectCategory->category_id = $request->category_id;
	    $projectCategory->save();

	    return back()->with('success','Kategori Başarıyla Düzenlenmiştir.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectCategory $projectCategory)
    {
	    $projectCategory->delete();

	    return back()->with('success','Kategori Başarıyla Silinmiştir.');
    }
}
