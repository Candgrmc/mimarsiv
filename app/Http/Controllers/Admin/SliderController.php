<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\imageUploader;
use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $sliders = DB::table('sliders')
		    ->orderBy('order', 'asc')
		    ->get();

	    return view('admin.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

	    $imageStatus = imageUploader::storeImage($request);

	    if($imageStatus != false){
		    $image = $imageStatus;
	    }

	    $slider = new Slider();
	    $slider->title = $request->title;
	    $slider->image = '/uploads/'.$image;
	    $slider->description = $request->description;
	    $slider->redirectTo = $request->redirectTo;
	    $slider->order = $request->order;
	    $slider->isActive = $request->isActive == 'on' ? true:false;
	    $slider->save();

	    $sliders = Slider::all();
        return redirect()->back()->with('success','Slider Başarıyla Güncellenmiştir.');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('admin.slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {

        if(count($request->image) == 0){
	        $slider->title = $request->title;
	        $slider->description = $request->description;
	        $slider->redirectTo = $request->redirectTo;
	        $slider->order = $request->order;
	        $slider->isActive = $request->isActive == 'on' ? true:false;
	        $slider->save();
        } else {
	        $imageStatus = imageUploader::storeImage($request);

	        if($imageStatus != false){
		        $image = $imageStatus;
	        }

	        $slider->title = $request->title;
	        $slider->image = '/uploads/'.$image;
	        $slider->redirectTo = $request->redirectTo;
	        $slider->description = $request->description;
	        $slider->order = $request->order;
	        $slider->isActive = $request->isActive == 'on' ? true:false;
	        $slider->save();
        }

	    $sliders = Slider::all();
        return redirect()->back()->with('success','Slider Başarıyla Güncellenmiştir.');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
    	// Let's delete assets

	    Storage::disk('public')->delete(substr($slider->image,'9'));
	    $slider->forceDelete();

	    return back()->with('success','Slider Başarıyla Silinmiştir.');
    }
}
