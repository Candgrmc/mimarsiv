<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Event;
use App\User;
use Illuminate\Http\Request;
use DB;
use Cocur\Slugify\Slugify;

class EventController extends Controller
{
  public function index(){
    $events = Event::get();

    return view('admin.events.index',compact('events'));
  }

  public function create(){
    return view('admin.events.create');
  }

  public function edit(Event $event){
    return view('admin.events.edit',compact('event'));
  }
  public function delete(Event $event){
    $event->delete();
    return redirect()->back()->with('success','Etkinlik Başarıyla Eklendir');
  }
}
