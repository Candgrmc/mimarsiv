<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\UserCv;
use Illuminate\Http\Request;

class UserCvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserCv  $userCv
     * @return \Illuminate\Http\Response
     */
    public function show(UserCv $userCv)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserCv  $userCv
     * @return \Illuminate\Http\Response
     */
    public function edit(UserCv $userCv)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserCv  $userCv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserCv $userCv)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserCv  $userCv
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCv $userCv)
    {
        //
    }
}
