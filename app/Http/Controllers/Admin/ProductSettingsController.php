<?php

namespace App\Http\Controllers\Admin;

use App\ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductSettingsController extends Controller
{
    public function settings(){

        $settings = ProductSetting::get();

        return view('admin.product.settings',compact('settings'));

    }
    public function updateSettings(Request $req){
        $settings = ProductSetting::create([
            'price' => $req->price,
            ]);

        return redirect()->back();

    }
}
