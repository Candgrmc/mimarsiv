<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Partner;
use Illuminate\Http\Request;
use App\Helpers\imageUploader;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
		$partners = Partner::all();

		return view('admin.partners.index',compact('partners'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.partners.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$imageStatus = imageUploader::storeImage($request);

		if($imageStatus != false){
			$image = $imageStatus;
		}

		$partner = new Partner();
		$partner->name = $request->name;
		$partner->website = $request->website;
		$partner->image = '/uploads/'.$image;
		$partner->save();

		$partners = Partner::all();
        $request->session()->flash('success','Başarıyla Eklendi');
        return redirect()->back();


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Partner  $partner
	 * @return \Illuminate\Http\Response
	 */
	public function show(Partner $partner)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Partner  $partner
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Partner $partner)
	{

		return view('admin.partners.edit',compact('partner'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Partner  $partner
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Partner $partner)
	{


		if(count($request->image) == 0){
			$partner->name = $request->name;
			$partner->website = $request->website;
			$partner->save();
		} else {
			$imageStatus = imageUploader::storeImage($request);

			if($imageStatus != false){
				$image = $imageStatus;
			}

			$partner->name = $request->name;
			$partner->website = $request->website;
			$partner->image = '/uploads/'.$image;
			$partner->save();
		}

		$partners = Partner::all();
        $request->session()->flash('success','Başarıyla Güncellendi');
        return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Partner  $partner
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Partner $partner)
	{
		// Let's delete assets

		Storage::disk('public')->delete(substr($partner->image,'9'));
		$partner->forceDelete();

		return back()->with('success','Partner Başarıyla Silinmiştir.');
	}
}
