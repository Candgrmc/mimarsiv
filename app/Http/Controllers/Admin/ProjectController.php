<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\imageUploader;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProjectConsultant;
use App\ProjectProduct;
use App\Projects;
use App\ProjectCategory;
use App\ProjectGallery;
use App\ProjectType;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\Storage;
use DB;

class ProjectController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$projects = Projects::all();


		return view('admin.projects.index',compact('projects'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$categories = ProjectCategory::select('id','title')->get();
		$types = ProjectType::select('id','name')->get();
		$investors = User::where('user_type_id',3)->get();
		$consultants = User::where('user_type_id',4)->get();
		$offices = User::where('user_type_id',6)->get();
		$products = Product::all();

		return view('admin.projects.create',compact('categories','types','investors','consultants','offices','products'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');

		$user = Auth::user()->getAuthIdentifier();
		$image = $request->file('image');

		$imageStatus = imageUploader::multiImage($image);

		if($imageStatus != false){
			$gallery = $imageStatus;
		}

		$project = new Projects();
		$project->name = $request->title;
		$project->slug = $slugify->slugify($request->title);
		$project->project_type_id = intval($request->type);
		$project->project_category_id = intval($request->category);
		$project->company_id = intval($request->office);
		$project->investor_id = intval($request->investor);
		$project->story = $request->story;
		$project->year = $request->year;
		$project->image = '/storage/'.$gallery[0];
		$project->country = $request->country;
		$project->save();

		$projectID = Projects::where('image','/storage/'.$gallery[0])->select('id')->first();


		foreach ($gallery as $item){
			$projectGallery = new ProjectGallery();
			$projectGallery->project_id = $projectID->id;
			$projectGallery->image = '/storage/'.$item;
			$projectGallery->save();
		}

		foreach ($request->products as $item2){
			$projectProduct = new ProjectProduct();
			$projectProduct->projects_id = $projectID->id;
			$projectProduct->product_id = intval($item2);
			$projectProduct->save();
		}
		foreach ($request->consultants as $item3){
			$projectConsultant = new ProjectConsultant();
			$projectConsultant->project_id = $projectID->id;
			$projectConsultant->consultant_id = intval($item3);
			$projectConsultant->save();
		}

		$projects = Projects::all()->load('user');

		//dd($projects);
		return view('admin.projects.index',compact('projects'));

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Projects  $project
	 * @return \Illuminate\Http\Response
	 */
	public function show(Projects $project)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Projects  $project
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Projects $project)
	{
		$categories = ProjectCategory::select('id','title')->get();
		$types = ProjectType::select('id','name')->get();
		$investors = User::where('user_type_id',3)->get();
		$consultants = User::where('user_type_id',4)->get();
		$offices = User::where('user_type_id',6)->get();
		$products = Product::all();
		$ProjectConsultants = ProjectConsultant::where('project_id',$project->id)->get();
		$projectProducts = ProjectProduct::where('projects_id',$project->id)->get();
		$images = ProjectGallery::where('project_id',$project->id)->get();


		return view('admin.projects.edit',compact('projectProducts','ProjectConsultants','categories','types','investors','consultants','offices','products','project','images'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Projects  $project
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Projects $project)
	{

		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');

		$user = Auth::user()->getAuthIdentifier();
		$image = $request->file('image');

		if(count($image) > 0){
			$imageStatus = imageUploader::multiImage($image);

			if($imageStatus != false){
				$gallery = $imageStatus;
			}
		}

		$project->name = $request->title;

		$project->project_category_id = intval($request->category);
		$project->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);

		if(count($image) > 0){
			$project->image = '/uploads/'.$gallery[0];
		}

		$project->catalogue = $request->catalogue;
		$project->video = $request->video;
		$project->overview = $request->overview;
		$project->bim = $request->bim;
		//$project->price = $request->price;
		$project->save();



		if(count($image) > 0) {


			foreach ($gallery as $item){
				$projectGallery = new ProjectGallery();
				$projectGallery->project_id = $project->id;
				$projectGallery->image = '/uploads/'.$item;
				$projectGallery->save();
			}
		}

		/*ProjectsCertificate::where('project_id',$project->id)->delete();

		foreach ($request->certificates as $item2){
			$projectCertificate = new ProjectsCertificate();
			$projectCertificate->projects_id = $project->id;
			$projectCertificate->certificate_id = intval($item2);
			$projectCertificate->save();
		}

		$projects = Projects::all()->load('user');
		*/

		//dd($projects);
		return redirect()->back();
	}

	public function deleteImage($id)
	{
		$image = ProdjetGallery::find($id)->delete();

		Storage::disk('public')->delete(substr($id,'9'));

		return back()->with('success','Resim başarıyla silinmiştir');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Projects  $project
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Projects $project)
	{
		// Let's delete assets

		Storage::disk('public')->delete(substr($project->image,'9'));
		$project->delete();

		return back()->with('success','Ürün Başarıyla Silinmiştir.');
	}
	public function settings(){

        $settings = DB::table('project_settings')->get();

        return view('admin.projects.settings',compact('settings'));

    }
    public function updateSettings(Request $req){
        $settings = DB::table('project_settings')->insert([
            'price' => $req->price
            ]);

        return redirect()->back();

    }
}
