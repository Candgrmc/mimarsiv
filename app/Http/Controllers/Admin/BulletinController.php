<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Bulletin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Cocur\Slugify\Slugify;
use App\Helpers\imageUploader;

class BulletinController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$bulletins = Bulletin::all();

		return view('admin.bulletins.index',compact('bulletins'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$users = User::all();
		return view('admin.bulletins.create',compact('users'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');


		$imageStatus = imageUploader::storeImage($request);

		if($imageStatus != false){
			$image = $imageStatus;
		}

		$bulletin = new Bulletin();
		$bulletin->name = $request->title;
		$bulletin->user_id = intval($request->user);
		$bulletin->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
		$bulletin->image = '/uploads/'.$image;
		$bulletin->isActive = $request->isActive == "on" ? true:false;
		$bulletin->content = $request->text;
		$bulletin->keywords = $request->keywords;
		$bulletin->meta_description = $request->meta_description;

		$bulletin->save();

		$bulletins = Bulletin::all();
		return view('admin.bulletins.index',compact('bulletins'))->with('success','Bülten Başarıyla Eklenmiştir.');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Bulletin  $bulletin
	 * @return \Illuminate\Http\Response
	 */
	public function show(Bulletin $bulletin)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Bulletin  $bulletin
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Bulletin $bulletin)
	{
		$users = User::all();
		return view('admin.bulletins.edit',compact('users','bulletin'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Bulletin  $bulletin
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Bulletin $bulletin)
	{
		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');

		if($request->image){

			$imageStatus = imageUploader::storeImage($request);

			if(count($imageStatus) == 1){

				$image = $imageStatus;
			}
		}

		$bulletin->name = $request->title;
		$bulletin->user_id = intval($request->user);
		$bulletin->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
		if($request->image){
			$bulletin->image = '/uploads/'.$image;
		}
		$bulletin->isActive = $request->isActive == "on" ? true:false;
		$bulletin->content = $request->text;
		$bulletin->keywords = $request->keywords;
		$bulletin->meta_description = $request->meta_description;

		$bulletin->save();

		$bulletins = Bulletin::all();
		return view('admin.bulletins.index',compact('bulletins'))->with('success','Bülten Başarıyla Eklenmiştir.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Bulletin  $bulletin
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Bulletin $bulletin)
	{
		Storage::disk('public')->delete(substr($bulletin->image,'9'));
		$bulletin->forceDelete();

		return back()->with('success','Bülten Başarıyla Silinmiştir.');
	}
}
