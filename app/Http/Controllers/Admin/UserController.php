<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users =  User::all();

        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
	    $user->delete();

	    return back()->with('success','Kullanıcı Başarıyla Silinmiştir.');
    }

    public static function activateUser($id){

	    if (! Gate::allows('admin_access')) {
		    return abort(404);
	    }
    	$user = User::find($id);
    	$user->approved = 1;
    	$user->save();
	    return back()->with('success','Üye Aktifleştirilmiştir');

    }
	public static function deActivateUser($id){
		if (! Gate::allows('admin_access')) {
			return abort(404);
		}
		$user = User::find($id);
		$user->approved = 0;
		$user->save();
		return back()->with('success','Üye Pasifleştirilmiştir');
	}

	public function changeUserPassword($id)
	{
    	return view('admin.users.change_password',compact('id'));
	}

	public function setUserPassword($id,Request $request)
	{
    	$user = User::findOrFail($id);
    	$user->password = bcrypt($request->password);
    	$user->save();

		return redirect('/yonetici/users')->with('success','Şifre Başarıyla Değiştirilmiştir');
	}
}
