<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\News;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use Cocur\Slugify\Slugify;
use App\Helpers\imageUploader;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
		$news = News::all();

		return view('admin.news.index',compact('news'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$users = User::all();
		return view('admin.news.create',compact('users'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');


		$imageStatus = imageUploader::storeFile($request->image);
		$mainImageStatus = imageUploader::storeFile($request->main_image);

		if($imageStatus != false){
			$image = $imageStatus;
		}
		if($mainImageStatus != false){
			$mainImage = $mainImageStatus;
		}

		$new = new News();
		$new->title = $request->title;
		$new->slug = $slugify->slugify($request->title);
		$new->image = '/uploads/'.$image;
		$new->main_image = '/uploads/'.$mainImage;
		$new->content = $request->text;
		$new->keywords = $request->keywords;
		$new->meta_description = $request->meta_description;
		$new->isActive = $request->isActive == "on" ? true:false;
		$new->isFeatured = $request->isFeatured == "on" ? true:false;

		$new->save();

		$news = News::all();
        $request->session()->flash('success','Başarıyla Eklendi');
        return redirect()->to('yonetici/news');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\News  $new
	 * @return \Illuminate\Http\Response
	 */
	public function show(News $new)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\News  $new
	 * @return \Illuminate\Http\Response
	 */
	public function edit(News $news)
	{
		$users = User::all();

		return view('admin.news.edit',compact('users','news'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\News  $new
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, News $news)
	{

		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');



			$imageStatus = imageUploader::storeFile($request->image);
			$mainImageStatus = imageUploader::storeFile($request->main_image);

			if($imageStatus != false){
				$image = $imageStatus;
			}
			if($mainImageStatus != false){
				$mainImage = $mainImageStatus;
			}


		$news->title = $request->title;
		$news->slug =$slugify->slugify($request->title);
		if($image){
			$news->image = '/uploads/'.$image;
		}
		if($mainImage){
			$news->main_image = '/uploads/'.$mainImage;
		}
		$news->content = $request->text;
		$news->keywords = $request->keywords;
		$news->meta_description = $request->meta_description;
		$news->isActive = $request->isActive == "on" ? true:false;
		$news->isFeatured = $request->isFeatured == "on" ? true:false;

		$news->save();

		$newsList = News::all();
		$request->session()->flash('success','Başarıyla Güncellendi');
		return redirect()->to('yonetici/news');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\News  $new
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(News $news)
	{
		Storage::disk('public')->delete(substr($news->image,'9'));

		$news->delete();

		return back()->with('success','Haber Başarıyla Silinmiştir.');
	}
}
