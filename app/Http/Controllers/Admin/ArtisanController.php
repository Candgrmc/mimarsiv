<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Artisan;
use App\User;
use Illuminate\Http\Request;
use DB;
use Cocur\Slugify\Slugify;
class ArtisanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artisans = User::where('user_type_id',5)->get();

        return view('admin.artisans.index',compact('artisans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Artisan  $artisan
     * @return \Illuminate\Http\Response
     */
    public function show(Artisan $artisan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Artisan  $artisan
     * @return \Illuminate\Http\Response
     */
    public function edit(Artisan $artisan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Artisan  $artisan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Artisan $artisan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Artisan  $artisan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artisan $artisan)
    {
        //
    }

    public function categories(){
        $categories = DB::table('artisan_categories')->get();


        return view('admin.artisanCategories.index',compact('categories'));
    }
    public function create_category(){
        return view('admin.artisanCategories.create');
    }

    public function add_category(Request $req){
        $slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');
        DB::table('artisan_categories')->insert([
            'title' => $req->title,
            'slug' => $slugify->slugify($req->title)
        ]);

        return redirect()->to('yonetici/artisan_categories')->with('success','Kategori başarıyla eklendi');
    }
    public function edit_category($id){
        $category = DB::table('artisan_categories')->where('id',$id)->first();
        return view('admin.artisanCategories.edit',compact('category'));
    }
    public function update_category(Request $req){
        $category = DB::table('artisan_categories')->where('id',$req->id)->update($req->except('_token'));

        return redirect()->to('yonetici/artisan_categories')->with('success','Başarıyla güncellendi');
    }

    public function delete_category($id){
        DB::table('artisan_categories')->where('id',$id)->delete();
        return back()->with('success','Kategori başarıyla silindi');
    }
}
