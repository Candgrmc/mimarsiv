<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Article;
use Illuminate\Http\Request;
use Cocur\Slugify\Slugify;
use App\Helpers\imageUploader;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$articles = Article::all();

		return view('admin.articles.index',compact('articles'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$users = User::where('user_type_id',4)->get();
		return view('admin.articles.create',compact('users'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');

		$imageDetail = imageUploader::storeFile($request->detail_image);
		$imageStatus = imageUploader::storeFile($request->main_image);

		if($imageStatus != false){
			$main_image = $imageStatus;
		}
		if($imageDetail != false){
			$detail_image = $imageDetail;
		}


		$article = new Article();
		$article->title = $request->title;
		$article->user_id = $request->user;
		$article->type = $request->article_type;
		$article->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
		if($main_image){
			$article->image = '/uploads/'.$main_image;
		}
		if($detail_image){
			$article->detail_image = '/uploads/'.$detail_image;
		}
		$article->content = $request->text;
		$article->keywords = $request->keywords;
		$article->meta_description = $request->meta_description;

		$article->save();

		$articles = Article::all();
		return redirect()->to('yonetici/articles');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Article  $article
	 * @return \Illuminate\Http\Response
	 */
	public function show(Article $article)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Article  $article
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Article $article)
	{
		$users = User::all();
		return view('admin.articles.edit',compact('users','article'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Article  $article
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Article $article)
	{
		$slugify = new Slugify();
		$slugify->activateRuleset('turkish')->addRule('_', '2');

		if($request->file){
			$imageStatus = imageUploader::storeImage($request);

			if($imageStatus != false){
				$image = $imageStatus;
			}
		}

		$article->title = $request->title;
		$article->user_id = intval($request->user);
		$article->slug = $slugify->slugify($request->title);
		if($request->file){
			$article->image = '/storage/'.$image;
		}
		$article->content = $request->text;
		$article->keywords = $request->keywords;
		$article->meta_description = $request->meta_description;

		$article->save();

		$articles = Article::all();
		return redirect()->to('yonetici/articles')->with('success','Makale Başarıyla Düzenlendi.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Article  $article
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Article $article)
	{
		Storage::disk('public')->delete(substr($article->image,'9'));
		$article->forceDelete();

		return back()->with('success','Makale Başarıyla Silinmiştir.');
	}
}
