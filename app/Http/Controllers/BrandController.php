<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Product;
use App\Slider;
use App\ProductCategory;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index(){
    	$brands = Brand::all();
      $sliders = Slider::get();

    	return view('brands.index',compact('brands','sliders'));
    }

    public function detail ($slug){
    	$brand = Brand::where('slug',$slug)->first();

    	$products = Product::where('brand_id',$brand->id)->get();

    	$categories = ProductCategory::all();

    	return view('products.index',compact('products','categories'));
    }
}
