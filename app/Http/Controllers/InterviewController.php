<?php

namespace App\Http\Controllers;

use App\Interview;
use App\User;
use App\Office;
use App\Projects;
use Illuminate\Http\Request;

class InterviewController extends Controller
{
    public function index(){
    	$interviews = Interview::all();

    }

    public function detail($slug)
    {
    	$interview = Interview::where('slug',$slug)->first();

    	$interviews = Interview::where('id','!=',$interview->id)->get()->take(5);
      $user = User::find($interview->user_id);
      $office = Office::where('user_id',$user->id)->first();
      $projects = Projects::where('user_id',$user->id)->orderBy('created_at','desc')->take(6)->get();




    	return view('interviews.detail',compact('interview','interviews','user','office','projects'));
    }
}
