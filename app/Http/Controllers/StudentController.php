<?php

namespace App\Http\Controllers;

use App\Student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class StudentController extends Controller
{


    public function index(){
        if (! Gate::allows('student_access')) {
            return abort(404);
        }
        $student = request()->user();
  
        return view('students.index',compact('student'));
    }
    public function credentials(){
        if (! Gate::allows('student_access')) {
            return abort(404);
        }
        $school = Student::where('user_id',request()->user()->id)->first();

        return view('students.credentials',compact('school'));
    }

    public function update(Request $req){
        if (! Gate::allows('student_access')) {
            return abort(404);
        }
        try{
            $student = User::find($req->user()->id);
            $student->name = $req->name;
            if($student->password && $student->password == $student->password_repeat){
                $student->password = bcrypt($student->password);
            }
            $student->phone = $req->phone;
            $student->save();
            $req->session()->flash('success','Bilgileriniz başarıyla güncellendi');
            return redirect()->back();
        }catch (\Exception $e){
            return $e->getMessage();
        }



    }
}
