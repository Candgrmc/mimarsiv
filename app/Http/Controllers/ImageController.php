<?php

namespace App\Http\Controllers;

use App\Helpers\imageUploader;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ImageController extends Controller
{
    public function update(Request $req){

        $user = User::find($req->user()->id);
        if(Input::hasFile('logo')){
            $img = imageUploader::storeFile($req->logo);
            $user->image = 'uploads/'.$img;
        }
        if(Input::hasFile('cover')){
            $cover = imageUploader::storeFile($req->cover);
            $user->cover = 'uploads/'.$cover;

        }
        $user->save();
        return redirect()->back();
    }
}
