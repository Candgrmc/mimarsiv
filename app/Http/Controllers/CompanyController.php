<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Slider;
use App\Legal;
use App\ProductSetting;
use App\ProjectGallery;
use App\Projects;
use App\Certificate;
use App\Company;
use App\ProductCategory;
use App\User;
use App\Investor;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cocur\Slugify\Slugify;
use App\Product;
use App\Helpers\imageUploader;
use App\ProductGallery;
use App\ProductCertificate;
use App\ProjectCategory;
use App\ProjectProduct;
use App\Prize;
use Illuminate\Support\Facades\Input;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



	    $company = Company::where('user_id',Auth::user()->getAuthIdentifier())->select('slug')->first();

	    return view('company.index',compact('company'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function companies(){
       $companies = Company::all();
       $sliders = Slider::get();
       return view('company.list',compact('companies','sliders'));
     }

     public function detail(Request $req){
       $company = Company::whereSlug($req->slug)->first();
       $user = User::find($company->user_id);

       return  view('company.detail',compact('user','company'));
     }

    public function create()
    {
	    $company = Company::where('user_id',Auth::user()->getAuthIdentifier())->first();
	    $categories = ProductCategory::all();
	    $certificates = Certificate::all();
	    $brands = Brand::all();

        return view('company.create_product',compact('company','categories','certificates','brands'));
    }
    public function createProject()
    {
        $company = Company::where('user_id',Auth::user()->getAuthIdentifier())->first();
        $categories = ProjectCategory::all();
        $certificates = Certificate::all();
        $brands = Brand::all();
        $investors = Company::get();

        return view('company.create_project',compact('company','categories','certificates','brands','investors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $certificates = null;
        $prizes = null;
        $legals = null;

        $slugify = new Slugify();
        $slugify->activateRuleset('turkish')->addRule('_', '2');

        $user = Auth::user()->getAuthIdentifier();
        $image = $request->file('image');


        $imageStatus = imageUploader::multiImage($image);


        if($imageStatus != false){
            $gallery = $imageStatus;
        }

        $product = new Product();
        $product->name = $request->title;
        $product->user_id = $user;
        $product->product_category_id = intval($request->category);
        $product->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
        $product->color = $request->color;
        $product->image = '/uploads/'.$gallery[0];
        $product->brand_id = $request->brand;
        $product->video = $request->video;
        $product->overview = $request->overview;
        if(Input::file('bim')){
            $bim = '/uploads/'.imageUploader::storeFile($request->bim);
            $product->bim = $bim;
        }
        if(Input::file('catalogue')){
            $catalogue = '/uploads/'.imageUploader::storeFile($request->catalogue);
            $product->catalogue = $catalogue;
        }

        if($request->brand_id){
            $product->brand_id = $request->brand_id;
        }else{
            $image = imageUploader::storeFile(Input::file('brand_img'));
            $brand = new Brand();
            $brand->name = $request->brand_name;
            if($image){
                $brand->image = '/uploads/'.$image;
            }
            $brand->save();

        }

        $product->price = $request->price;
        $product->featured = 0;
        if($product->save() && $request->project_id){
            $projectProduct = new ProjectProduct();
            $projectProduct->product_id = $product->id;
            $projectProduct->projects_id = $request->project_id;
            $projectProduct->save();
        }

        if(Input::hasFile('certificates')){
            $certificates = $request->file('certificates');

            $certificates = imageUploader::multiImage($certificates);

        }
        if(Input::hasFile('prizes')){
            $prizes = $request->file('prizes');

            $prizes = imageUploader::multiImage($prizes);

        }
        if(Input::hasFile('legals')){
            $legals = $request->file('legals');

            $legals = imageUploader::multiImage($legals);

        }





        foreach ($gallery as $item){
            $productGallery = new ProductGallery();
            $productGallery->product_id = $product->id;
            $productGallery->image = '/uploads/'.$item;
            $productGallery->save();
        }

        if($certificates){
            $count = 1;
            foreach ($certificates as $item2){
                $certificate = new Certificate();
                $certificate->name = 'Sertifika-'.$count;
                $certificate->slug = $slugify->slugify($product->name).'-'.'Sertifika-'.$count;
                $certificate->img = '/uploads/'.$item2;
                if($certificate->save()){
                    $productCertificate = new ProductCertificate();
                    $productCertificate->product_id = $product->id;
                    $productCertificate->certificate_id = $certificate->id;
                    $productCertificate->save();
                }
                $count+=1;

            }
        }

        if($prizes != null){

            foreach ($prizes as $prize){
                $mPrize = new Prize();
                $mPrize->name = 'Prize-'.$count;

                $mPrize->img = '/uploads/'.$prize;
                $mPrize->product_id = $product->id;
                $mPrize->save();


            }
        }
        if($legals != null){

            foreach ($legals as $legalimg){
                $legal = new Legal();
                $legal->name = 'Legal-'.time();
                $legal->slug = 'Legal-'.time();
                $legal->path = '/uploads/'.$legalimg;
                $legal->product_id = $product->id;
                $legal->save();


            }
        }


        return redirect()->to('/company')->with('success', 'Ürün başarıyla eklenmiştir.');
    }

    public function storeProject(Request $request)
    {
        $company = Company::where('user_id',Auth::user()->getAuthIdentifier())->first();
        $slugify = new Slugify();
        $slugify->activateRuleset('turkish')->addRule('_', '2');

        $user = Auth::user()->getAuthIdentifier();
        $image = $request->file('image');

        $imageStatus = imageUploader::multiImage($image);

        if($imageStatus != false){
            $gallery = $imageStatus;
        }

        $project = new Projects();
        $project->name = $request->title;
        $project->project_category_id = $request->category;
        $project->slug = $request->slug != null ? $request->slug:$slugify->slugify($request->title);
        $project->story = $request->story;
        $project->company_id = $company->id;
        $project->country = $request->country;
        $project->year = $request->start_year.'-'.$request->end_year;
        $project->image = 'uploads/'.$gallery[0];


        $project->save();

        //$productID = Product::where('image','/storage/'.$gallery[0])->select('id')->first();


        foreach ($gallery as $item){
            $productGallery = new ProjectGallery();
            $productGallery->project_id = $project->id;
            $productGallery->image = 'uploads/'.$item;
            $productGallery->save();
        }




        return back()->with('success', 'Proje başarıyla eklenmiştir.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Request $req)
    {
       $company = Company::where('slug',$req->slug)->first();
       $user = User::find($company->user_id);


       return view('company.profile',compact('user','company'));
    }

    public function credentials(){
        $company = Company::where('user_id',request()->user()->id)->first();
        return view('company.credentials',compact('company'));
    }
    public function updateCredentials(Request $req){
        try{
            $user = User::find($req->user()->id);
            $company = Company::where('user_id',$user->id)->first();
            $user->name = $req->name;
            $user->phone = $req->phone;
            if($req->password  && $req->password == $req->password_repeat){
                $user->password = bcrypt($req->password);
            }
            $user->save();
            $company->url = $req->url;
            $company->description = $req->description;
            $company->save();

            $req->session()->flash('success','Başarıyla Güncellendi');
            return redirect()->back();

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
