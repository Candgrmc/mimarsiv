<?php

namespace App\Http\Controllers\Auth;

use App\Architect;
use App\Company;
use App\Office;
use App\Student;
use App\User;
use App\Consultant;
use App\Artisan;
use App\Http\Controllers\Controller;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {


	    $slugify = new Slugify();
	    $slugify->activateRuleSet('turkish');

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'user_type_id' => $data['user_type_id'],
            'approved' => 0
        ]);

        switch ($data['user_type_id']){
            case 1:
                $student = new Student();
                $student->user_id = $user->id;
                $student->school = $data['school_name'];
                $student->department = $data['department'];
                $student->save();
                break;
            case 2:

                $architect = new Architect();
                $architect->user_id = $user->id;
                $architect->company_name = $data['acompany_name'];
                $architect->company_phone = $data['acompany_phone'];
                $architect->save();
                break;
            case 4:
                $company = new Consultant();
                $company->company_name = $data['cocompany_name'];
                $company->company_address = $data['coaddress'];
                $company->company_phone = $data['cophone'];
                $company->consultant_category = $data['consultant_category'];
                $company->user_id = $user->id;
                $company->save();
                break;
            case 5:
                $company = new Artisan();
                $company->company_name = $data['arcompany_name'];
                $company->company_address = $data['araddress'];
                $company->company_phone = $data['arphone'];
                $company->artisan_category = $data['artisan_category'];
                $company->user_id = $user->id;
                $company->save();
                break;
            case 6:
                $office = new Office();
                $office->name = $data['office_name'];
                $office->address = $data['ofaddress'];
                $office->phone = $data['ofphone'];
                $office->user_id = $user->id;
                $office->slug = $slugify->slugify($data['office_name'], ['separator' => '-']);
                $office->save();
                break;
            case 7:
                $company = new Company();
                $company->name = $data['ccompany_name'];
                $company->address = $data['caddress'];
                $company->phone = $data['cphone'];
                $company->user_id = $user->id;
                $company->slug = $slugify->slugify($data['ccompany_name'], ['separator' => '-']);
                $company->save();
                break;

        }



        return $user;
    }
}
