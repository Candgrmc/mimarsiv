<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
    protected $fillable = [
    	'name',
	    'slug'
    ];

    public function projects(){
    	return $this->hasMany('App\Projects');
    }
}
