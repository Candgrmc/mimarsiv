<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artisan extends Model
{
    protected $fillable = [
    	'user_id',
	    'company_id'
    ];

	public function user(){
		return $this->belongsTo('App\User','user_id','id');
	}
	public function company(){
		return $this->hasOne('App\Company','id','company_id');
	}

	public function projects(){
		return $this->hasMany(ArtisanProject::class,'artisan_id','id');
	}
	public function category(){
		return $this->belongsTo(ArtisanCategory::class,'artisan_category_id');
	}
    public function references(){
		return $this->belongsToMany('App\Projects','artisan_projects','project_id','id');
	}
}
