<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulletin extends Model
{
	protected $fillable = [
		'user_id',
		'title',
		'slug',
		'content',
		'image',
		'keywords',
		'meta_description',
		'isActive'
	];

	public function user(){
		return $this->belongsTo('App\User');
	}
}
