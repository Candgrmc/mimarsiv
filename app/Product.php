<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = [
    	'featured',
	    'user_id',
	    'product_category_id',
	    'name',
	    'color',
	    'image',
	    'video',
	    'catalogue',
	    'overview',
	    'bim',
	    'price',
	    'slug',
	    'brand_id'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function prizes(){
        return $this->hasMany('App\Prize');
    }
	public function category(){
		return $this->belongsTo('App\ProductCategory','product_category_id','id');
	}
	public function certificates(){
		return $this->belongsToMany('App\Certificate','product_certificates');
	}
	public function gallery(){
		return $this->hasMany('App\ProductGallery');
	}
	public function events(){
		return $this->hasMany('App\ProductEvent');
	}
	public function projects(){
	    return $this->belongsToMany('App\Projects','project_products');
    }

	public function productRequests(){
		return $this->hasMany('App\UserRequest');
	}
	public function brand(){
    	return $this->belongsTo(Brand::class);
	}
	public function userSelected(){
	    return $this->belongsToMany('App\User','user_selected_products')->withPivot('note');
    }



    public function getNote(){
	    return $this->userSelected()->pivot->note;
    }


}
