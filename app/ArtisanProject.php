<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtisanProject extends Model
{
	protected $fillable = [
		'artisan_id',
		'project_id'
	];

	public function artisan(){
		return $this->belongsTo(Artisan::class,'artisan_id');
	}
	public function project(){
		return $this->belongsTo(Projects::class,'project_id');
	}
}
