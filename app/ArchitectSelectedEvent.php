<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchitectSelectedEvent extends Model
{
    protected $fillable = [
    	'architect_id',
	    'event_id'
    ];

    public function architect(){
    	return $this->belongsTo('App\Architect');
    }
	public function event(){
		return $this->belongsTo('App\Event');
	}
}
