<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsultantReference extends Model
{
    protected $fillable = [
    	'consultant_id',
	    'project_id'
    ];

	public function consultant(){
		return $this->belongsTo('App\Consultant');
	}

	public function project(){
		return $this->hasOne('App\Project');
	}
}
