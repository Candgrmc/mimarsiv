<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchitectSelectedProject extends Model
{
	protected $fillable = [
		'architect_id',
		'project_id'
	];

	public function architect(){
		return $this->belongsTo('App\Architect');
	}
	public function project(){
		return $this->belongsTo('App\Project');
	}
}
