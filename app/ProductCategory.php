<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ProductCategory extends Model
{
    protected $fillable = [
    	'title',
	    'slug',
	    'category_id'
    ];

    public function products(){
    	return $this->hasMany('App\Product');
    }
	public function category()
	{
		return $this->belongsTo(ProductCategory::class, 'category_id');
	}

  public function hasSub(){
    $categories = self::all();

    foreach ($categories as $category) {
        if($category->category_id == $this->id){
          return true;
        }
    }

    return false;
  }

  public function subCategories(){
    $categories = self::all();
    $subCategories = [];
    foreach ($categories as $category) {
        if($category->category_id == $this->id){
          array_push($subCategories , $category);
        }
    }

    return collect($subCategories);

  }
}
