<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectConsultant extends Model
{
    protected $fillable = [
    	'project_id',
	    'consultant_id'
    ];

    public function project(){
    	return $this->belongsTo('App\Projects');
    }
	public function consultant(){
    	return $this->hasOne('App\Consultant','id','consultant_id');
	}
}
