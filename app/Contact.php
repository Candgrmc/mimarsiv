<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
    	'latitude',
	    'longitude',
	    'address',
	    'phone',
	    'email'
    ];
}
