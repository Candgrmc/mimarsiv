<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCertificate extends Model
{
    protected $fillable = [
    	'product_id',
	    'certificate_id'
    ];

    public function certificate(){
    	return $this->belongsTo('App\Certificate');
    }
	public function product(){
		return $this->belongsTo('App\Product');
	}
}
