<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $fillable = [
    	'name',
	    'slug'
    ];

    public function products(){
    	return $this->hasMany('App\Product');
    }
    public function projects(){
        return $this->belongsTo('App\Projects','project_certificate');
    }
}
