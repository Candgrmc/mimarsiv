<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
    	'user_id',
	    'title',
	    'slug',
	    'content',
	    'image',
	    'keywords',
	    'meta_description'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
