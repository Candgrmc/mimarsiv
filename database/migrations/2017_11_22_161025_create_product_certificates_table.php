<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_certificates', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('product_id')->unsigned();
	        $table->integer('certificate_id')->unsigned();
	        $table->foreign('product_id')->references('id')->on('products');
	        $table->foreign('certificate_id')->references('id')->on('certificates');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_certificates');
    }
}
