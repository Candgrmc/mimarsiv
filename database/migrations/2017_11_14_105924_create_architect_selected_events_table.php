<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchitectSelectedEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('architect_selected_events', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('architect_id')->unsigned();
	        $table->integer('event_id')->unsigned();
	        $table->foreign('architect_id')->references('id')->on('architects');
	        $table->foreign('event_id')->references('id')->on('events');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('architect_selected_events');
    }
}
