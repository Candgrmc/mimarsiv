<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectConsultantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_consultants', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('project_id')->unsigned();
	        $table->integer('consultant_id')->unsigned();
	        $table->foreign('consultant_id')->references('id')->on('consultants');
	        $table->foreign('project_id')->references('id')->on('projects');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_consultants');
    }
}
