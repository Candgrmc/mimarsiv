<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchitectSelectedProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('architect_selected_projects', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('architect_id')->unsigned();
	        $table->integer('project_id')->unsigned();
	        $table->foreign('architect_id')->references('id')->on('architects');
	        $table->foreign('project_id')->references('id')->on('projects');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('architect_selected_projects');
    }
}
