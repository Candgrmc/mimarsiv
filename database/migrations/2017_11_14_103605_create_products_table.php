<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('featured');
            $table->integer('user_id')->unsigned();
            $table->integer('product_category_id')->unsigned();
            $table->integer('certificate_id')->unsigned();
			$table->string('name');
			$table->string('slug');
			$table->string('color');
			$table->string('image');
			$table->string('brand');
			$table->string('video');
			$table->string('catalogue');
			$table->text('overview');
			$table->text('bim');
			$table->string('price');
	        $table->foreign('user_id')->references('id')->on('users');
	        $table->foreign('product_category_id')->references('id')->on('product_categories');
	        $table->foreign('certificate_id')->references('id')->on('certificates');
	        $table->timestamps();
	        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
