<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchitectSelectedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('architect_selected_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('architect_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->foreign('architect_id')->references('id')->on('architects');
            $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('architect_selected_products');
    }
}
