<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_type_id')->unsigned();
            $table->integer('project_category_id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->text('story');
            $table->integer('company_id')->unsigned();
            $table->unsignedInteger('consultant_id')->nullable();
            $table->unsignedInteger('investor_id')->nullable();
            $table->integer('year');
            $table->string('country');
	        $table->foreign('company_id')->references('id')->on('companies');
	        $table->foreign('consultant_id')->references('id')->on('consultants');
	        $table->foreign('investor_id')->references('id')->on('investors');
	        $table->foreign('project_category_id')->references('id')->on('project_categories');
	        $table->foreign('project_type_id')->references('id')->on('project_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
