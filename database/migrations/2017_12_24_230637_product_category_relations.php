<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductCategoryRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('product_categories', function(Blueprint $table) {
		    if (!Schema::hasColumn('product_categories', 'category_id')) {
			    $table->integer('category_id')->unsigned()->nullable();
			    $table->foreign('category_id')->references('id')->on('product_categories')->onDelete('cascade');
		    }

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
