@extends('layouts.app')
<header id="header">
    @include("layouts.top")
    @include("layouts.menu")
</header>
@section('content')
    <main id="main">
        <div class="main-banner">
            <div class="banner-carousel">
                <div class="mask">
                    <div class="slideset">
                        @foreach($sliders as $slider)
                            <div class="slide">
                                <img src="{{$slider->image}}" alt="banner image">
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="pagination"></div>
            </div>
        </div>
        <div class="container">

                <div class="row">
                  <div class="col-md-3">
                      <div class="breadcrumb-area">
                          <ol class="breadcrumb">
                              <li><a href="/">ANASAYFA</a></li>
                              <li><a href="#">PROJELER</a></li>

                          </ol>
                      </div>
                  </div>
                </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12  col-xs-12 col-sm-12">
                      <div class="search-filter-area">
                          <div class="select-inputs-wrap">
                              <div class="select-input size1">
                                  <select>
                                      <option>MİMARİ OFİS</option>
                                      <option>MİMARİ OFİS 1</option>
                                      <option>MİMARİ OFİS 2</option>
                                      <option>MİMARİ OFİS 3</option>
                                  </select>
                              </div>
                              <div class="select-input size2">
                                  <select>
                                      <option>TİP</option>
                                      <option>TİP 1</option>
                                      <option>TİP 2</option>
                                      <option>TİP 3</option>
                                  </select>
                              </div>
                              <div class="select-input size2">
                                  <select>
                                      <option>YIL</option>
                                      <option>YIL 1</option>
                                      <option>YIL 2</option>
                                      <option>YIL 3</option>
                                  </select>
                              </div>

                              <div class="select-input size1">
                                  <select>
                                      <option>DANIŞMAN</option>
                                      <option>DANIŞMAN 1</option>
                                      <option>DANIŞMAN 2</option>
                                      <option>DANIŞMAN 3</option>
                                  </select>
                              </div>

                              <div class="select-input size1">
                                  <select>
                                      <option>ÜLKE</option>
                                      <option>ÜLKE 1</option>
                                      <option>ÜLKE 2</option>
                                      <option>ÜLKE 3</option>
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>

        </div>

        <div class="search-result-info">
            <div class="container">
                <div class="results-info">
                    <p>Results: {{$projects->count()}}</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <ul class="sidebar-nav">
                      @foreach($categories as $category)

                          @if($category->category_id == null)
                              <li >
                                  <a href="{{url('category/'.$category->slug)}}">{{$category->title}}</a> <a href="#" class="category-toggle"> @if($category->hasSub())<span class="caret"></span> @endif </a>
                                  <ul class="sub-menu" style="display:none;margin-left:-20px">
                                    @if($category->hasSub())
                                      @foreach($category->subCategories() as $subcategory)


                                          <li  ><a href="{{url('category/'.$subcategory->slug)}}">{{$subcategory->title}}</a> <a href="#" class="category-toggle">@if($subcategory->hasSub())<span class="caret"></span> @endif</a>
                                            <ul style="display:none;margin-left:-20px">
                                              @if($subcategory->hasSub())
                                              @foreach($subcategory->subCategories() as $sub1category)

                                                  <li class="category_sub_menu_2" ><a href="{{url('category/'.$sub1category->slug)}}">{{$sub1category->title}}</a> <a href="#">@if($sub1category->hasSub())<span class="caret"></span> @endif</a>
                                                    <ul style="display:none;margin-left:-20px">
                                                      @if($sub1category->hasSub())
                                                      @foreach($sub1category->subCategories() as $sub2category)

                                                          <li class="category_sub_menu_3" >
                                                            <a href="{{url('category/'.$sub2category->slug)}}">{{$sub2category->title}}</a>
                                                            @if($sub2category->hasSub())
                                                            @foreach($sub2category->subCategories() as $sub3category)

                                                                <li class="category_sub_menu_2" ><a href="{{url('category/'.$sub1category->slug)}}">{{$sub3category->title}}</a> <a href="#">@if($sub3category->hasSub())<span class="caret"></span> @endif</a>
                                                                  <ul style="display:none;margin-left:-20px">
                                                                    @if($sub3category->hasSub())
                                                                    @foreach($sub3category->subCategories() as $sub4category)

                                                                        <li class="category_sub_menu_3" >
                                                                          <a href="{{url('category/'.$sub4category->slug)}}">{{$sub4category->title}}</a>@if($sub4category->hasSub())<span class="caret"></span> @endif</a>

                                                                          @if($sub4category->hasSub())
                                                                          @foreach($sub4category->subCategories() as $sub5category)

                                                                              <li class="category_sub_menu_3" ><a href="{{url('category/'.$sub5category->slug)}}">{{$sub5category->title}}</a></li>

                                                                          @endforeach
                                                                          @endif

                                                                        </li>

                                                                    @endforeach
                                                                    @endif
                                                                  </ul>
                                                                </li>

                                                            @endforeach
                                                            @endif
                                                          </li>

                                                      @endforeach
                                                      @endif
                                                    </ul>
                                                  </li>

                                              @endforeach
                                              @endif
                                            </ul>
                                          </li>

                                      @endforeach
                                          @endif
                                  </ul>
                              </li>
                          @endif

                      @endforeach
                    </ul>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="image-gallery gallery-type-4">
                        @foreach($projects as $project)
                        <div class="gallery-item col-md-3 col-sm-3 col-xs-3" style="height: 188px;">
                            <a href="project_detail/{{$project->slug}}" class="item">
                                <img src="{{$project->image}}" alt="image" width="248" height="188">
                                <span class="head">{{$project->name}}</span>
                                <span class="text">
                                    @if($project->company){{$project->company->name}} <br>@endif
                                    @if($project->category){{$project->category->title}}@endif <br>
                                </span>
                            </a>
                        </div>
@endforeach

                </div>
            </div>
        </div>
    </main>

    <script type="text/javascript">
    $(document).ready(function(){


    $('.category-toggle').on('click',function(e){
      console.log('clicked')
      e.preventDefault();
      $(this).siblings('ul').slideToggle()
    })
    });
    </script>
@endsection
