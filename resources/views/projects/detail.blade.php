@extends('layouts.app')
<div id="wrapper" class="common-page proje-sayfasi"><br>
<header id="header">
    @include("layouts.top")
    @include("layouts.menu")
</header>
@section('content')
    <div class="container">
        <div class="row">


            <div class="col-sm-5 col-md-4">
                <div class="sidebar-box margin-bottom-20">

                        <div class="icon-box" style="padding: 10px">

                            <img src="@if($project->company->user && $project->company->user->image){{asset($project->company->user->image)}} @else {{asset('images/default.jpeg')}} @endif" alt="client" width="130" height="87">

                        </div>


                    @if($project->company)
                    <p>{{$project->company->name}}</p>
                    <address>
                        <strong>Adres :</strong>{{$project->company->address}} <br>
                         <strong>Telefon :</strong> {{$project->company->phone}} <br>
                        <strong>Web :</strong>{{$project->company->url}}
                    </address>
                    @endif


                </div>


            <div class="sidebar-box">
                <span class="title">{{trans('main.otherprojectsofcompany')}}</span>
                <ul class="list-items">
                  @php($otherProjects = App\Projects::where('user_id',$project->user_id)->orderBy('created_at','desc')->limit(10)->get())

                @foreach($otherProjects as $cproject)
                        <li><a href="/project_detail/{{$cproject->slug}}">{{$cproject->name}}</a></li>

                    @endforeach
                </ul>
            </div>


            </div>

            <div class="col-sm-7 col-md-8">
                <h1 class="top-title">{{$project->name}}</h1>

                <div class="linked-gallery-head" style="max-height:500px;overflow: hidden;">
                    @foreach($gallery as $item)
                    <div class="image-item">
                      <a href="{{asset($item->image)}}" data-lightbox="{{asset($item->image)}}" data-title="{{$project->name}}">
                        <img src="{{asset($item->image)}}" height="400" alt="gallery-item">
                      </a>
                    </div>
                    @endforeach()
                </div>

                <div class="linked-gallery-pager">
                    @foreach($gallery as $item)
                    <div class="pager-item">
                        <img src="{{asset($item->image)}}" height="93" width="114" alt="pager item">
                    </div>
                    @endforeach()
                </div>
                  <ul class="tab-list">
                    <li class="active"><a href="#tab-item-01" data-toggle="tab">{{trans('main.project_story')}}</a></li>
                    <li><a href="#tab-item-02" data-toggle="tab">{{trans('main.project_kunye')}}</a></li>
                  </ul>

                <div class="tab-content">
                  <article class="tab-pane active article padding-bottom-20" id="tab-item-01">{!! $project->story !!}</article>
                  <article class="tab-pane article padding-bottom-20" id="tab-item-02">{!! $project->tag !!}</article>
              </div>
                <h3 class="article-h3">PROJEDE KULLANILAN ÜRÜNLER</h3>

                <div class="image-gallery gallery-type-2 padding-bottom-15" >



                            @foreach($project->products as $product)
                        <div class="gallery-item" >
                            <a href="#" class="item" style="height: 141px;">
                                <img src="{{$product->image}}" alt="image" width="188" height="141">
                                <span class="head">{{$product->name}}</span>

                                    @if($product->brand_id)
                                        @php($brand = App\Brand::find($product->brand_id))
                                    @endif
                                    @if(isset($brand))
                                        <span class="text">{{$brand->name}}</span>

                                    @endif

                            </a>
                        </div>
                            @endforeach()

                </div>

                <h3 class="article-h3">PROJEDE YER ALAN DANIŞMANLAR</h3>
                <div class="image-gallery gallery-type-2">


                        @foreach($project->consultants as $consultant)


                        <div class="gallery-item col-md-3 col-xs-3 col-sm-3">
                            <a href="{{url('consultant/'.$consultant->id)}}" class="item" style="height: 141px;">
                                <img src="@if($consultant->image){{asset($consultant->image)}} @else {{asset('images/default.jpeg')}} @endif" alt="image" width="188" height="141">
                                <span class="head">{{$consultant->name}}</span>
                                <span class="text">
                                    @if($consultant->company)
                                   {{$consultant->company->name}}<br>
                                   @endif
                                </span>
                            </a>
                              </div>
                          @endforeach



                </div>

            </div>
        </div>
    </div>
    </div>
@endsection
