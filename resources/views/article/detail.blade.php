
@extends('layouts.app')
<div id="wrapper" class="common-page roportaj-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="row">
              @if($user && $consultant)
                <div class="col-sm-5 col-md-4">

                    <div class="sidebar-box margin-bottom-20">
                        <div class="icon-box">
                            <img src="@if($user->image){{asset($user->image)}} @else {{asset('images/default.jpeg')}} @endif" alt="client" width="122" height="46">
                        </div>
                        <p>{{$consultant->company_name}}</p>
                        <address>
                            <strong>Telefon : </strong>{{$consultant->company_phone}} <br>
                            <strong>Adres : </strong>{{$consultant->company_address}} <br>
                            <strong>Web : </strong>{{$consultant->company_url}}
                        </address>
                    </div>




                        <div class="sidebar-box">
                        <h2 class="sidebar-title">REFERENCE PROJECTS</h2>
                        <div class="image-gallery gallery-type-5 padding-bottom-15">
                            @foreach($user->references as $project)
                            <div class="gallery-item" style="height: 200px;width:200px ">
                                <a href="/project_detail/{{$project->slug}}" class="item" style="object-fit:contain">
                                    <img src="{{asset($project->image)}}" alt="image" height="141" >
                                    <span class="head">{{$project->name}}</span>
                                    @if($project->company)
                                    <span class="text">
                                    {{$project->company->name}} <br>
                                        {{$project->category->title}} <br>
                                </span>
                                        @endif()
                                </a>
                            </div>
                                @endforeach
                        </div>
                    </div>

                </div>
                @endif
                <div class="@if($user && $consultant)col-sm-7 col-md-8 @else col-md-12 col-sm-12 col-xs-12  @endif">
                    <div class="top-normal-banner margin-bottom-5">
                        <img src="{{asset($article->detail_image)}}" alt="image banner" width="770" height="399">
                    </div>
                    <article class="article padding-bottom-30">
                     {!! $article->content !!}
                    </article>
                    <h3 class="article-h3">DİĞER RÖPORTAJLAR</h3>

                    <div class="image-gallery gallery-type-2">
                      @foreach($articles as $item)
                      <div class="gallery-item col-md-3 col-sm-3 col-xs-3" style="height:200px">
                          <a href="/article/{{$item->slug}}" class="item" style="object-fit:contain">
                              <img src="{{asset($item->image)}}" alt="image" width="188" height="141">
                              <span class="head">{{$item->title}}</span>
                              <span class="text">

                              </span>
                          </a>
                      </div>
                     @endforeach

                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
