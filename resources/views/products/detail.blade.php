
@extends('layouts.app')
<div id="wrapper" class="common-page proje-sayfasi">
    <header id="header">

        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')

        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-md-4">
                    @if($company)
                    <div class="sidebar-box margin-bottom-30">
                        <div class="icon-box">
                            <img src="@if($company && $company->logo) {{$company->logo}} @elseif($product->user && $product->user->image) {{asset($product->user->image)}}  @else{{ asset('images/default.jpeg')}} @endif" alt="client" width="94" height="47" style="vertical-align: middle !important;">
                        </div>
                        <p>{{$company->name}}</p>
                        <address>

                          <strong>{{trans('main.address')}}: </strong>  {{$company->address}} <br>
                          <strong>{{trans('main.phone')}}: </strong>  {{$company->phone}}<br>
                          <strong>Website: </strong>  {{$company->url}}
                        </address>
                    </div>
                    @endif()


                    @if(Auth::check())
                    <div class="sidebar-box">
                        <form action="#" class="find-retailers">
                            <div class="input-field-wrapper">

                                <div class="row" style="padding:10px" onclick="alert('Test')">
                                  <button type="button" class="btn btn-default" name="button">REQUEST PRICE</button>
                                </div>
                                <div class="row" style="padding:10px">
                                  <button type="button" class="btn btn-default" name="button">REQUEST CATALOGOUES</button>
                                </div>
                                <div class="row" style="padding:10px">
                                  <button type="button" class="btn btn-default" name="button">REQUEST SAMPLE</button>
                                </div>
                                <div class="row" style="padding:10px">
                                  <button type="button" class="btn btn-default" name="button">REQUEST VİSİT</button>
                                </div>


                            </div>
                        </form>
                    </div>
                    @endif


                        @if($company)

                    <div class="sidebar-box">
                        <span class="title">{{trans('main.otherproductsofcompany')}}</span>
                        <ul class="list-items">
                        @php $products = \App\Http\Controllers\ProductsController::getCompanyProducts($company->user_id) @endphp
                        @if($products)
                        @foreach($products as $cproduct)
                                <li><a href="/product/{{$product->slug}}">{{$cproduct->name}}</a></li>

                            @endforeach
                        </ul>
                    </div>
                            @endif()
                        @endif
                </div>
                <div class="col-sm-7 col-md-8">
                    <h1 class="top-title">{{$product->name}}</h1>
                    <div class="linked-gallery-head" style="height:500px">
                        @foreach($images as $image)
                            <div class="image-item">
                              <a href="{{asset($image->image)}}" data-lightbox="{{asset($image->image)}}" data-title="{{$product->name}}">
                                <img src="{{$image->image}}" height="300" class="gallery-item">
                                </a>
                            </div>
                            @endforeach


                    </div>

                    <div class="linked-gallery-pager ">
                        @foreach($images as $image)
                        <div class="pager-item">
                            <img src="{{$image->image}}" height="93" width="114" class="pager item">
                        </div>
                        @endforeach
                    </div>


                    <ul class="tab-list">
                        <li class="active"><a href="#tab-item-01" data-toggle="tab">{{trans('main.overview')}}</a></li>
                        <li><a href="#tab-item-02" data-toggle="tab">{{trans('main.prices')}}</a></li>
                        <li><a href="#tab-item-03" data-toggle="tab">BİM</a></li>
                        <li><a href="#tab-item-04" data-toggle="tab">{{trans('main.prizes')}}</a></li>
                        <li><a href="#tab-item-05" data-toggle="tab">{{trans('main.certificates')}}</a></li>
                        <li><a href="#tab-item-06" data-toggle="tab">{{trans('main.catalog')}}</a></li>
                        <li><a href="#tab-item-07" data-toggle="tab">{{trans('main.video')}}</a></li>
                        <li><a href="#tab-item-08" data-toggle="tab">MTS</a></li>
                    </ul>
                    <div class="tab-content" style="margin-bottom:20px">
                        <article id="tab-item-01" class="tab-pane active article padding-bottom-15">
                            {!! $product->overview !!}
                        </article>
                        <article id="tab-item-02" class="tab-pane article">
                            {{$product->price}}
                        </article>
                        <article id="tab-item-03" class="tab-pane article">
                            @if($product->bim)<a href="{{asset($product->bim)}}">BIM</a>@endif
                            </article>
                        <article id="tab-item-04" class="tab-pane article">
                            <div class="row">
                            @foreach($product->prizes as $prize)
                                <div class="col-md-3">
                                    <a href="{{asset($prize->img)}}"><img src="{{$prize->img}}" height="93" width="114" alt="pager item"></a>
                                </div>
                            @endforeach
                            </div>
                           </article>
                        <article id="tab-item-05" class="tab-pane article">
                                <div class="row">
                                    @foreach($product->certificates as $certificate)

                                        <div class="col-md-3">
                                            <a href="{{asset($certificate->img)}}"><img src="{{$certificate->img}}" height="93" width="114" alt="pager item"></a>
                                        </div>
                                    @endforeach
                                </div>
                           </article>
                        <article id="tab-item-06" class="tab-pane article">
                            @if($product->catalogue)
                                <a href="{{asset($product->catalogue)}}">{{trans('main.catalog')}}</a>
                            @endif
                        </article>
                        <article id="tab-item-07" class="tab-pane article">

                        </article>

                        <article id="tab-item-08" class="tab-pane article col-md-12">
                            @foreach($legals as $legal)
                                @php
                                    $urlParse = explode('.',$legal->path);
                                    $fileType = array_pop($urlParse);
                                @endphp
                            @if($fileType == 'pdf')

                                    <div class="col-md-3">
                                        <a href="{{$legal->path}}"><i class="glyphicon glyphicon-file"></i> File</a>
                                    </div>
                            @else
                                <div class="col-md-3">
                                    <a href="{{asset($legal->path)}}">{{$fileType}}<img src="{{$legal->path}}" height="93" width="114" alt="pager item"></a>
                                </div>
                            @endif
                            @endforeach
                         </article>



                    </div>


                    <h3 class="article-h3">{{trans('main.reference_projects')}}</h3>
                    <div class="image-gallery gallery-type-2 padding-bottom-15">

                        @foreach($product->projects as $project)


                                <div class="gallery-item col-md-3 col-xs-3 col-sm-3" style=" height: 188px;">
                                    <a href="/project_detail/{{$project->slug}}" class="item">
                                        <img src="{{asset($project->image)}}" alt="image" width="248" height="188">
                                        <span class="head">{{$project->name}}</span>
                                        <span class="text">
                                        {{$project->company->name}} <br>
                                            {{$project->category->title}} <br>
                                </span>
                                    </a>
                                </div>

                            @endforeach()

                    </div>
                </div>
            </div>
        </div>
</div>

@endsection
