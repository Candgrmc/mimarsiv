
@extends('layouts.app')
<div id="wrapper" class="common-page proje-listeleme">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
    <div class="main-banner">
        <div class="banner-carousel">
            <div class="mask">
                <div class="slideset">
                    @foreach($sliders as $slider)
                    <div class="slide">
                        <a href="@if($slider->redirectTo){{$slider->redirectTo}}@else javascript:void(0) @endif"><img src="{{asset($slider->image)}}" alt="banner image"></a>
                    </div>
                    @endforeach

                </div>
            </div>
            <div class="pagination"></div>
        </div>
    </div>
        <div class="container">


            <div class="row">
                <div class="col-lg-3">
                    <div class="breadcrumb-area">
                        <ol class="breadcrumb">
                            <li><a href="/">ANASAYFA</a></li>
                            <li class="active">ÜRÜNLER</li>
                        </ol>
                    </div>
                </div>

            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="search-filter-area">
                      <div class="select-inputs-wrap">
                          <div class="select-input size1">
                              <select>
                                  <option>MARKA</option>
                                  <option>MARKA 1</option>
                                  <option>MARKA 2</option>
                                  <option>MARKA 3</option>
                              </select>
                          </div>
                          <div class="select-input size2">
                              <select>
                                  <option>ÜLKE</option>
                                  <option>ÜLKE 1</option>
                                  <option>ÜLKE 2</option>
                                  <option>ÜLKE 3</option>
                              </select>
                          </div>
                          <div class="select-input size1">
                              <select>
                                  <option>SERTİFİKA</option>
                                  <option>SERTİFİKA 1</option>
                                  <option>SERTİFİKA 2</option>
                                  <option>SERTİFİKA 3</option>
                              </select>
                          </div>
                          <br>
                          <div class="select-input size2">
                              <select>
                                  <option>PROJE</option>
                                  <option>PROJE 1</option>
                                  <option>PROJE 2</option>
                                  <option>PROJE 3</option>
                              </select>
                          </div>
                          <div class="select-input size2">
                              <select>
                                  <option>FİYAT</option>
                                  <option>FİYAT 1</option>
                                  <option>FİYAT 2</option>
                                  <option>FİYAT 3</option>
                              </select>
                          </div>
                          <div class="select-input size2">
                              <select>
                                <option>BİM</option>
                                  <option>Var</option>
                                  <option>Yok</option>

                              </select>
                          </div>

                      </div>
                  </div>
              </div>
            </div>
        </div>

        <div class="search-result-info">
            <div class="container">
                <div class="results-info">
                    <p>Results: {{count($products)}}</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <h1 class="sidebar-title">KATEGORİLER</h1>
                    <div class="dropdown">
                      <ul class="sidebar-nav text-capitalise">


                        @foreach($categories as $category)

                            @if($category->category_id == null)
                                <li >
                                    <a href="{{url('category/'.$category->slug)}}">{{$category->title}}</a> <a href="#" class="category-toggle"> @if($category->hasSub())<span class="caret"></span> @endif </a>
                                    <ul class="sub-menu" style="display:none;margin-left:-20px">
                                      @if($category->hasSub())
                                        @foreach($category->subCategories() as $subcategory)


                                            <li  ><a href="{{url('category/'.$subcategory->slug)}}">{{$subcategory->title}}</a> <a href="#" class="category-toggle">@if($subcategory->hasSub())<span class="caret"></span> @endif</a>
                                              <ul style="display:none;margin-left:-20px">
                                                @if($subcategory->hasSub())
                                                @foreach($subcategory->subCategories() as $sub1category)

                                                    <li class="category_sub_menu_2" ><a href="{{url('category/'.$sub1category->slug)}}">{{$sub1category->title}}</a> <a href="#">@if($sub1category->hasSub())<span class="caret"></span> @endif</a>
                                                      <ul style="display:none;margin-left:-20px">
                                                        @if($sub1category->hasSub())
                                                        @foreach($sub1category->subCategories() as $sub2category)

                                                            <li class="category_sub_menu_3" >
                                                              <a href="{{url('category/'.$sub2category->slug)}}">{{$sub2category->title}}</a>
                                                              @if($sub2category->hasSub())
                                                              @foreach($sub2category->subCategories() as $sub3category)

                                                                  <li class="category_sub_menu_2" ><a href="{{url('category/'.$sub1category->slug)}}">{{$sub3category->title}}</a> <a href="#">@if($sub3category->hasSub())<span class="caret"></span> @endif</a>
                                                                    <ul style="display:none;margin-left:-20px">
                                                                      @if($sub3category->hasSub())
                                                                      @foreach($sub3category->subCategories() as $sub4category)

                                                                          <li class="category_sub_menu_3" >
                                                                            <a href="{{url('category/'.$sub4category->slug)}}">{{$sub4category->title}}</a>@if($sub4category->hasSub())<span class="caret"></span> @endif</a>

                                                                            @if($sub4category->hasSub())
                                                                            @foreach($sub4category->subCategories() as $sub5category)

                                                                                <li class="category_sub_menu_3" ><a href="{{url('category/'.$sub5category->slug)}}">{{$sub5category->title}}</a></li>

                                                                            @endforeach
                                                                            @endif

                                                                          </li>

                                                                      @endforeach
                                                                      @endif
                                                                    </ul>
                                                                  </li>

                                                              @endforeach
                                                              @endif
                                                            </li>

                                                        @endforeach
                                                        @endif
                                                      </ul>
                                                    </li>

                                                @endforeach
                                                @endif
                                              </ul>
                                            </li>

                                        @endforeach
                                            @endif
                                    </ul>
                                </li>
                            @endif

                        @endforeach
                      </ul>
                    </div>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="image-gallery gallery-type-4">
                        @foreach($products as $product)
                        <div class="gallery-item col-md-3 col-sm-3 col-xs-2" style="height: 188px;">
                            <a href="/product/{{$product->slug}}" class="item">
                                <img src="{{$product->image}}" alt="image" width="188" height="141">
                                <span class="head">{{$product->name}}</span>
                                <span class="text">
                                    @if($product->user && $product->user->company)
                                  {{$product->user->company->name}} <br>

                                   {{$product->category->title}} <br>
                                   @endif()
@if($product->brand)
                                   {{$product->brand->name}}
    @endif()

                                </span>
                            </a>
                        </div>
                            @endforeach()
                    </div>
                </div>
            </div>
        </div>
</div>

<script type="text/javascript">
$(document).ready(function(){


$('.category-toggle').on('click',function(e){
  console.log('clicked')
  e.preventDefault();
  $(this).siblings('ul').slideToggle()
})
});


</script>
</script>
@endsection
