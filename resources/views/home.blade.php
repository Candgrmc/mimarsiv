@extends('layouts.app')
<header id="header">
    @include("layouts.top")
    @include("layouts.menu")
</header>
@section('content')
    <main id="main">
        <div class="main-banner">
            <div class="banner-carousel">
                <div class="mask">
                    <div class="slideset">
                        @foreach($sliders as $slider)
                        <div class="slide">
                            <a href="@if($slider->redirectTo){{$slider->redirectTo}}@else javascript:void(0) @endif"><img src="{{asset($slider->image)}}" alt="banner image"></a>
                        </div>
                        @endforeach

                    </div>
                </div>
                <div class="pagination"></div>
            </div>
        </div>
        <style media="screen">
          .like-btn{
            position:absolute; left:35 ;bottom:18;font-size:34px;color:#fff; text-shadow:5px 5px 5px #000 ; display:none  ;
          }
          .like-btn:hover{
            text-decoration: none;
            color:#fff;
          }

        </style>
        <div class="showcase-block">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.new_arrivals')}}</h1>
                <div class="row">
                  <div class="image-gallery gallery-type-4 ">

                    @foreach($products as $product)
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <a href="product/{{$product->slug}}" class="img-block products" style="background-size: cover;  height: 264px; margin: 10px;">
                            <img src="{{asset($product->image)}}" alt="{{$product->name}}">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="title">{{$product->name}}</span>
                                    <span class="text">

                                    </span>
                                </div>
                            </div>
                        </a>
                        @if(request()->user() && !request()->user()->likedProducts->contains($product->id) && (request()->user()->user_type_id == 1  || request()->user()->user_type_id == 2))
                        <a href="{{url('like_product/'.$product->id)}}" class="like-btn" style=" "><i class="fa fa-thumbs-up"></i> </a>
                        @endif
                    </div>
                    @endforeach
                  </div>
                </div>
            </div>
        </div>

        <div class="showcase-block green">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.latest_news')}}</h1>
                <div class="row">

                      <div class="image-gallery gallery-type-4 ">
                        @foreach($news as $new)
                        <div class="col-md-4 col-sm-4 col-xs-4" style="height: 240px;">
                            <a href="/news/{{$new->slug}}" class="item" style="background-size: cover ; background-repeat: no-repeat;  object-fit: cover;">
                                <img src="{{asset($new->main_image)}}" alt="image"  >
                                <span class="head">{{$new->title}}</span>

                            </a>
                        </div>
                        @endforeach()

                      </div>


                </div>
            </div>
        </div>

        <div class="showcase-block alternate">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.interview')}}</h1>
                <div class="row">
                  <div class="image-gallery gallery-type-4 ">
                    @foreach($interviews as $interview)
                    <div class="col-md-4 col-sm-4 col-xs-4" style="height: 240px;">
                        <a href="/interviews/{{$interview->slug}}" class="item" style="background-size: cover ; background-repeat: no-repeat;  object-fit: cover;">
                            <img src="{{asset($interview->image)}}" alt="image"  >
                            <span class="head">{{$interview->title}}</span>

                        </a>
                    </div>
                    @endforeach()

                  </div>


                </div>
            </div>
        </div>

        <div class="showcase-block green">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.mimworld')}}</h1>
                <div class="row">
                    <div class="col-xs-12 col-md-5 col-sm-5"  >
                      <h3 style="color:#fff">Mimshots</h3>
                      <div class="image-gallery gallery-type-4 ">
                        @php($countMS = 0)
                      @foreach($insta_photos as $key => $value)

                      @if($value->type == 'Mimshots' && $countMS<6 )
                      <div class="col-md-4 col-sm-3 col-xs-4" style="height:120px !important; margin-bottom:15px; background-color:#fff; padding:5px;">
                        <a href="{{($value->owner) ? 'http://instagram.com/'.substr($value->owner,1) : 'http://instagram.com/explore/tags/'.substr($value->hashtag,1)}}" target="_blank" class="item" style="background-size: cover ; background-repeat: no-repeat;  object-fit: cover;">
                        <img src="{{$value->url}}" alt="image" >
                        <span class="head">{{($value->owner) ? $value->owner : $value->hashtag}}</span>
                      </a>
                      </div>
                      @php($countMS++)
                      @endif

                      @endforeach
                    </div>
                    </div>
                    <div class="col-xs-12 col-md-5 col-sm-5 col-md-offset-1" >
                      <h3 style="color:#fff">Miminsta</h3>
                      <div class="image-gallery gallery-type-4 ">
                        @php($countMI = 0)
                          @foreach($insta_photos as $key => $value)

                          @if($value->type == 'Miminsta' && $countMI<6 )
                          <div class="col-md-4 col-sm-3 col-xs-4" style="height:120px !important; margin-bottom:15px; background-color:#fff; padding:5px;">
                            <a href="{{($value->owner) ? 'http://instagram.com/'.substr($value->owner,1) : 'http://instagram.com/explore/tags/'.substr($value->hashtag,1)}}" class="item" target="_blank" style="background-size: cover ; background-repeat: no-repeat;  object-fit: cover;">
                            <img src="{{$value->url}}" alt="image"  >
                            <span class="head">{{($value->owner) ? $value->owner : $value->hashtag}}</span>
                          </a>
                          </div>
                          @php($countMI++)
                          @endif

                          @endforeach
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="showcase-block ">
            <div class="container">
              <h1 class="showcase-title">{{trans('main.projects')}}</h1>
              @foreach($projects as $project)
              <div class="col-xs-12 col-sm-4 col-md-4">
                  <a href="project_detail/{{$project->slug}}" class="img-block projects" style="background-size: cover;   height: 264px; margin: 10px">
                      <img src="{{$project->image}}" alt="image" >
                      <div class="overlay">
                          <div class="inner">
                              <span class="title">{{$project->name}}</span>
                              <span class="text">

                              </span>
                          </div>
                      </div>
                  </a>
                  @if(request()->user() && !request()->user()->likedProjects->contains($project->id) && (request()->user()->user_type_id == 1  || request()->user()->user_type_id == 2))
                  <a href="{{url('like_project/'.$project->id)}}" class="like-btn" style=" "><i class="fa fa-thumbs-up"></i> </a>
                  @endif
                </div>
              @endforeach
            </div>
          </div>
          <div class="showcase-block alternate green">
              <div class="container">
                  <h1 class="showcase-title">{{trans('main.consultants')}}</h1>
                  <div class="row">
                    <div class="image-gallery gallery-type-4 ">
                      @if($article)
                      <div class="col-xs-12 col-md-6">
                          <a href="{{url('article/'.$article->slug)}}" class="item projects" style="height: 300px !important;background-size:cover">
                            <img src="@if($article->image) {{asset($article->image)}} @endif" alt="image" style="object-fit:cover">
                              <span class="head" style="font-size:20px">{{$article->title}}</span>
                          </a>
                      </div>

                      <div class="col-xs-12 col-md-6">
                          <article class="article" style="font-size:16px">
                              <p>{!! str_limit($article->content, 700, '<br>')!!}</p>
                              <a href="{{url('article/'.$article->slug)}}" class="pull-right" style="color:#fff"> Makalenin Devamı.. </a>
                          </article>
                      </div>
                      @endif
                    </div>
                  </div>
              </div>
          </div>

        <div class="share-block green" style="padding:20px">
            <div class="container">
                <h2>#share</h2>
                <h3>{{trans('main.this_month_concept')}}<strong>
                  @if(isset($concept) && $concept)
                    #{{$concept}}
                  @endif
                  </strong></h3>
                <div class="row">
                  <div class="image-gallery gallery-type-4 ">
                    @php($cCount=0)
                  @foreach($insta_photos as $index => $value)
                    @if($value->type == 'Hashtag' && $cCount<6 )
                    <div class="col-md-2 col-sm-2 col-xs-2" style="height:120px !important; margin-bottom:15px;">
                      <a href="{{($value->owner) ? 'http://instagram.com/'.substr($value->owner,1) : 'http://instagram.com/explore/tags/'.substr($value->hashtag,1)}}" class="item" style="background-size: cover ; background-repeat: no-repeat;  object-fit: cover;">
                      <img src="{{$value->url}}" alt="image"  >
                      <span class="head">{{($value->owner) ? $value->owner : $value->hashtag}}</span>
                    </a>
                    </div>
                    @php($cCount++)
                    @endif

                @endforeach
              </div>

                </div>
            </div>
        </div>

        <div class="showcase-block alternate green">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.trends')}}</h1>
                <div class="row">
                  @if($architect_article)
                    <div class="col-xs-12 col-md-6">
                      <h2 style="color:#fff">Mimarın Gözünden</h2>
                        <a href="{{url('article/'.$architect_article->slug)}}" class="img-block" style="background-size:contain;background-repeat:no-repeat; height:300px">
                            <img src="{{$architect_article->image}}" alt="image" height="300">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="title">{{$architect_article->title}}</span>
                                    <span class="text">
                                        <i class="fa fa-fw fa-eye fa-3x"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endif
                    @if($office_article)
                    <div class="col-xs-12 col-md-6">
                      <h2 style="color:#fff">Malzemecinin Gözünden</h2>
                        <a href="{{url('article/'.$office_article->slug)}}" class="img-block" height="300" style="background-size:contain;background-repeat:no-repeat; height:300px">
                            <img src="{{asset($office_article->image)}}" alt="image" style="object-fit:contain" height="341">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="title">{{$office_article->title}}</span>
                                    <span class="text">
                                        <i class="fa fa-fw fa-eye fa-3x"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="showcase-block ">
            <div class="container">
                <h1 class="showcase-title red"><i class="fa fa-bullhorn fa-fw"></i> {{trans('main.art_speaks')}}</h1>
                <div class="row">
                  <div class="image-gallery gallery-type-4 ">
                    @php($cCount=0)
                  @foreach($insta_photos as $index => $value)
                    @if($value->type == 'Sanat Dunyasi' && $cCount<12 )
                    <div class="col-md-2 col-sm-2 col-xs-2" style="height:120px !important; margin-bottom:15px;">
                      <a href="{{($value->owner) ? 'http://instagram.com/'.substr($value->owner,1) : 'http://instagram.com/explore/tags/'.substr($value->hashtag,1)}}" class="item" style="background-size: cover ; background-repeat: no-repeat;  object-fit: cover;">
                      <img src="{{$value->url}}" alt="image"  >
                      <span class="head">{{($value->owner) ? $value->owner : $value->hashtag}}</span>
                    </a>
                    </div>
                    @php($cCount++)
                    @endif

                @endforeach
              </div>
                </div>
            </div>
        </div>


        <div class="showcase-block bordered green">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.partners')}}</h1>
                <ul class="brands-list">
                  @foreach($partners as $partner)
                      <li><img src="{{$partner->image}}" class="img-responsive" alt="brand 01" width="129" height="47"></li>
                  @endforeach

                </ul>
            </div>
        </div>

        <div class="showcase-block">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.brands')}}</h1>
                <ul class="brands-list">
                    @foreach($brands as $brand)
                        <li><img src="{{$brand->image}}" alt="brand 01" width="129" height="47"></li>
                    @endforeach

                </ul>
            </div>
        </div>
    </main>

    <script type="text/javascript">
        $('.products').mouseenter(function(){

          $(this).next().fadeIn('fast')
        }).mouseleave(function(){
          var el = $(this)
          setTimeout(function(){
            el.next().fadeOut('slow')
          },300)
        })
        $('.projects').mouseenter(function(){
          $(this).next().fadeIn('fast')
        }).mouseleave(function(){
          var el = $(this)
          setTimeout(function(){
            el.next().fadeOut('slow')
          },300)
        })
    </script>
@endsection
