@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            @include('layouts.profileTop')
            @include('students.buttons')


            <div class="image-gallery-holder" >
                <h2 class="gallery-title">BEĞENDİĞİM ÜRÜNLER</h2>
                <div class="image-gallery one-row-slider" >

                    @foreach(request()->user()->likedProducts as $item)

                        <div class="gallery-item" style="height: 300px; position:relative">

                            <a href="/product/{{$item->slug}}" class="item" style="height: 155px!important; ">
                                <img src="{{$item->image}}" alt="image" class="img">
                                <span class="text">{{$item->name}}</span>
                                  <a href="{{url('unlike_product/'.$item->id)}}" style="color:#fff;text-shadow:2px 2px 2px #690d0d;font-size:25px;position:absolute;bottom:160;right:10"><i class="fa fa-thumbs-down"></i></a>
                            </a>
                            <form class="" action="{{url('update_product_pivot')}}" method="post">
                              {!! csrf_field() !!}
                              <input type="hidden" name="product_id" value="{{$item->id}}">
                              <textarea  rows="3" class="col-md-12" placeholder="NOT YAZ:" style="margin-top:1px" name="product_note">{{$item->pivot->note}}</textarea>
                              <button type="submit" class="pull-left btn-sm btn" style="border-radius:0;background-color:#c2b59b;color:#fff">Send</button>
                            </form>
                           <br>


                        </div>
                    @endforeach
                </div>



            </div>

            <div class="image-gallery-holder" >
                <h2 class="gallery-title">BEĞENDİĞİM PROJELER</h2>
                <div class="image-gallery one-row-slider" >

                    @foreach($student->likedProjects as $item)


                        <div class="gallery-item" style="height: 300px;position:relative" >

                            <a href="/project_detail/{{$item->slug}}" class="item" style="height: 155px; ">
                                <img src="{{$item->image}}" alt="image" class="img">
                                <span class="text">{{$item->name}}</span>
                                <a href="{{url('unlike_product/'.$item->id)}}" style="color:#fff;text-shadow:2px 2px 2px #690d0d;font-size:25px;position:absolute;bottom:160;right:10"><i class="fa fa-thumbs-down"></i></a>
                            </a>

                              <form class="" action="{{url('update_project_pivot')}}" method="post">
                                  {!! csrf_field() !!}

                                <input type="hidden" name="project_id" value="{{$item->id}}">
                                <textarea  rows="3" class="col-md-12" placeholder="NOT YAZ:" style="margin-top:1px" name="project_note">{{$item->pivot->note}}</textarea>
                                <button type="submit" class="pull-left btn-sm btn" style="border-radius:0;background-color:#c2b59b;color:#fff">Send</button>
                              </form>






                        </div>
                    @endforeach
                </div>



            </div>
            <div class="image-gallery-holder" >
                <h2 class="gallery-title">KATILDIĞIN ETKİNLİKLER</h2>
                <div class="image-gallery one-row-slider" >


                </div>



            </div>

            <div class="buttons-list">
                <a href="/company" class="button-green"><span>Ürün Öner</span></a>
                <a href="#" class="button-green"><span>Tasarımcı Öner</span></a>
                <a href="#" class="button-green"><span>Danışman Öner</span></a>

                <form id="logout-form" action="http://mimarsiv.dev/logout" method="POST" style="display: none;"><input type="hidden" name="_token" value="c8MwCOzvQD5JQV5YBJSuzZuzZRRbt6P6tAyghhNx"></form>
            </div>
        </div>
</div>
@endsection
