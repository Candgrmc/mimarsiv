<style media="screen">
	.nav-pills > .active> a:focus{
		background-color: #c2b59b !important;
	}
	.nav-pills > .active> a{
		background-color: #c2b59b !important;
	}
	.btn-gold{
		background-color: #c2b59b !important;
	}
</style>
<div id="signinModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<ul class="nav nav-pills nav-justified nav-default">
					<li class="active">
						<a data-toggle="pill" href="#loginTab">{{trans('main.login')}}</a>
					</li>
					<li>
						<a data-toggle="pill" href="#registerTab">{{trans('main.register')}}</a>
					</li>
				</ul>
				<div class="tab-content">
					<div id="loginTab" class="tab-pane fade in active">
						<form method="POST" action="{{ route('login') }}">
							{{ csrf_field() }}
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label for="email">{{trans('main.e_mail_address')}}</label>
								<input type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus> @if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label for="password">{{trans('main.password')}}</label>
								<input  type="password" class="form-control" name="password" required> @if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
								@endif
							</div>


							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember" {{ old( 'remember') ? 'checked' : '' }}> {{trans('main.remember_me')}}
								</label>
							</div>

							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-gold">
										{{trans('main.login')}}
									</button>

									<a class="btn btn-link pull-right" href="{{ route('password.request') }}">
										{{trans('main.lost_password')}}
									</a>
								</div>
							</div>
						</form>
					</div>
					<div id="registerTab" class="tab-pane fade">
						<form method="POST" action="{{ route('register') }}" id="register">
							{{ csrf_field() }}

							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name">{{trans('main.name_surname')}}</label>
								<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus> @if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group">
								<label for="user_type_id">{{trans('main.account_type')}}:</label>
								<select class="form-control" name="user_type_id" id="account_type">
									<option value="">Seçiniz..</option>
									<option value="1" >{{trans('main.student')}}</option>
									<option value="2" >{{trans('main.architect')}}</option>
									<!-- <option value="3" >{{trans('main.investor')}}</option> -->
									<option value="6" >{{trans('main.office')}}</option>
									<option value="7" >{{trans('main.company')}}</option>
									<option value="4" >{{trans('main.consultant')}}</option>
									<option value="5" >{{trans('main.artisan')}}</option>


								</select>
							</div>
							<!--
							Student
							-->
							<div id="student" class="partials" style="display: none;">
								<div class="form-group">
									<label for="school_name">{{trans('main.school_name')}}</label>
									<input type="text" class="form-control" name="school_name">
								</div>
								<div class="form-group" id="department">
									<label for="department">{{trans('main.department_name')}}</label>
									<input type="text" class="form-control" name="department">
								</div>
							</div>

							<!--Student End -->

							<!--
							Architect
							-->
							<div id="architect" class="partials" style="display: none;">
								<div class="form-group">
									<label for="arc_company_name">{{trans('main.company_name')}}</label>
									<input type="text" class="form-control" name="acompany_name">
								</div>
								<div class="form-group">
									<label for="arc_company_name">{{trans('main.company_phone')}}</label>
									<input type="text" class="form-control" name="acompany_phone">
								</div>

							</div>
							<!--Architect End -->
							<!--
							Office
							-->
							<div id="office" class="partials" style="display: none;">
								<div class="form-group">
									<label for="office_name">{{trans('main.office_name')}}</label>
									<input type="text" class="form-control" name="office_name">
								</div>
								<div class="form-group">
									<label for="address">{{trans('main.address')}}</label>
									<input type="text" class="form-control" name="ofaddress">
								</div>
								<div class="form-group" >
									<label for="phone">{{trans('main.phone')}}</label>
									<input type="text" class="form-control" name="ofphone">
								</div>
							</div>
							<!--Office End -->

							<!--
							Consultant
							-->
							<div id="consultant" class="partials" style="display: none;">
								<div class="form-group">
									<label for="consultant_category">Danışman Kategorisi</label>
									<select class="consultant_category" name="consultant_category">
											<option value="">Seçiniz..</option>
											@php($consultant_categories = DB::table('consultant_categories')->get())
											@foreach($consultant_categories as $category)
												<option value="{{$category->id}}">{{$category->title}}</option>
											@endforeach
									</select>
								</div>
								<div class="form-group">
									<label for="company_name">{{trans('main.company_name')}}</label>
									<input type="text" class="form-control" name="cocompany_name">
								</div>
								<div class="form-group">
									<label for="address">{{trans('main.address')}}</label>
									<input type="text" class="form-control" name="coaddress">
								</div>
								<div class="form-group" >
									<label for="phone">{{trans('main.phone')}}</label>
									<input type="text" class="form-control" name="cophone">
								</div>
							</div>
							<!--Consultant End -->
							<!--
							Artisan
							-->
							<div id="artisan" class="partials" style="display: none;">
								<div class="form-group">
									<label for="artisan_category">Zanaatkar Kategorisi</label>
									<select class="" name="artisan_category">
										<option value="">Seçiniz..</option>
										@foreach(DB::table('artisan_categories')->get() as $category)
											<option value="{{$category->title}}">{{$category->title}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label for="company_name">{{trans('main.company_name')}}</label>
									<input type="text" class="form-control" name="arcompany_name">
								</div>
								<div class="form-group">
									<label for="address">{{trans('main.address')}}</label>
									<input type="text" class="form-control" name="araddress">
								</div>
								<div class="form-group" >
									<label for="phone">{{trans('main.phone')}}</label>
									<input type="text" class="form-control" name="arphone">
								</div>
							</div>
							<!--Artisan End -->
							<!--
							Company
							-->
							<div id="company" class="partials" style="display: none;" >
								<div class="form-group">
									<label for="company_name">{{trans('main.company_name')}}</label>
									<input type="text" class="form-control" name="ccompany_name">
								</div>
								<div class="form-group">
									<label for="address">{{trans('main.address')}}</label>
									<input type="text" class="form-control" name="caddress">
								</div>
								<div class="form-group" >
									<label for="phone">{{trans('main.phone')}}</label>
									<input type="text" class="form-control" name="cphone">
								</div>
							</div>
							<!-- Company End -->
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label for="email">{{trans('main.e_mail_address')}}</label>
								<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required> @if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label for="password">{{trans('main.password')}}</label>
								<input id="password" type="password" class="form-control" name="password" required> @if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group">
								<label for="password-confirm">{{trans('main.re_password')}}</label>
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
							</div>

							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-gold">
										{{trans('main.register')}}
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
		src="http://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
