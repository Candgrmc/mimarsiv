<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mimarsiv</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/slick.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/lightbox.min.css')}}">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{asset('js/jquery.main.js')}}" defer></script>
    <script src="{{asset('js/slick.min.js')}}"></script>
    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>

</head>
<body>
  <div class="show-popup">
    <div class="overlay"></div>
    <div class="img-show">
      <span>X</span>
      <img src="">
    </div>
  </div>
@yield("content")


</body>
<footer id="footer">
    <div class="container">
        <div class="footer-holder">
            <ul class="footer-nav">
                <?php $pages = \App\Page::all(); ?>
                @foreach($pages as $page)
                        <li><a href="{{url('pages/'.$page->slug)}}">{{$page->title}}</a></li>
                    @endforeach
                <li><a href="{{url('pages/contact')}}">{{trans('main.contact')}}</a></li>

            </ul>
            <div class="copyright">
                <p>MIMARSIV © 2017. {{trans('main.copyright')}}</p>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}">

</script>
<script>
    $('.cktext').ckeditor();

    $(document).ready(function() {
        $('.linked-gallery-head').slick({
            dots: false,
            infinite: true,
            adaptiveHeight: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.linked-gallery-pager',
            prevArrow: '<button class="slick-prev"><i class="fa fa-angle-left"></i></button>',
            nextArrow: '<button class="slick-next"><i class="fa fa-angle-right"></i></button>'
        });

        $('.linked-gallery-pager').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: false,
            focusOnSelect: true,
            asNavFor: '.linked-gallery-head',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });

        $('.one-row-slider').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: '<button class="slick-prev"><i class="fa fa-angle-left"></i></button>',
            nextArrow: '<button class="slick-next"><i class="fa fa-angle-right"></i></button>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });

        $('.two-row-slider').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: '<button class="slick-prev"><i class="fa fa-angle-left"></i></button>',
            nextArrow: '<button class="slick-next"><i class="fa fa-angle-right"></i></button>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });
    });
</script>
<script type="text/javascript">
$(function () {
  "use strict";

  $(".popup img").click(function () {
    $('.show-popup img').css({
         position:'absolute',
         left: ($(window).width() - $('body').outerWidth())/2,
         top: ($(window).height() - $('body').outerHeight())/2
     });
      var $src = $(this).attr("src");
      $(".show-popup").fadeIn();
      $(".img-show img").attr("src", $src);

  });

  $("span, .overlay").click(function () {
      $(".show-popup").fadeOut();
  });

});
</script>
<script>


	$('#account_type').on('change',function(){
	    var type = $(this).val()
	    if(type == 1){
			showThis('student');
		}else if(type == 2){
            showThis('architect');
		}else if(type == 3){
            closeAll()
		}else if(type == 4){
            showThis('consultant');
        }else if(type == 5){
            showThis('artisan');
        }
        else if(type == 6){
            showThis('office');
        }
        else if(type == 7){
            showThis('company');
        }
	})

	function closeAll(){
	    $('.partials').slideUp('fast');
	}

	function showThis(el){
        closeAll()
	    $('#'+el).slideDown('fast')
	}
</script>
</html>
