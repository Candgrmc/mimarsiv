@php($type = App\UserType::find(request()->user()->user_type_id))
<div class="top-normal-banner">
    <div class="icon-box">
        <img style="cursor: pointer" id="logo" src="{{(request()->user()->image) ? asset(request()->user()->image):'https://www.breakthroughforlife.com/wp-content/uploads/2017/02/Life-Coach-Insights-Life-Architect.jpg'}}" height="47" width="94" alt="{{request()->user()->name}}">
    </div>
    <img style="cursor: pointer" id="cover" src="{{(request()->user()->cover) ? asset(request()->user()->cover) : asset('images/default_cover.jpg')}}" height="960" width="1920" alt="{{request()->user()->name}}">
</div>
<form action="{{url('image/update')}}" enctype="multipart/form-data" id="photos" method="post">
    {{csrf_field()}}
    <input type="file" style="display:none" id="logoUpload" name="logo">
    <input type="file" style="display:none" id="coverUpload" name="cover">
</form>
<script>
    $('#logo').on('click',function () {

        $('#logoUpload').click();
    })
    $('#cover').on('click',function () {

        $('#coverUpload').click();
    })

    $('input[type="file"]').on('change',function(){
        $('#photos').submit()
    })

</script>
