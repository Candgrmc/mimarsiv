			<div class="container">
				<div class="top-bar clearfix">
					<ul class="lang-selector hidden-xs">
						<li><a href="/language/tr"><span>tur</span></a></li>
						<li><a href="/language/en"><span>eng</span></a></li>
					</ul>
					<button type="button" class="pull-right visible-xs navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
						<i class="fa fa-bars"></i>
					</button>
					<ul class="social-networks">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
					</ul>
				</div>
				<div class="middle-bar">
					<div class="logo-block">
						<a href="/">
							<img src="{{asset('images/logo.png')}}" alt="logo">
						</a>
					</div>
					<div class="search-box">
						<form class="search-form">

							<fieldset>
								<div class="form-block">
									<div class="input-item">
										<input type="text" id="sQuery" name="sQuery">
									</div>
									<div class="button-item">
										<button type="button" onclick="window.location.href='/search/'+$('#sQuery').val()"><span></span></button>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<div class="navbar navbar-default" @if(Request::segment(1) != '' && Request::segment(1) != 'products' && Request::segment(1) != 'projects' && Request::segment(1) != 'companies' && Request::segment(1) != 'consultants' && Request::segment(1) != 'artisans' && Request::segment(1) != 'brands'  ) style="margin-bottom:80px" @endif>

				<div class="container">
					<div class="collapse navbar-collapse" id="main-navbar">
						<ul class="main-navbar">
							<li><a href="/products"><span>{{trans('main.products')}}</span></a></li>
							<li><a href="/brands"><span>{{trans('main.brands')}}</span></a></li>
							<li><a href="/projects"><span>{{trans('main.projects')}}</span></a></li>
							<li><a href="/companies"><span>{{trans('main.companies')}}</span></a></li>
							<li><a href="/consultants"><span>{{trans('main.consultants')}}</span></a></li>
							<li><a href="/artisans"><span>{{trans('main.artisans')}}</span></a></li>
							<li><a href="/events"><span>{{trans('main.events')}}</span></a></li>


						</ul>
						<div class="login-area" style="overflow:hidden; width:200px;">
							@unless(Auth::check())
								<a href="#" class="login" data-toggle="modal" data-target="#signinModal">
							<span>{{trans('main.top_log')}}<b>{{trans('main.top_in')}}</b>
							</span>
								</a>
							@else
								<span>{{trans('main.hello')}} {{Auth::user()["name"]}}</span><br>
							@can('admin_access')
									<span><a href="/yonetici"> {{trans('main.admin_panel')}}</a></span>
								@endcan()
							@can('architect_access')
									<span><a href="/architect"> {{trans('main.architect_panel')}}</a></span>
								@endcan()

								@can('office_access')
									<span><a href="/office"> {{trans('main.office_panel')}}</a></span>
								@endcan()
								@can('company_access')
									<span><a href="/company"> {{trans('main.company_panel')}}</a></span>
								@endcan()
								@can('consultant_access')
									<span><a href="/consultant"> {{trans('main.consultant_panel')}}</a></span>
								@endcan()
								@can('student_access')
									<span><a href="/student"> {{trans('main.student_panel')}}</a></span>
								@endcan()
								@can('artisan_access')
									<span><a href="/artisan"> {{trans('main.artisan_panel')}}</a></span>
								@endcan()

								<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{trans('main.logout')}}</a>

								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>

							@endunless
						</div>
					</div>
				</div>
			</div>
