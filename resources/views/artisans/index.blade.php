
@extends('layouts.app')
<div id="wrapper" class="common-page danisman-listeleme">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="main-banner">
            <div class="banner-carousel">
                <div class="mask">
                    <div class="slideset">
                        @foreach($sliders as $slider)
                            <div class="slide">
                                <img src="{{$slider->image}}" alt="banner image">
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="pagination"></div>
            </div>
        </div>
        <div class="container">
          <div class="row">

                  <div class="breadcrumb-area col-md-3">
                      <ol class="breadcrumb">
                          <li><a href="#">HOME</a></li>
                          <li class="active"><a href="/consultants">ZANAATKARLAR</a></li>
                      </ol>
                  </div>

          </div>
            <div class="col-md-12 col-sm-12" style="margin-top:-50px">


                    <div class="search-filter-area col-md-8 col-sm-8 col-xs-8">
                        <div class="select-inputs-wrap">

                            <div class="select-input size2">
                                <select>
                                    <option>ÜLKE</option>
                                    <option>ÜLKE 1</option>
                                    <option>ÜLKE 2</option>
                                    <option>ÜLKE 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="search-result-info col-md-4 col-sm-4 col-xs-4">
                        <div class="results-info">
                            <p>Results: {{count($artisans)}}</p>
                        </div>
                    </div>



            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-lg-3">
                    <ul class="sidebar-nav">
                        @foreach($categories as $category)
                            <li><a href="project_category/{{$category->slug}}">{{$category->title}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-8 col-lg-9">
                    <div class="image-gallery gallery-type-2">
                        @foreach($artisans as $artisan)
                            <div class="gallery-item col-md-3 col-sm-3 col-xs-2" style="height: 188px;">
                                <a href="/artisan_detail/{{$artisan->id}}" class="item" >
                                    <img src="@if($artisan->image){{asset($artisan->image)}} @else {{asset('images/default.jpeg')}}@endif" alt="image" width="200" height="151">
                                    <span class="head">{{$artisan->name}}</span>
                                    <span class="text">
                                   {{$artisan->name}}<br>
                                </span>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
