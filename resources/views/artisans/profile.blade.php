@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            @include('layouts.profileTop')
            @include('artisans.buttons')
            <div class="row">
                <div class="form-group col-md-4">

                    <input type="text" class="form-control" placeholder="Proje Ara.." id="search_project">
                </div>
            </div>
            <div class="row">
              <ul class="pull-left" id="project_list">

              </ul>
            </div>


            <div class="image-gallery-holder">
                <h2 class="gallery-title">PROJELER</h2>
                <div class="image-gallery one-row-slider">
                  @foreach(request()->user()->Areferences as $project)
                  <div class="gallery-item" style="height: 141px;">
                      <a href="/project_detail/{{$project->slug}}" class="item">
                          <img src="{{asset($project->image)}}" alt="image" width="188" height="141">
                          <span class="text">{{$project->name}}</span>
                      </a>
                  </div>
                      @endforeach

                </div>

            </div>
        </div>
</div>
<script>
    $('#search_project').on('keyup',function(){
        var keyword = $(this).val();
        if(keyword.length > 3){
            $.getJSON('/consultant/projects/'+keyword,function(res){
                console.log(res)
                $('#project_list').html('')
                for(var i = 0 ; i < res.length ; i++){
                    $('#project_list').append('<li class="listItem">'+res[i].name+' <a href="/artisan/add_reference/'+res[i].id+'" data-id="'+res[i].id+'" class="reference" > Referansa Ekle</a></li>');
                }
            })
        }

    })

</script>
@endsection
