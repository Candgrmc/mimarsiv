
@extends('layouts.app')
<div id="wrapper" class="common-page proje-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
    @php($company = App\Artisan::where('user_id',$artisan->id)->first())

        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="sidebar-box">
                        @if($company)
                        <div class="icon-box">
                            <img src="@if($artisan->image){{asset($artisan->image)}} @else {{asset('images/default.jpeg')}} @endif" alt="client" width="130" height="87">
                        </div>

                        <p>{{$company->company_name}}</p>
                        <address>
                          <strong>Telefon</strong> :{{$company->company_phone}} <br>
                          <strong>Adres</strong> :{{$company->company_address}} <br>
                          <strong>Web</strong> :{{$company->company_url}}
                        </address>
                        @endif

                    </div>

                   {{-- <div class="sidebar-box">
                        <h2>find retailers</h2>
                        <form action="#" class="find-retailers">
                            <div class="select-inputs-wrap">
                                <div class="select-input">
                                    <select class="large">
                                        <option>COUNTRY</option>
                                        <option>NEPAL</option>
                                        <option>USA</option>
                                        <option>CHINA</option>
                                    </select>
                                </div>
                                <div class="select-input">
                                    <select class="large">
                                        <option>STATE</option>
                                        <option>STATE 1</option>
                                        <option>STATE 2</option>
                                        <option>STATE 3</option>
                                    </select>
                                </div>
                                <div class="select-input inline">
                                    <select class="large">
                                        <option>CITY</option>
                                        <option>CITY 1</option>
                                        <option>CITY 2</option>
                                        <option>CITY 3</option>
                                    </select>
                                </div>
                                <div class="select-input inline button-wrap">
                                    <button type="submit"><span>search</span></button>
                                </div>
                            </div>
                        </form>
                    </div>--}}
                </div>
                <div class="col-sm-8">

                  @php($photos = App\CompanyGallery::where('company_id',$artisan->id)->get())

                  <h1 class="top-title">{{$artisan->name}}</h1>
                  <div class="linked-gallery-head" style="height:500px">
                      @foreach($photos as $image)
                          <div class="image-item">
                              <img src="{{$image->path}}" height="300" alt="gallery-item">
                          </div>
                          @endforeach


                  </div>

                  <div class="linked-gallery-pager">
                      @foreach($photos as $image)
                      <div class="pager-item">
                          <img src="{{$image->path}}" height="93" width="114" alt="pager item">
                      </div>
                      @endforeach
                  </div>
                  <ul class="tab-list">
                    <li class="active"><a href="#tab-item-01" data-toggle="tab">{{trans('main.about')}}</a></li>

                  </ul>
                    <article class="article" style="margin-bottom:80px">
                        @if($company)
                          {!! $company->description !!}
                        @endif
                    </article>
                    <h3 class="article-h3 padding-bottom-5">REFERENCE PROJECTS</h3>
                    <div class="image-gallery gallery-type-2 col-md-12 col-xs-12 col-sm-12">

                        @foreach($artisan->Areferences as $project)
                            <div class="gallery-item " style=" height: 188px;">
                                <a href="{{url('project_detail/'.$project->slug)}} col-md-3 col-xs-3 col-sm-3" class="item">
                                    <img src="@if($project->image){{asset($project->image)}} @else {{asset('images/default.jpeg')}} @endif" alt="image"  height="188">
                                    <span class="head">{{$project->name}}</span>
                                    <span class="text">
                                    {{$project->name}} <br>
                                        @if($project->category)
                                          {{$project->category->title}} <br>
                                        @endif
                                </span>
                                </a>
                            </div>
                            @endforeach
                    </div>

                </div>
            </div>
        </div>
</div>
@endsection
