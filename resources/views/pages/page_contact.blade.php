
@extends('layouts.app')
<div id="wrapper" class="common-page">
    <header id="header">

        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')

    <main id="main">
    <div class="breadcrumb-area">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Anasayfa</a></li>
                <li><a href="#">İletişim</a></li>

            </ol>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-5 col-md-4">
                <div class="sidebar-box margin-bottom-20">
                    <span class="title">MİMARŞİV ORGANİZASYON LTD.ŞTİ</span>
                    <ul class="list-items padding-bottom-15 margin-bottom-20">
                        <li><strong>ADRES:</strong> <span>Fransız Geçidi Karaköy ISTANBUL</span></li>
                        <li><strong>TELEFON:</strong> <span>0 212 243 36 43</span></li>
                        <li><strong>EMAİL:</strong> <span>info@mimarsiv.com</span></li>
                    </ul>
                    <br>
                    <br>

                    <span class="title">İLETİŞİM FORMU</span>
                    <form action="#" class="find-retailers">
                        <div class="single-line">
                            <label>ADINIZ SOYADINIZ :</label>
                            <div class="input-box-wrap">
                                <input type="text">
                            </div>
                        </div>
                        <div class="single-line">
                            <label>TELEFON :</label>
                            <div class="input-box-wrap">
                                <input type="text">
                            </div>
                        </div>
                        <div class="single-line">
                            <label>E-POSTA :</label>
                            <div class="input-box-wrap">
                                <input type="text">
                            </div>
                        </div>
                        <div class="single-line">
                            <label>MESAJINIZ :</label>
                            <div class="input-box-wrap">
                                <textarea cols="30" rows="1"></textarea>
                            </div>
                        </div>
                        <div class="button-submit-box text-right">
                            <button type="submit"><span>GÖNDER</span></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-7 col-md-8">
                <div class="map-block">

                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3010.0454455389704!2d28.977477815386212!3d41.0242616792991!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab9dc57a45403%3A0xa0b0997f53cf34f6!2sMimarsiv!5e0!3m2!1str!2str!4v1520121269468" height="531" width="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</main>
</div>

@endsection
