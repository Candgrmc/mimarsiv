
@extends('layouts.app')
<div id="wrapper" class="common-page proje-sayfasi">
    <header id="header">

        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')

        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-md-4">
                    @if(isset($company) && $company)
                    <div class="sidebar-box margin-bottom-30">
                        <div class="icon-box">
                            <img src="@if($company && $company->logo) {{$company->logo}} @elseif($product->user && $product->user->image) {{asset($product->user->image)}}  @else{{ asset('images/default.jpeg')}} @endif" alt="client" width="94" height="47" style="vertical-align: middle !important;">
                        </div>
                        <p>{{$company->name}}</p>
                        <address>

                          <strong>{{trans('main.address')}}: </strong>  {{$company->address}} <br>
                          <strong>{{trans('main.phone')}}: </strong>  {{$company->phone}}<br>
                          <strong>Website: </strong>  {{$company->url}}
                        </address>
                    </div>
                    @endif()




                </div>
                <div class="col-sm-7 col-md-8">
                    <h1 class="top-title">{{$page->title}}</h1>
                    <div class="linked-gallery-head" style="height:500px">

                            <div class="image-item">
                              <a href="{{asset($page->image)}}" data-lightbox="{{asset($page->image)}}" data-title="{{$page->title}}">
                                <img src="{{$page->image}}" height="300" class="gallery-item">
                                </a>
                            </div>



                    </div>



                    <ul class="tab-list">
                        <li class="active"><a href="#tab-item-01" data-toggle="tab">{{trans('main.about')}}</a></li>

                    </ul>
                    <div class="tab-content" style="margin-bottom:20px">
                        <article id="tab-item-01" class="tab-pane active article padding-bottom-15">
                            {!! $page->content !!}
                        </article>




                    </div>



                </div>
            </div>
        </div>
</div>

@endsection
