@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        <style>
            .listItem a{
                text-decoration: none;
                color: #000;

            }
            .listItem{
                list-style: none;
            }
            .listItem a:hover{
                text-decoration: none;
                color:silver;
            }
            .reference{
                border-bottom:1px solid silver;
            }
        </style>
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container" >
            @include('layouts.profileTop')
            @include('consultants.buttons')



              <div class="row">
                  <div class="form-group col-md-4">

                      <input type="text" class="form-control" placeholder="Proje Ara.." id="search_project">
                  </div>
              </div>
              <div class="row">
                <ul class="pull-left" id="project_list">

                </ul>
              </div>

            <div class="image-gallery-holder" >
                <h2 class="gallery-title">Referanslar</h2>
                <div class="image-gallery one-row-slider" >

                    @foreach(request()->user()->references as $item)

                        <div class="gallery-item" style="height: 300px;" >

                            <a href="/project_detail/{{$item->slug}}" class="item" style="height: 155px; ">
                                <img src="{{$item->image}}" alt="image" class="img">
                                <span class="text">{{$item->name}}</span>
                            </a>



                        </div>
                    @endforeach
                </div>




            </div>



        </div>
</div>

<script>
    $('#search_project').on('keyup',function(){
        var keyword = $(this).val();
        if(keyword.length > 3){
            $.getJSON('/consultant/projects/'+keyword,function(res){
                console.log(res)
                $('#project_list').html('')
                for(var i = 0 ; i < res.length ; i++){
                    $('#project_list').append('<li class="listItem">'+res[i].name+' <a href="/consultant/add_reference/'+res[i].id+'" data-id="'+res[i].id+'" class="reference" > Referansa Ekle</a></li>');
                }
            })
        }

    })

</script>
@endsection
