
@extends('layouts.app')
<div id="wrapper" class="common-page danishman-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
    @php($company = App\Consultant::where('user_id',$consultant->id)->first())

        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="sidebar-box">
                        @if($company)
                        <div class="icon-box">
                            <img src="@if($consultant->image){{asset($consultant->image)}} @else {{asset('images/default.jpeg')}} @endif" alt="client" width="130" height="87">
                        </div>

                        <p>{{$company->company_name}}</p>
                        <address>
                            <strong>Telefon</strong> :{{$company->company_phone}} <br>
                            <strong>Adres</strong> :{{$company->company_address}} <br>
                            <strong>Web</strong> :{{$company->company_url}}
                        </address>
                        @endif

                    </div>

                   {{-- <div class="sidebar-box">
                        <h2>find retailers</h2>
                        <form action="#" class="find-retailers">
                            <div class="select-inputs-wrap">
                                <div class="select-input">
                                    <select class="large">
                                        <option>COUNTRY</option>
                                        <option>NEPAL</option>
                                        <option>USA</option>
                                        <option>CHINA</option>
                                    </select>
                                </div>
                                <div class="select-input">
                                    <select class="large">
                                        <option>STATE</option>
                                        <option>STATE 1</option>
                                        <option>STATE 2</option>
                                        <option>STATE 3</option>
                                    </select>
                                </div>
                                <div class="select-input inline">
                                    <select class="large">
                                        <option>CITY</option>
                                        <option>CITY 1</option>
                                        <option>CITY 2</option>
                                        <option>CITY 3</option>
                                    </select>
                                </div>
                                <div class="select-input inline button-wrap">
                                    <button type="submit"><span>search</span></button>
                                </div>
                            </div>
                        </form>
                    </div>--}}
                </div>
                <div class="col-sm-8">
                  
                    <article class="article" style="margin-bottom:80px">
                        @if($company)
                          {!! $company->description !!}
                        @endif
                    </article>
                    <h3 class="article-h3 padding-bottom-5">REFERENCE PROJECTS</h3>
                    <div class="image-gallery gallery-type-2">

                        @foreach($consultant->references as $project)

                            <div class="gallery-item" style="width: 248px; height: 188px;">
                                <a href="project_detail/{{$project->slug}}" class="item">
                                    <img src="{{asset($project->image)}}" alt="image" width="248" height="188">
                                    <span class="head">{{$project->name}}</span>
                                    <span class="text">
                                    {{$project->name}} <br>
                                        @if($project->category)
                                          {{$project->category->title}} <br>
                                        @endif
                                </span>
                                </a>
                            </div>
                            @endforeach
                    </div>

                </div>
            </div>
        </div>
</div>
@endsection
