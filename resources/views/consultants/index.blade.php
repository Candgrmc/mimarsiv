
@extends('layouts.app')
<div id="wrapper" class="common-page danisman-listeleme">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="main-banner">
            <div class="banner-carousel">
                <div class="mask">
                    <div class="slideset">
                        @foreach($sliders as $slider)
                            <div class="slide">
                                <img src="{{$slider->image}}" alt="banner image">
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="pagination"></div>
            </div>
        </div>
        <div class="container padding-top-20">
            <div class="row">
                <div class="col-sm-6 col-md-5">
                    <div class="breadcrumb-area">
                        <ol class="breadcrumb">
                            <li><a href="#">HOME</a></li>
                            <li class="active"><a href="/consultants">DANIŞMANLAR</a></li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-6 col-md-7">
                    <div class="search-filter-area padding-top">
                        <div class="select-inputs-wrap">
                            <div class="select-input size1">
                                <select>
                                    <option>PROJE TİPİ</option>
                                    <option>PROJE TİPİ 1</option>
                                    <option>PROJE TİPİ 2</option>
                                    <option>PROJE TİPİ 3</option>
                                </select>
                            </div>
                            <div class="select-input size2">
                                <select>
                                    <option>ÜLKE</option>
                                    <option>ÜLKE 1</option>
                                    <option>ÜLKE 2</option>
                                    <option>ÜLKE 3</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="search-result-info">
                        <div class="results-info">
                            <p>Results: {{count($consultants)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-lg-3">
                    <ul class="sidebar-nav">
                        @foreach($categories as $category)
                            <li><a href="project_category/{{$category->slug}}">{{$category->title}}</a></li>
                            @endforeach
                    </ul>
                </div>
                <div class="col-sm-8 col-lg-9">
                    <div class="image-gallery gallery-type-2 col-md-12">
                        @foreach($consultants as $consultant)
                        @php($company = App\Consultant::where('user_id',$consultant->id)->first())
                        <div class="gallery-item col-md-3 col-sm-3 col-xs-2" style="height: 188px;">
                            <a href="{{url('/consultant/'.$consultant->id)}}" class="item">
                                <img src="@if($consultant->image){{asset($consultant->image)}} @else {{asset('images/default.jpeg')}} @endif" alt="image"  height="151">
                                <span class="head">{{$consultant->name}}</span>
                                <span class="text">
                                  @if($company)
                                   {{$company->company_name}}<br>
                                   @endif
                                </span>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
