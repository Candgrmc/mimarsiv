@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
@section('content')
        <div class="container">
            @include('layouts.profileTop')
            @include('consultants.buttons')
            <form action="{{url('consultant/update_credentials')}}" method="post" >
                {{csrf_field()}}
                <div class="col-md-6">
                  @if($company)
                  <div class="form-group">
                      <label for="company">{{trans('main.company_name')}}</label>
                      <input type="text" class="form-control" value="@if($company){{$company->company_name}} @endif" disabled>
                  </div>
                  <div class="form-group">
                      <label for="company">{{trans('main.address')}}</label>
                      <input type="text" class="form-control" value="@if($company){{$company->company_address}} @endif" disabled>
                  </div>
                  <div class="form-group">
                      <label for="company">{{trans('main.company_phone')}}</label>
                      <input type="text" class="form-control" value="@if($company){{$company->company_phone}} @endif" disabled>
                  </div>
                  <div class="form-group">
                      <label for="url">Web Site</label>
                      <input name="url" type="text" class="form-control" value="@if($company){{$company->company_url}} @endif">
                  </div>
                  <div class="form-group">
                      <label for="company">{{--trans('main.writing')--}} Ön Yazı</label> <br>
                      <textarea name="description" rows="8" cols="80">{{$company->description}}</textarea>
                  </div>
                  @else
                    <h5 class="text-danger">Kayıtlı firma bilgileri bulunmamaktadır. Lütfen yöneticiyle görüşünüz.</h5>
                  @endif

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" value="{{request()->user()->email}}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="name">{{trans('main.name')}}</label>
                        <input type="text" class="form-control" name="name" value="{{request()->user()->name}}" required>
                    </div>



                    <div class="form-group">
                        <label for="phone">{{trans('main.phone')}}</label>
                        <input type="phone" name="phone" class="form-control" name="phone" value="{{request()->user()->phone}}">
                    </div>
                    <div class="form-group">
                        <label for="password">{{trans('main.password')}}</label>
                        <input type="password" name="password" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="password_repeat">{{trans('main.re_password')}}</label>
                        <input type="password" name="password_repeat" class="form-control" >
                    </div>

                    <button type="submit" class="button-green" style="margin-bottom:20px">Güncelle</button>
                </div>


            </form>
        </div>
</div>
@endsection
