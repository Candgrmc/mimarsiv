<div class="buttons-list">
    <a href="/consultant" class="button-green"><span>MIMARSIVIM</span></a>
    <a href="{{url('consultant/credentials')}}" class="button-green"><span>BILGILERIM</span></a>
    <a href="{{ route('logout') }}" class="button-green" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">ÇIKIŞ YAP</a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>

</div>
