@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
@section('content')
        <div class="container">
            @include('layouts.profileTop')
            @include('company.buttons')
            <form action="{{url('company/update_credentials')}}" method="post" >
                {{csrf_field()}}
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" value="{{request()->user()->email}}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="name">{{trans('main.name')}}</label>
                        <input type="text" class="form-control" name="name" value="{{request()->user()->name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="university">{{trans('main.company')}}</label>
                        <input type="university" class="form-control" value="@isset($company){{$company->name}} @endisset" disabled>
                    </div>
                    <div class="form-group">
                        <label for="department">{{trans('main.address')}}</label>
                        <input type="department" class="form-control" value="@isset($company){{$company->address}} @endisset" disabled>
                    </div>
                    <div class="form-group">
                        <label for="url">Website</label>
                        <input type="url" class="form-control" name="url" value="@isset($company){{$company->url}} @endisset" >
                    </div>

                    <div class="form-group">
                        <label for="phone">{{trans('main.phone')}}</label>
                        <input type="phone" class="form-control" name="phone" value="{{$company->phone}}">
                    </div>
                    <div class="form-group">
                      <label for="description">Ön Yazı</label>
                      <textarea name="description" rows="8" cols="80">{{$company->description}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="password">{{trans('main.password')}}</label>
                        <input type="password" name="password" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="password-repeat">{{trans('main.re_password')}}</label>
                        <input type="password" name="password-repeat" class="form-control" >
                    </div>

                    <button type="submit" class="button-green" style="margin-bottom:20px">Güncelle</button>
                </div>


            </form>
        </div>
</div>
@endsection
