@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
                @include('layouts.profileTop')
                @include('company.buttons')

            <ul class="default-accordion">
                <form action="{{ URL::to('/project/store') }}" method="post" enctype="multipart/form-data" class="simple-form">
                    <li class="active">
                        <a href="#" class="opener">GENEL BİLGİLER</a>
                        <div class="slide-box">

                            <div class="form-group size3">
                                <div class="select-input">
                                    <label for="form-element01">Proje TİPİ :</label>
                                    <select class="large straight form-control" name="category">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <br>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group size2">
                                <label for="form-element01">Proje ADI :</label>
                                <input type="text" id="form-element01" name="title">
                            </div>
                            <div class="form-group size5">
                                <label for="form-element02">Proje KÜNYESİ :</label>
                                <textarea class="form-control" rows="5" name="tag"></textarea>
                            </div>

                            <div class="form-group size5">
                                <label for="form-element02">Proje HİKAYESİ :</label>
                                <textarea class="form-control" rows="5" name="story"></textarea>
                            </div>
                            <div class="form-group size5">
                                <label for="form-element02">Proje YATIRIMCISI :</label>
                                <select class="large straight form-control" name="investor">
                                    @foreach($investors as $investor)
                                        <option value="{{$investor->id}}">{{$investor->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group size5">
                                <label for="form-element02">Proje YILI :</label>
                                <div class="container">
                                    <select name="start_year" id="">
                                        <option value="2000">2000</option>
                                        <option value="2001">2001</option>
                                        <option value="2002">2002</option>
                                        <option value="2003">2003</option>
                                        <option value="2004">2004</option>
                                        <option value="2005">2005</option>
                                        <option value="2006">2006</option>
                                        <option value="2007">2007</option>
                                        <option value="2008">2008</option>
                                        <option value="2009">2009</option>
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                    </select> -
                                    <select name="end_year" id="">
                                        <option value="2000">2000</option>
                                        <option value="2001">2001</option>
                                        <option value="2002">2002</option>
                                        <option value="2003">2003</option>
                                        <option value="2004">2004</option>
                                        <option value="2005">2005</option>
                                        <option value="2006">2006</option>
                                        <option value="2007">2007</option>
                                        <option value="2008">2008</option>
                                        <option value="2009">2009</option>
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                    </select>
                                </div>

                            </div>
                            <div class="form-group size5">
                                <label for="form-element02">Proje ÜLKESİ :</label>
                                <div class="container">
                                    <input type="text" name="country" class="">

                                </div>

                            </div>

                        </div>
                    </li>

                    <li>
                        <a href="javascript:void(0)" class="opener">Resimler</a>
                        <div class="slide-box">
                            <input type="file" name="image[]" multiple="multiple" required="required">
                            <span class="help-block">Birden fazla resim seçip ürün galerisi oluşturabilirsiniz</span>
                        </div>
                    </li>
                    {{-- <li>
                         <a href="#" class="opener">Sertifikalar</a>
                         <div class="slide-box">
                             <select multiple="multiple" name="certificates[]" id="certs" class="">
                             <div class="col-sm-5">
                                 @foreach($certificates as $certificate)
                                     <option value="{{$certificate->id}}">{{$certificate->name}}</option>
                                 @endforeach
                             </div>
                             </select>
                              </div>
                     </li>--}}

                    <br>
                    <input type="submit" class="button-plain large" value="Gönder">
                </form>
            </ul>
        </div>
</div>
@endsection