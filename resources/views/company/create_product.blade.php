@extends('layouts.app')

<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        <style>
            .listItem a{
                text-decoration: none;
                color: #000;

            }
            .listItem{
                list-style: none;
                border:none !important;
                display: inline-block;
                margin : 5px;
            }
            .listItem a:hover{
                text-decoration: none;
                color:silver;

            }
            .selectedItem{

                box-shadow: 2px 2px 2px 2px #e70700;

            }
        </style>
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
                @include('layouts.profileTop')
                @include('company.buttons')

            <ul class="default-accordion">
                <form action="{{ url('/company/product/store') }}" method="post" enctype="multipart/form-data" class="simple-form">
                    <li class="active">
                        <a href="#" class="opener">GENEL BİLGİLER</a>
                        <div class="slide-box">

                            <div class="form-group size3">
                                <div class="select-input">
                                    <select class="large straight" name="category">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <br>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group size2">
                                <label for="form-element01">ÜRÜN ADI :</label>
                                <input type="text" id="form-element01" name="title">
                            </div>

                            <div class="form-group size5">
                                <label for="form-element02">ÜRÜN AÇIKLAMASI :</label>
                                <textarea class="form-control" rows="5" name="overview"></textarea>
                            </div>

                            <div class="form-group">
                                <label>VİDEO :</label>
                                <br>
                                <input type="text" id="form-element01" name="video">
                            </div>


                            <div class="form-group">
                                <label>Proje :</label>

                                <br>
                                <input type="text" id="project_name" placeholder="Ara.." style="margin-top:5px" autocomplete="off">

                                <input type="hidden" id="project_selected_id" name="project_id" >
                                <ul id="project_list">

                                </ul>

                            </div>

                        </div>
                    </li>
                    <li>
                        <a href="#" class="opener">Marka</a>
                        <div class="slide-box">
                            <input type="text" placeholder="Marka Ara.." id="brandSearch">  <button type="button" class="button-plain" onclick="$('#new_brand').slideToggle('fast')">Yeni Ekle</button>
                            <ul id="brands_list" style="border:none">

                            </ul>
                        </div>
                        <div id="new_brand" style="margin-bottom:5px; display:none">
                            <input type="hidden" name="brand_id">
                            <input type="text" placeholder="Marka Adı.." name="brand_name">
                            <input type="text" placeholder="Web Sitesi.." name="brand_site">
                            <label for="brand_img">Resim</label>
                            <input type="file" name="brand_img">

                        </div>

                    </li>
                    <li>
                        <a href="#" class="opener">Katalog</a>
                        <div class="slide-box">
                            <div class="form-group">
                                <label>KATALOG :</label>
                                <br>
                                <input type="file" id="form-element01" name="catalogue">

                            </div>
                        </div>


                    </li>

                    <li>
                        <a href="#" class="opener">BİM</a>
                        <div class="slide-box">
                            <input type="file" name="bim">
                        </div>
                    </li>
                    <li>
                        <a href="#" class="opener">Resimler</a>
                        <div class="slide-box">
                            <input type="file" name="image[]" multiple="multiple" required="required" accept="image/*">
                            <span class="help-block">Birden fazla resim seçip ürün galerisi oluşturabilirsiniz</span>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="opener">Sertifikalar</a>
                        <div class="slide-box">
                            <input type="file" name="certificates[]" multiple="multiple" >
                            <span class="help-block">Birden fazla resim seçip ürün galerisi oluşturabilirsiniz</span>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="opener">Ödüller</a>
                        <div class="slide-box">
                            <input type="file" name="prizes[]" multiple="multiple" >
                            <span class="help-block">Birden fazla resim seçip ürün galerisi oluşturabilirsiniz</span>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="opener">Teknik Şartname</a>
                        <div class="slide-box">
                            <input type="file" name="legals[]" multiple="multiple" >
                            <span class="help-block">Birden fazla resim/pdf seçebilirsiniz</span>
                        </div>
                    </li>
                    {{-- <li>
                         <a href="#" class="opener">Sertifikalar</a>
                         <div class="slide-box">
                             <select multiple="multiple" name="certificates[]" id="certs" class="">
                             <div class="col-sm-5">
                                 @foreach($certificates as $certificate)
                                     <option value="{{$certificate->id}}">{{$certificate->name}}</option>
                                 @endforeach
                             </div>
                             </select>
                              </div>
                     </li>--}}

                    <li>
                        @php($settings = App\ProductSetting::first())
                        <a href="#" class="opener">FİYAT ARALIĞI</a>
                        <div class="slide-box">
                            <div class="row">
                                <div class="col-md-6">

                                    <select class="" name="price">
                                        @foreach(App\ProductSetting::all() as $price)
                                            <option value="{{$price->price}}">{{$price->price}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                    </li>
                    <br>
                    <input type="submit" class="button-plain large" value="Gönder">
                </form>
            </ul>
        </div>
</div>
<script>
    $('#project_name').on('keyup',function(){
        var keyword = $(this).val();
        if(keyword.length > 3){
            $.getJSON('/office/projects/'+keyword,function(res){
                console.log(res)
                $('#project_list').html('')
                for(var i = 0 ; i < res.length ; i++){
                    $('#project_list').append('<li class="listItem"><a href="javascript:void(0)" data-id="'+res[i].id+'" class="projects" onclick="getValue($(this))">'+res[i].name+'</a></li>');
                }
            })
        }

    })
    function getValue(el){
        $('#project_name').val(el.text())
        $('#project_selected_id').val(el.attr('data-id'));
    }

    var brands = []

    @foreach($brands as $brand)
        brands.push($.parseJSON('{!! $brand !!}'))

    @endforeach


    $('#brandSearch').on('keyup',function(){
        var value = $(this).val();
        $('#brands_list').html('')
        count = 0;
        brands.forEach(function(item){
            if(value.length && item.name.toLowerCase().indexOf(value.toLowerCase()) != -1 && count <5){

                $('#brands_list').append('<li class="listItem"><a href="javascript:void(0)" onclick="selectItem($(this))" data-id="'+item.id+'" data-site="'+item.website+'"><img src="'+item.image+'" height="70" class="img-rounded"> </a></li>')

                count++
            }
        })
    })

    function selectItem(el){
        if(el.attr('data-id') != $('input[name="brand_id"]').val()){
            $('input[name="brand_id"]').val(el.attr('data-id'))
            $('.selectedItem').removeClass('selectedItem')
            el.children('img').addClass('selectedItem')
        }else{
            $('input[name="brand_id"]').val('')
            $('.selectedItem').removeClass('selectedItem')
        }

    }

</script>
@endsection
