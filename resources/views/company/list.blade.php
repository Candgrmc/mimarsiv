
@extends('layouts.app')
<div id="wrapper" class="common-page proje-listeleme">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
    <div class="main-banner">
        <div class="banner-carousel">
            <div class="mask">
                <div class="slideset">
                    @foreach($sliders as $slider)
                    <div class="slide">
                        <a href="@if($slider->redirectTo){{$slider->redirectTo}}@else javascript:void(0) @endif"><img src="{{asset($slider->image)}}" alt="banner image"></a>
                    </div>
                    @endforeach

                </div>
            </div>
            <div class="pagination"></div>
        </div>
    </div>
        <div class="search-result-info">
            <div class="container">
              <div class="row">
                <div class="col-md-3">
                    <div class="breadcrumb-area">
                        <ol class="breadcrumb">
                            <li><a href="/">ANASAYFA</a></li>
                            <li><a href="#">FİRMALAR</a></li>

                        </ol>
                    </div>
                </div>
              </div>
                <div class="results-info">
                    <p>Results: {{count($companies)}}</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="image-gallery gallery-type-4">
                        @foreach($companies as $company)
                        <div class="gallery-item" style="width: 248px; height: 188px;">
                            <a href="{{url('company/'.$company->slug)}}" class="item" style="background-size: contain;background-repeat: no-repeat;">
                                <img src="{{$company->user->image}}" alt="image" width="188" height="141" >
                                <span class="head">{{$company->name}}</span>

                            </a>
                        </div>
                            @endforeach()
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
