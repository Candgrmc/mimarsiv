@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            @include('layouts.profileTop')
            @include('company.buttons')

            <div class="image-gallery-holder">
                <h2 class="gallery-title">ÜRÜNLER</h2>
                <div class="image-gallery one-row-slider">

                    @foreach(request()->user()->products as $item)
                        <div class="gallery-item" style="height: 141px;">
                            <a href="/product/{{$item->slug}}" class="item">
                                <img src="{{$item->image}}" alt="image" width="188" height="141">
                                <span class="text">{{$item->name}}</span>
                            </a>
                        </div>
                    @endforeach
                </div>

                <div class="text-right">

                    <a href="/company/product/add" class="button-plain">ÜRÜN EKLE</a>
                </div>
            </div>

            <div class="image-gallery-holder">
                <h2 class="gallery-title">PROJELER</h2>
                <div class="image-gallery one-row-slider">
                    @foreach(request()->user()->products as $product)

                    @foreach($product->projects as $project)

                        <div class="gallery-item" style="height: 141px;">
                            <a href="/project_detail/{{$project->slug}}" class="item">
                                <img src="{{asset($project->image)}}" alt="image" width="188" height="141">
                                <span class="text">{{$project->name}}</span>
                            </a>
                        </div>
                    @endforeach
                        @endforeach

                </div>

            </div>
        </div>
</div>
@endsection