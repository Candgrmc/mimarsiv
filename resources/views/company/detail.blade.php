
@extends('layouts.app')
<div id="wrapper" class="common-page danishman-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')


        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="sidebar-box">
                        @if($company && $user)
                        <div class="icon-box">
                            <img src="@if($user->image){{asset($user->image)}} @else {{asset('images/default.jpeg')}} @endif" alt="client" width="130" height="87">
                        </div>

                        <p>{{$company->name}}</p>
                        <address>
                            <strong>Telefon</strong> :{{$company->phone}} <br>
                            <strong>Adres</strong> :{{$company->address}} <br>
                            <strong>Web</strong> :{{$company->url}}
                        </address>
                        @endif

                    </div>

                   {{-- <div class="sidebar-box">
                        <h2>find retailers</h2>
                        <form action="#" class="find-retailers">
                            <div class="select-inputs-wrap">
                                <div class="select-input">
                                    <select class="large">
                                        <option>COUNTRY</option>
                                        <option>NEPAL</option>
                                        <option>USA</option>
                                        <option>CHINA</option>
                                    </select>
                                </div>
                                <div class="select-input">
                                    <select class="large">
                                        <option>STATE</option>
                                        <option>STATE 1</option>
                                        <option>STATE 2</option>
                                        <option>STATE 3</option>
                                    </select>
                                </div>
                                <div class="select-input inline">
                                    <select class="large">
                                        <option>CITY</option>
                                        <option>CITY 1</option>
                                        <option>CITY 2</option>
                                        <option>CITY 3</option>
                                    </select>
                                </div>
                                <div class="select-input inline button-wrap">
                                    <button type="submit"><span>search</span></button>
                                </div>
                            </div>
                        </form>
                    </div>--}}
                </div>
                <div class="col-sm-8">
                  <article class="article" style="margin-bottom:80px">
                      @if($company)
                        {!! $company->description !!}
                      @endif
                  </article>
                  <h3 class="article-h3 padding-bottom-5">{{trans('main.productsofcompany')}}</h3>
                  <div class="image-gallery gallery-type-2 col-md-12 col-xs-12 col-sm-12">

                      @foreach($user->products as $product)
                          <div class="gallery-item" style=" height: 188px;">
                              <a href="product_detail/{{$product->slug}}" class="item col-md-4 col-xs-3 col-sm-3">
                                  <img src="{{$product->image}}" alt="image" height="188">
                                  <span class="head">{{$product->name}}</span>
                                  <span class="text">
                                  {{$product->name}} <br>
                                      @if($product->category)
                                        {{$product->category->title}} <br>
                                      @endif
                              </span>
                              </a>
                          </div>
                          @endforeach
                  </div>



                </div>
            </div>
        </div>
</div>
@endsection
