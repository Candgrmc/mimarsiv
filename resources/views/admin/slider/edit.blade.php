@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Slider Düzenle</h3>
        </div>

        <div class="panel-body">
            <br />
            {!! Form::open(array('action' => array('Admin\SliderController@update', $slider->id), 'method'=> 'patch','files'=> true)) !!}


            <div class="form-group">
                <label class="col-sm-5 control-label">Resim</label>
                <div class="col-sm-7 control-label">
                    <img src="{{$slider->image}}" alt="" style="width: 350px;">
                </div>
            </div>


                <div class="form-group">
                    <label class="col-sm-5 control-label">Başlık</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="title" class="form-control" required="required" value="{{$slider->title}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Resim Yazısı</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="description" class="form-control" required="required" value="{{$slider->description}}">
                    </div>
                </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Resim Linki</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="redirectTo" class="form-control" value="{{$slider->redirectTo}}">
                </div>
            </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Sıra</label>
                    <div class="col-sm-7 control-label">
                        <input type="number" name="order" class="form-control" required="required" value="{{$slider->order}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Aktiflik Durumu</label>
                    <div class="col-sm-7 control-label">
                        <div class="ckbox ckbox-success">
                            <input type="checkbox" id="checkboxSuccess" checked="checked" name="isActive" />
                            <label for="checkboxSuccess"></label>
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Resim</label>
                    <div class="col-sm-7 control-label">
                        <input type="file" name="image">
                    </div>
                </div>
                <input type="submit" value="Kaydet">
                        {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




