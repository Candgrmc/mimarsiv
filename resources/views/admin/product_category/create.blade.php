@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Ürün Kategorisi Ekle</h3>
        </div>

        <div class="panel-body">
            <br />
            <form action="{{ URL::to('/yonetici/product_category') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-5 control-label">Üst Kategori</label>
                    <div class="col-sm-7 control-label">
                        <select name="category_id" id="category_id">
                            <option selected="selected" value=""> Lütfen Seçim Yapınız</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->title}}</option>
                             @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">İsim</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="title" class="form-control" required="required">
                    </div>
                </div>
            
                <input type="submit" value="Kaydet">
            </form>
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection
