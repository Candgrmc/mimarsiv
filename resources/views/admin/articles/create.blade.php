@extends('admin.layouts.app')
<header id="header">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Makale Ekle</h3>
        </div>

        <div class="panel-body">
            <br />
            <form action="{{ URL::to('/yonetici/articles') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-5 control-label">Makale Tipi</label>
                    <div class="col-sm-7 col-md-7 col-xs-7 control-label">
                        <select class="form-control" name="article_type" id="article_type" onchange="changeForm($(this).val())">
                          <option value="">Seçiniz..</option>
                            <option value="1">Danışman </option>
                            <option value="2">Mimar </option>
                            <option value="3">Malzemeci </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Başlık</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="title" class="form-control" required="required">
                    </div>
                </div>

                <div class="form-group" id="article_about" style="display:none">
                    <label class="col-sm-5 control-label">Makale Kişisi</label>
                    <div class="col-sm-7">

                        <select name="user" class="hidden" data-placeholder="Lütfen Seçiniz..."  id="user_select">
                          <option value="">Seçiniz..</option>
                            @foreach($users as $user)
                              <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Anasayfa Resmi</label>
            <div class="col-sm-7 control-label">
                <input type="file" name="main_image" required="required">


            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Detay Resmi</label>
            <div class="col-sm-7 control-label">
                <input type="file" name="detail_image" required="required">


            </div>
        </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">İçerik</label>
                    <div class="col-sm-7 control-label">
                        <textarea class="form-control" rows="5" name="text" required="required"></textarea>
                    </div>
                </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Anahtar Kelimeler</label>
            <div class="col-sm-7 control-label">
                <input  name="keywords" class="form-control" id="tags">
                <span class="help-block">Kelimeleri virgül ile ayırınız.</span>

            </div>
        </div>

                <input type="submit" value="Kaydet">

            </form>
        </div><!-- panel-body -->
    </div><!-- panel -->
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script type="text/javascript">

    $("#user_select").select2({});

    function changeForm(val){
    if(val == 1){
      $('#article_about').fadeIn('fast')
    }else{
      $('#article_about').fadeOut('fast')
    }
  }



    </script>
@endsection
