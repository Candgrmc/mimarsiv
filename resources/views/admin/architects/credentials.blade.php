@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="top-normal-banner">
                <div class="icon-box">
                    <img src="{{request()->user()->image}}" height="47" width="94" alt="">
                </div>
                <img src="{{asset('images/img-banner04.png')}}" height="960" width="1920" alt="">
            </div>
            <div class="buttons-list">
                <a href="/company" class="button-green"><span>MIMARSIVIM</span></a>
                <a href="#" class="button-green"><span>BILGILERIM</span></a>
                <a href="{{ route('logout') }}" class="button-green" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">ÇIKIŞ YAP</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </div>
            <form action="" >
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" value="{{request()->user()->email}}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="name">İsim</label>
                        <input type="text" class="form-control" value="{{request()->user()->name}}" required>
                    </div>

                    <div class="form-group">
                        <label for="password">Şifre</label>
                        <input type="password" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="password-repeat">Şifre Tekrar</label>
                        <input type="password-repeat" class="form-control" >
                    </div>

                    <button type="submit" class="button-green" style="margin-bottom:20px">Güncelle</button>
                </div>


            </form>
        </div>
</div>
@endsection

