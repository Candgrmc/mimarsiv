@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Partner Düzenle</h3>
        </div>

        <div class="panel-body">
            <br />
            {!! Form::open(array('action' => array('Admin\PartnerController@update', $partner->id), 'method'=> 'patch','files'=> true)) !!}


            <div class="form-group">
                <label class="col-sm-5 control-label">Resim</label>
                <div class="col-sm-7 control-label">
                    <img src="{{$partner->image}}" alt="" style="width: 350px;">
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-5 control-label">İsim</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="name" class="form-control" required="required" value="{{$partner->name}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Websitesi</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="website" class="form-control" value="{{$partner->website}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Resim</label>
                <div class="col-sm-7 control-label">
                    <input type="file" name="image">
                </div>
            </div>
            <input type="submit" value="Kaydet">
            {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




