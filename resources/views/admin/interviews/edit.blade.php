@extends('admin.layouts.app')
<header id="header">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Röportaj Düzenle</h3>
        </div>

        <div class="panel-body">
            <br />
            {!! Form::open(array('action' => array('Admin\InterviewController@update', $interview->id), 'method'=> 'patch','files'=> true)) !!}
            <div class="form-group">
                <label class="col-sm-5 control-label">Başlık</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="title" class="form-control" value="{{$interview->title}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Röportaj Kişisi</label>
                <div class="col-sm-7">

                    <select name="user" class="hidden" data-placeholder="Lütfen Seçiniz..." required="required" id="user_select">
                      <option value="">Seçiniz..</option>
                        @foreach($users as $user)
                          <option @if($interview->user_id == $user->id) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Resim</label>
                <div class="col-sm-7 control-label">
                    <input type="file" name="image" >
                    <span class="help-block">Kapak Resmi</span>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">İçerik</label>
                <div class="col-sm-7 control-label">
                    <textarea class="form-control" rows="5" name="text">{{$interview->content}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Anahtar Kelimeler</label>
                <div class="col-sm-7 control-label">
                    <input  name="keywords" class="form-control" id="tags" value="{{$interview->keywords}}">
                    <span class="help-block">Kelimeleri virgül ile ayırınız.</span>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Kısa Açıklama</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="meta_description" class="form-control" value="{{$interview->meta_description}}">
                </div>
            </div>
            <input type="submit" value="Kaydet">
            {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel -->
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
  <script type="text/javascript">

  $("#user_select").select2({});
  </script>
@endsection
