@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Zanaatkar Kategorisi Düzenle</h3>
        </div>

        <div class="panel-body">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <br />
            {!! Form::open(array('action' => array('Admin\ArtisanController@update_category', $category->id), 'method'=> 'post')) !!}



            <div class="form-group">
                <label class="col-sm-5 control-label">İsim</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="title" class="form-control" required="required" value="{{$category->title}}">
                </div>
            </div>


                <input type="submit" value="Kaydet">
                        {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection
