@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Sayfa Ekle</h3>
        </div>

        <div class="panel-body">
            <br />
            <form action="{{ URL::to('/yonetici/pages') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-5 control-label">Başlık</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="title" class="form-control" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Kısa İsim</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="slug" class="form-control">
                        <span class="help-block">Lütfen Türkçe karakter ve boşluk kullanmayınız <br>
                            Örnek : Sayfa İsmi -> <i>sayfa-ismi</i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Aktiflik Durumu</label>
                    <div class="col-sm-7 control-label">
                        <div class="ckbox ckbox-success">
                            <input type="checkbox" id="checkboxSuccess" checked="checked" name="isActive" />
                            <label for="checkboxSuccess"></label>
                        </div>

                    </div>
                </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Resim</label>
            <div class="col-sm-7 control-label">
                <input type="file" name="image" required="required">
                <span class="help-block">Kapak Resmi</span>

            </div>
        </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">İçerik</label>
                    <div class="col-sm-7 control-label">
                        <textarea class="form-control" rows="5" name="text" required="required"></textarea>
                    </div>
                </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Anahtar Kelimeler</label>
            <div class="col-sm-7 control-label">
                <input  name="keywords" class="form-control" id="tags">
                <span class="help-block">Kelimeleri virgül ile ayırınız.</span>

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Kısa Açıklama</label>
            <div class="col-sm-7 control-label">
                <input type="text" name="meta_description" class="form-control">
            </div>
        </div>
                <input type="submit" value="Kaydet">
            </form>
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




