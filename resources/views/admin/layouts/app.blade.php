<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../admin/images/favicon.png" type="image/png">

    <title>Mimarsiv Yönetici Paneli</title>
    <link href="{{asset('admin/css/style.default.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{asset('admin/js/html5shiv.js')}}"></script>
    <script src="{{asset('admin/js/respond.min.js')}}"></script>
    <![endif]-->


    <link href="{{asset('admin/css/style.default.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/jquery.datatables.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/dropzone.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/jquery.tagsinput.css')}}" rel="stylesheet">


    <style>


        #delBtn:hover
        {
            background-color: #e7e7e7 !important;
            color: #333 !important;
            display: block !important;
        }
    </style>
</head>

<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<div class="mainpanel">
@yield("content")
</div>
</body>

<script src="{{asset('admin/js/jquery-1.11.1.min.js')}}"></script>

<script src="{{asset('admin/js/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('admin/js/jquery-ui-1.10.3.min.js')}}"></script>
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/modernizr.min.js')}}"></script>
<script src="{{asset('admin/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('admin/js/toggles.min.js')}}"></script>
<script src="{{asset('admin/js/retina.min.js')}}"></script>
<script src="{{asset('admin/js/jquery.cookies.js')}}"></script>

<script src="{{asset('admin/js/flot/jquery.flot.min.js')}}"></script>
<script src="{{asset('admin/js/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('admin/js/flot/jquery.flot.spline.min.js')}}"></script>
<script src="{{asset('admin/js/morris.min.js')}}"></script>
<script src="{{asset('admin/js/raphael-2.1.0.min.js')}}"></script>
<script src="{{asset('admin/js/dropzone.min.js')}}"></script>
<script src="{{asset('admin/js/jquery.tagsinput.min.js')}}"></script>
<script src="{{asset('admin/js/custom.js')}}"></script>

<script src="{{asset('admin/js/dashboard.js')}}"></script>


<script src="{{asset('admin/js/jquery.datatables.min.js')}}"></script>
<script src="{{asset('admin/js/select2.min.js')}}"></script>

<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script>
    $('textarea').ckeditor();
    $('#table1').DataTable({
      "pageLength": 5
    });

        $('#instagram_search_button').on('click',function(){
          var page = 1;
          var value = $('#instagram_search_text').val();
          $('.panel-footer').slideDown('fast')

          getPhotos(value)
        })



        function getPhotos(value){
          $('#instagram_results').html('')
          $('#result_count span').html('');
          var firstLetter = value.slice(0,1);
          if(firstLetter === '#'){
            var tag = value.substring(1);
            $.getJSON('https://www.instagram.com/explore/tags/'+tag+'/?__a=1',function(res){
              var results = res.graphql.hashtag.edge_hashtag_to_media

              $('#result_count span').html(results.count)

              console.log(results)
              $('#instagram_results').html('')
              results.edges.forEach(function(item){

                console.log(item.node.display_url)
                $('#instagram_results').append('<div class="col-md-2 col-sm-2 col-xs-6" style="margin-bottom:20px">'+
                '<img src="'+item.node.display_url+'" class="img-responsive"">'+
                '<div class="row" style="padding:10px">'+
                '<a href="'+item.node.display_url+'"  target="_blank" class="btn-sm btn-primary" style="border-radius:0;"><i class="fa fa-eye"></i> </a>'+
                ' <a href="javascript:void(0)" onclick="savePhoto(this)" class="btn-sm btn-success" data-owner="" data-hashtag="'+value+'" data-img="'+item.node.display_url+'" style="border-radius:0;"><i class="fa fa-plus"></i></a>'+
                ' </div></div>')
              })

            })
          }
          else if(firstLetter == '@'){
            $.getJSON('https://www.instagram.com/'+value.substring(1)+'/?__a=1',function(res){
              console.log(res)
              var count = res.user.media.count;
              var results = res.user.media.nodes
              var pageInfo = res.user.media.page_info;
              $('#result_count').slideDown('fast')
              $('#result_count span').html(count)
              console.log(results)


              results.forEach(function(item){

                console.log(item.display_src)

                $('#instagram_results').append('<div class="col-md-2 col-sm-2 col-xs-6" style="margin-bottom:20px">'+
                '<img src="'+item.display_src+'" class="img-responsive" >'+
                '<div class="row" style="padding:10px">'+
                '<a href="'+item.display_src+'"  target="_blank" class="btn-sm btn-primary" style="border-radius:0;"><i class="fa fa-eye"></i> </a>'+
                ' <a href="javascript:void(0)" onclick="savePhoto(this)" class="btn-sm btn-success" data-owner="'+value+'" data-hashtag="" data-img="'+item.display_src+'" style="border-radius:0;"><i class="fa fa-plus"></i></a>'+
                ' </div></div>')

              })

            })
          }else{
            alert('Hatalı Arama')
          }
        }


        function savePhoto(el){
          var link = $(el).attr('data-img');
          var type = $('#save_type').val();

          var owner = $(el).attr('data-owner');
          var hashtag = $(el).attr('data-hashtag')

          $.post('/yonetici/save_photo',{url:link,type:type,hashtag:hashtag,owner:owner},function(res){
              if(res.status){
                $(el).remove()

              }
          })
        }

    // $('.textarea').ckeditor(); // if class is prefered.
</script>

</body>
</html>
