<div class="leftpanel">

        <div class="logopanel">
            <h1><span>[</span> mimarsiv <span>]</span></h1>
        </div><!-- logopanel -->

        <div class="leftpanelinner">

            <h5 class="sidebartitle">Menü</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket">
                <li><a href="{{url("yonetici")}}"><i class='fa fa-dashboard'></i> <span>Kontrol Paneli</span></a></li>
                <li><a href="{{url("yonetici/sliders")}}"><i class='fa fa-image'></i> <span>Slider</span></a></li>
                <li class="nav-parent"><a href="#"><i class="fa fa-shopping-basket"></i> <span>Ürünler</span></a>

                    <ul class="children">
                        <li><a href="{{url("yonetici/products")}}"><i class='fa fa-shopping-basket'></i> <span>Ürünler</span></a></li>
                        <li><a href="{{url("yonetici/product_category")}}"><i class='fa fa-folder-open'></i> <span>Ürün Kategorileri</span></a></li>
                        <li><a href="{{url("yonetici/product_settings")}}"><i class='fa fa-money '></i> <span>Ürün Fiyat Aralığı</span></a></li>

                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-child"></i> <span>Üyelik Kategorileri</span></a>
                    <ul class="children">
                        <li><a href="{{url('/yonetici/artisan_categories')}}"><i class='fa fa-barcode'></i> <span>Zanaatkar Kategorileri</span></a></li>
                        <li><a href="{{url('/yonetici/consultant_categories')}}"><i class='fa fa-barcode'></i> <span>Danışman Kategorileri</span></a></li>
                    </ul>
                </li>
                <li class="nav-parent"><a href="#"><i class="fa fa-building"></i> <span>Projeler</span></a>
                    <ul class="children">
                        <li><a href="{{url("yonetici/projects")}}"><i class='fa fa-shopping-basket'></i> <span>Projeler</span></a></li>
                        <li><a href="{{url("yonetici/project_category")}}"><i class='fa fa-folder-open'></i> <span>Proje Tipleri</span></a></li>
                        <li><a href="{{url("yonetici/project_settings")}}"><i class='fa fa-money '></i> <span>Proje Fiyat Aralığı</span></a></li>

                    </ul>
                </li>
                <li class="nav-parent"><a href="#"><i class="fa fa-users"></i> <span>Üyeler</span></a>
                    <ul class="children">
                        <li><a href="{{url("yonetici/users")}}"><i class='fa fa-users'></i> <span>Bütün Kullanıcılar</span></a></li>
                        <li><a href="{{url("yonetici/students")}}"><i class='fa fa-users'></i> <span>Öğrenciler</span></a></li>
                        <li><a href="{{url("yonetici/architects")}}"><i class='fa fa-users'></i> <span>Mimarlar</span></a></li>
                        <li><a href="{{url("yonetici/offices")}}"><i class='fa fa-users'></i> <span>Ofisler</span></a></li>
                        <li><a href="{{url("yonetici/companies")}}"><i class='fa fa-users'></i> <span>Firma</span></a></li>
                        <li><a href="{{url("yonetici/consultants")}}"><i class='fa fa-users'></i> <span>Danışmanlar</span></a></li>
                        <li><a href="{{url("yonetici/artisans")}}"><i class='fa fa-users'></i> <span>Zanaatkârlar</span></a></li>


                    </ul>
                </li>
                <li class="nav-parent"><a href="#"><i class="fa fa-newspaper-o"></i> <span>İçerik Yönetimi</span></a>
                    <ul class="children">
                        <li><a href="{{url("yonetici/pages")}}"><i class='fa fa-newspaper-o'></i> <span>Sayfalar</span></a></li>
                        <li><a href="{{url("yonetici/news")}}"><i class='fa fa-newspaper-o'></i> <span>Haberler</span></a></li>
                        <li><a href="{{url("yonetici/interviews")}}"><i class='fa fa-microphone'></i> <span>Röportajlar</span></a></li>
                        <li><a href="{{url("yonetici/articles")}}"><i class='fa fa-newspaper-o'></i> <span>Makaleler</span></a></li>
                        <li><a href="{{url("yonetici/instagram")}}"><i class='fa fa-instagram'></i> <span>Instagram</span></a></li>
                        <li><a href="{{url("yonetici/articles")}}"><i class='fa fa-newspaper-o'></i> <span>Ayın Konsepti</span></a></li>
                        <li><a href="{{url("yonetici/articles")}}"><i class='fa fa-newspaper-o'></i> <span>Sanat Dünyası</span></a></li>

                        <li><a href="{{url("yonetici/brands")}}"><i class='fa fa-suitcase'></i> <span>Markalar</span></a></li>
                        <li><a href="{{url("yonetici/partners")}}"><i class='fa fa-suitcase'></i> <span>Partnerler</span></a></li>
                    </ul>
                </li>


                </ul>


        </div><!-- leftpanelinner -->
    </div><!-- leftpanel -->
