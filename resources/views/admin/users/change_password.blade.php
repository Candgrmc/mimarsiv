@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Şifre Değiştir</h3>
        </div>

        <div class="panel-body">
            <br />
            {!! Form::open(array('action' => array('Admin\UserController@setUserPassword', $id), 'method'=> 'patch','files'=> false)) !!}


            <div class="form-group">
                <label class="col-sm-5 control-label">Şifre</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="password" class="form-control" value="">
                </div>
            </div>

            <input type="submit" value="Kaydet">
            {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




