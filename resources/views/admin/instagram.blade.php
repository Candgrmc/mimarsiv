@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')



    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Instagram Yönetimi</h3>
             </div>

        <div class="panel-body">


            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr>
                        <th>Resim</th>
                        <th>Instagram İsmi</th>
                        <th>Tip</th>
                        <th>İşlem</th>

                    </tr>
                    </thead>
                    <tbody>
                      @foreach($photos as $photo)

                      <tr class="gradeA">
                        <td> <img src="{{$photo->url}}" alt="" height="100" width="100"> </td>
                        <td>{{($photo->owner) ? $photo->owner : $photo->hashtag}}</td>
                        <td>{{($photo->type == 'Hashtag')? 'MimShare' : $photo->type}}</td>
                        <td> <a href="{{url('yonetici/delete_photo/'.$photo->id)}}" class="btn-sm btn-danger"> <i class="fa fa-trash"></i> </a> </td>
                      </tr>
                      @endforeach

                    </tbody>
                </table>
            </div><!-- table-responsive -->

        </div><!-- panel-body -->
    </div><!-- panel -->

    <div class="panel panel-default">
        <div class="panel-body">
          <form action="{{url('yonetici/push-concept')}}" method="post">
            {{csrf_field()}}
            <label for="concept">Ayın Konsepti</label>
            <input type="text" name="concept" value="{{(isset($concept))? $concept : null}}" class="form-control">
            <button type="submit" name="button" class="btn btn-sm btn-primary" style="border-radius:0;margin-top:10px"> <i class="fa fa-plus"></i> Ekle</button>
          </form>

          <label for="instagram_search_text">Instagram Adı</label>
          <p class="text-danger">
            <small>Taglerde aramak için kelime başına # ekleyiniz (örn: #ornek)</small>
          </p>
          <p class="text-success">
          <small>Kullanıcı aramak için kelime başına @ ekleyiniz (örn: @ornek)</small>
        </p>
            <input type="text" id="instagram_search_text" class="form-control">
            <button type="button" name="button" id="instagram_search_button" class="btn btn-primary" style="margin-top:15px"><i class="fa fa-search"></i>  Ara</button>
        </div>
        <div class="panel-footer" style="display:none">
            <div class="type_select">
                <select class="form-control" name="type" id="save_type">
                    <option value="Sanat Dunyasi">Sanat Dünyası</option>
                    <option value="Miminsta">Miminsta</option>
                    <option value="Mimshots">Mimshots</option>
                    <option value="Hashtag">Mimshare</option>
                </select>
            </div>
            <p id="result_count" >Sonuç: <span ></span> </p>

            <div id="instagram_results" class="col-md-12 col-xs-12 col-sm-12">

            </div>
        </div>
    </div>



@endsection
