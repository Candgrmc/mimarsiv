@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Proje Fiyat Düzenle</h3>
        </div>

        <div class="panel-body">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <br />
                <form action="{{url('yonetici/update_project_settings')}}" method="post">
            {{csrf_field()}}

            <div class="form-group">
                <label class="col-sm-5 control-label">Fiyat Aralığı</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="price" class="form-control" required="required">
                </div>
            </div>


            <input type="submit" value="Kaydet">
                </form>

                <ul style="list-style:none">
                    @foreach($settings as $setting)
                        <li>{{$setting->price}}</li>
                    @endforeach
                </ul>
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection
