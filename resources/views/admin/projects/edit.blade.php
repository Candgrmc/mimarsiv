@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Proje Düzenle</h3>
        </div>

        <div class="panel-body">

              <div class="row" style="margin-bottom:150px">
                @foreach($images as $image)
                <div class="col-md-2 col-xs-2 col-sm-2">
                  <a href="/yonetici/product/delete_image/{{$image->id}}" class="pull-right" style="z-index:100;color:red;"><i class="fa fa-trash" style="z-index:100"></i></a>
                  <img src="{{$image->image}}" alt="image" width="188" height="141" style="z-index:1!important">

                </div>

                @endforeach()
              </div>


            {!! Form::open(array('action' => array('Admin\ProjectController@update', $project->id), 'method'=> 'patch','files'=> true)) !!}
            <div class="form-group">
                <label class="col-sm-5 control-label">Başlık</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="title" class="form-control" required="required" value="{{$project->name}}">
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Tipi</label>
                <div class="col-sm-5">
                    <select name="category" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                        <option value=""></option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" @if($category->id == $project->project_category_id) selected="selected" @endif>{{$category->title}}</option>
                    @endforeach
                </div>
                </select>
            </div>

            <div class="form-group">
                <label class="col-sm-5 control-label">Mimari Ofis</label>
                <div class="col-sm-5">
                    <select name="investor" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                        <option value=""></option>

                       @if(count($offices))
                        @foreach($offices as $office)

                       <option value="{{$office->id}}" @if($office->id == $project->user_id) selected="selected" @endif>{{$office->name}}</option>

                    	@endforeach
                    @endif()
                </div>
                </select>
            </div>




            <div class="form-group">
                <label class="col-sm-5 control-label">Resimler</label>
                <div class="col-sm-7 control-label">
                    <input type="file" name="image[]" multiple>
                    <span class="help-block">Birden fazla resim seçerek galeri oluşturabilirsiniz</span>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Hikayesi</label>
                <div class="col-sm-7 control-label">
                    <textarea class="form-control" rows="5" name="story" required="required">{{$project->story}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Yılı</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="year" class="form-control" value="{{$project->year}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Ülkesi</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="country" class="form-control" value="{{$project->country}}">
                </div>
            </div>

            <input type="submit" value="Kaydet">
            {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel -->

     <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title"> Danışmanlar & Zanaatkarlar</h3>
        </div>

        <div class="panel-body">
        	<ul style="list-style:none">
				@foreach($project->consultants as $consultant)
        		<li>{{$consultant->name}}</li>
        	@endforeach
        	</ul>
        </div>


        </div>

@endsection
