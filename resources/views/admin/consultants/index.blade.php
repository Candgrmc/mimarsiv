@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Danışman Yönetimi</h3>
             </div>

        <div class="panel-body">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <a class="btn btn-success" href="#">Yeni Ekle</a><br>
            <br />
            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr>
                        <th>İsim</th>
                        <th>Firma</th>
                        <th>Islemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($consultants as $consultant)
                    @php
                        $company = \App\Consultant::where('user_id',$consultant->id)->first();
                    @endphp
                    <tr class="gradeA">
                        <td>{{$consultant->name}}</td>
                        <td class="center">@if($company){{$company->name}} @endif</td>
                        <td class="center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">
                                    İşlemler <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    {{--<li><a href="/yonetici/consultants/{{$consultant->id}}/edit" disabled="">Düzenle</a></li>--}}
                                    @if($consultant->approved == 0)
                                        <li><a href="/yonetici/activate/{{$consultant->user->id}}">Üyeliği Aktifleştir</a></li>

                                    @else()
                                        <li><a href="/yonetici/deactivate/{{$consultant->id}}">Üyeliği Pasifleştir</a></li>

                                    @endif()
                                    <li><a href="#" disabled="">Düzenle</a></li>
                                    <li><a href="#" disabled="">Sil</a></li>
                                    <li><a href="#" disabled="">Görüntüle</a></li>
                                    <li><a href="#" disabled="">Firmayı Görüntüle</a></li>
                                    {{--{!! Form::open(array('action' => array('Admin\BrandController@destroy', $consultant->id), 'method'=> 'delete')) !!}
                                    <input type="submit" value="Sil" style="border: none;background-color: transparent;outline: none;display: block !important;width: 100%;text-align: left;" id="delBtn">
                                    {!! Form::close() !!}--}}
                                </ul>
                            </div><!-- btn-group -->

                            </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- table-responsive -->

        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection
