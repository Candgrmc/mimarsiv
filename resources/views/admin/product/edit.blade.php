@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Ürün Düzenle</h3>
        </div>

        <div class="panel-body">
            <h3>Resimler</h3>


                <div class="row" style="margin-bottom:25px">
                  @foreach($images as $image)
                  <div class="col-md-2 col-xs-2 col-sm-2">
                    <a href="{{url('yonetici/product/delete_image/'.$image->id)}}" class="pull-right" style="z-index:100;color:red;"><i class="fa fa-trash" style="z-index:100"></i></a>
                    <img src="{{$image->image}}" alt="image" width="188" height="141">
                  </div>
                  @endforeach()
                </div>

            <br />
            {!! Form::open(array('action' => array('Admin\ProductController@update', $product->id), 'method'=> 'patch','files'=> true)) !!}
            <div class="form-group">
                <label class="col-sm-5 control-label">Başlık</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="title" class="form-control" required="required" value="{{$product->name}}">
                </div>
            </div>
          
            <div class="form-group">
                <label class="col-sm-5 control-label">Ürün Kategorisi</label>
                <div class="col-sm-5">

                    <select name="category" class="select2" data-placeholder="Lütfen Seçiniz...">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" @if($category->id == $product->category->id) selected="selected" @endif>{{$category->title}}</option>
                    @endforeach
                </div>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Sertifikalar</label>
            <div class="col-sm-7">
                <select class="select2" name="certificates[]"  multiple data-placeholder="Lütfen Seçiniz...">
                    <div class="col-sm-5">
                         <option value=""></option>
                        @foreach($certificates as $certificate)
                            @foreach($product->certificates as $item)
                                <option value="{{$certificate->id}}" @if($certificate->id == $item->certificate_id) selected="selected" @endif>{{$certificate->name}}</option>
                            @endforeach
                        @endforeach

                    </div>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Renk</label>
            <div class="col-sm-7 control-label">
                <input type="text" name="color" class="form-control" value="{{$product->color}}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Resim</label>
            <div class="col-sm-7 control-label">
                <input type="file" name="image[]" multiple="multiple">
                <span class="help-block">Birden fazla resim seçip ürün galerisi oluşturabilirsiniz</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Marka</label>
            <div class="col-sm-5">

                <select name="category" class="select2" data-placeholder="Lütfen Seçiniz...">
                    @foreach($brands as $brand)
                        <option value="{{$brand->id}}" @if($brand->id == $product->brand_id) selected="selected" @endif>{{$brand->name}}</option>
                @endforeach
            </div>
            </select>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Video</label>
            <div class="col-sm-7 control-label">
                <input type="text" name="video" class="form-control" value="{{$product->video}}">
                <span class="help-block">Youtube linki</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Katalog</label>
            <div class="col-sm-7 control-label">
                <input type="text" name="catalogue" class="form-control" value="{{$product->catalogue}}">
                <span class="help-block">Katalog adresini yazabilirsiniz</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Açıklama</label>
            <div class="col-sm-7 control-label">
                <textarea class="form-control" rows="5" name="overview">{{$product->overview}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">BİM</label>
            <div class="col-sm-7 control-label">
                <textarea class="form-control" rows="5" name="bim">{{$product->bim}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Fiyat</label>
            <div class="col-sm-7 control-label">
                <input type="text" name="price" class="form-control" value="{{$product->price}}">
            </div>
        </div>
        <input type="submit" value="Kaydet">
                        {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection
