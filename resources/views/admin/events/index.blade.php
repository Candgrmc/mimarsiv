@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Marka Yönetimi</h3>
             </div>

        <div class="panel-body">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <a class="btn btn-success" href="/yonetici/event/create">Yeni Ekle</a><br>
            <br />
            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr>
                        <th>İsim</th>
                        <th>Resim</th>
                        <th>Website</th>
                        <th>Sponsor Marka</th>
                        <th>Sponsor Email</th>
                        <th>Sponsor Resim</th>
                        <th>Başlangıç Tarihi</th>
                        <th>Bitiş Tarihi</th>
                        <th>Islemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($events as $event)
                    <tr class="gradeA">
                        <td>{{$event->name}}</td>
                       <td><img src="{{asset($event->image)}}" alt="" style="width: 150px;"></td>
                        <td class="center">{{$event->website}}</td>
                        <td>{{$event->sponsor_brand}}</td>
                        <td>{{$event->sponsor_email}}</td>
                        <td>@if($event->sponsor_image) <img src="{{asset($event->sponsor_image)}}" alt="">  @endif</td>
                        <td>{{$event->start_date}}</td>
                        <td>{{$event->end_date}}</td>
                        <td class="center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">
                                    İşlemler <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/yonetici/events/{{$event->id}}/edit">Düzenle</a></li>
                                    <li> <a href="{{url('/yonetici/event_delete/'.$event->id)}}">Sil</a> </li>
                                </ul>
                            </div><!-- btn-group -->

                            </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- table-responsive -->

        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection
