@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Etkinlik Ekle</h3>
        </div>

        <div class="panel-body">
            <br />
            <form action="{{ URL::to('/yonetici/events/add') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-5 control-label">İsim</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="name" class="form-control" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Websitesi</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="website" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Resim</label>
                    <div class="col-sm-7 control-label">
                        <input type="file" name="image" required="required" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Başlangıç Tarihi</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="start_date" required="required" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-5 control-label">Başlangıç Tarihi</label>
                    <div class="col-sm-7 control-label">
                        <input type="date" name="start_date" required="required" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Bitiş Tarihi</label>
                    <div class="col-sm-7 control-label">
                        <input type="date" name="end_date" required="required" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Sponsor Marka</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="sponsor_brand" required="required" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Sponsor Email</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="sponsor_email" required="required" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-5 control-label">Sponsor Resim</label>
                    <div class="col-sm-7 control-label">
                        <input type="file" name="sponsor_image" required="required">
                    </div>
                </div>

                <input type="submit" value="Kaydet">
            </form>
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection
