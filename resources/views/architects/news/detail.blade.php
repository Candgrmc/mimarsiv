
@extends('layouts.app')
<div id="wrapper" class="common-page haber-detay-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="row main-row">
                <div class="col-sm-4">
                    <div class="sidebar-box">
                        @if(count($news->user) > 1 || count($news->user->company) > 1)

                        <div class="icon-box">
                                <img src="{{$news->user->company->logo}}" alt="client" width="122" height="46">
                            </div>
                            <p>{{$news->user->company->name}}</p>
                            <address>
                                {{$news->user->company->phone}} <br>
                                {{$news->user->company->address}}
                                Web : {{$news->user->company->url}}
                            </address>
                        @endif()
                    </div>
                </div>
                <div class="col-sm-8" style="width: 787px; height: 343px; ">
                    <div style="width: 787px; height: 343px; background-size: cover;">
                    <img src="{{$news->image}}" height="343" width="787" alt="banner" >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <article class="article">
                    {!! $news->content !!}
                    </article>
                    <h3 class="article-h3 padding-bottom-5">DİĞER HABERLER</h3>
                    <div class="image-gallery gallery-type-3">
                        @foreach($otherNews as $item)
                        <div class="gallery-item">
                            <a href="/news/{{$item->slug}}" class="item">
                                <img src="{{$item->image}}" alt="image" width="188" height="141">
                                <span class="head">{{$item->title}}</span>

                            </a>
                        </div>
@endforeach
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection