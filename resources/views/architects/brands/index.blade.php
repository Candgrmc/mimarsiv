
@extends('layouts.app')
<div id="wrapper" class="common-page proje-listeleme">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')

        <div class="search-result-info">
            <div class="container">
                <div class="results-info">
                    <p>Results: {{count($brands)}}</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="image-gallery gallery-type-4">
                        @foreach($brands as $brand)
                        <div class="gallery-item" style="width: 248px; height: 188px;">
                            <a href="/products/{{$brand->slug}}" class="item">
                                <img src="{{$brand->image}}" alt="image" width="188" height="141">
                                <span class="head">{{$brand->name}}</span>

                            </a>
                        </div>
                            @endforeach()
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection