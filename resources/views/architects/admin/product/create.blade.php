@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Ürün Ekle</h3>
        </div>

        <div class="panel-body">
            <br />
            <form action="{{ URL::to('/yonetici/products') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-5 control-label">Başlık</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="title" class="form-control" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Kısa İsim</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="slug" class="form-control">
                        <span class="help-block">Lütfen Türkçe karakter ve boşluk kullanmayınız <br>
                            Örnek : Ürün İsmi -> <i>urun-ismi</i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Ürün Kategorisi</label>
                    <div class="col-sm-5">

                        <select name="category"class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                            <option value=""></option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->title}}</option>
                            @endforeach
                            </div>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-5 control-label">Sertifikalar</label>
                    <div class="col-sm-7">
                        <select class="select2" name="certificates[]"  multiple data-placeholder="Lütfen Seçiniz...">
                            <div class="col-sm-5">
                                <option value=""></option>
                                @foreach($certificates as $certificate)
                                    <option value="{{$certificate->id}}">{{$certificate->name}}</option>
                                @endforeach
                            </div>
                        </select>
                    </div>
                </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Renk</label>
            <div class="col-sm-7 control-label">
                <input type="text" name="color" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Resim</label>
            <div class="col-sm-7 control-label">
                <input type="file" name="image[]" multiple="multiple" required="required">
                <span class="help-block">Birden fazla resim seçip ürün galerisi oluşturabilirsiniz</span>

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Marka</label>
            <div class="col-sm-5">

                <select name="brand" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                    <option value=""></option>
                    @foreach($brands as $brand)
                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                @endforeach
            </div>
            </select>
        </div>
    </div>

        <div class="form-group">
            <label class="col-sm-5 control-label">Video</label>
            <div class="col-sm-7 control-label">
                <input type="text" name="video" class="form-control">
                <span class="help-block">Youtube linki</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Katalog</label>
            <div class="col-sm-7 control-label">
                <input type="text" name="catalogue" class="form-control">
                <span class="help-block">Katalog adresini yazabilirsiniz</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Açıklama</label>
            <div class="col-sm-7 control-label">
                <textarea class="form-control" rows="5" name="overview"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">BİM</label>
            <div class="col-sm-7 control-label">
                <textarea class="form-control" rows="5" name="bim"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Fiyat</label>
            <div class="col-sm-7 control-label">
                <input type="text" name="price" class="form-control">
            </div>
        </div>
                <input type="submit" value="Kaydet">
            </form>
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




