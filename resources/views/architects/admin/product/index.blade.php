@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Ürün Yönetimi</h3>
             </div>

        <div class="panel-body">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <a class="btn btn-success" href="/yonetici/products/create">Yeni Ekle</a><br>
            <br />
            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr>
                        <th>Ürün Adı</th>
                        <th>Ekleyen</th>
                        <th>Resim</th>
                        <th>Kategori</th>
                        <th>Renk</th>
                        <th>Marka</th>
                        <th>Islemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                    <tr class="gradeA">
                        <td>{{$product->name}}</td>
                        <td>{{$product->user->name}}</td>
                        <td><img src="{{$product->image}}" alt="" style="width: 150px;"></td>
                       <td>{{$product->category->title}}</td>
                        <td>{{$product->color}}</td>
                        @if($product->brand)
                        <td>{{$product->brand->name}}</td>
                        @else
                            <td></td>
                        @endif
                        <td class="center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">
                                    İşlemler <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/yonetici/products/{{$product->id}}/edit">Düzenle</a></li>
                                    <li><a href="/product/{{$product->slug}}">Ürünü Görüntüle</a></li>
                                    <li><a href="{{$product->catalogue}}">Katalogu Görüntüle</a></li>
                                    <li><a href="{{$product->video}}">Videoyu Görüntüle</a></li>
                                    @if($product->featured == 0)
                                        <li><a href="/yonetici/setFeatured/{{$product->id}}">Ürünü Sitede Göster</a></li>
                                    @else()
                                        <li><a href="/yonetici/deSetFeatured/{{$product->id}}">Ürünü Sitede Gizle</a></li>
                                    @endif()
                                    <li><a href="/yonetici/product_gallery/{{$product->id}}">Galeri Görüntüle</a></li>
                                    {!! Form::open(array('action' => array('Admin\ProductController@destroy', $product->id), 'method'=> 'delete')) !!}
                                    <input type="submit" value="Sil" style="border: none;background-color: transparent;outline: none;display: block !important;width: 100%;text-align: left;" id="delBtn">
                                    {!! Form::close() !!}
                                </ul>
                            </div><!-- btn-group -->

                            </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- table-responsive -->

        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




