@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Kullanıcı Yönetimi</h3>
             </div>

        <div class="panel-body">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <a class="btn btn-success" href="#">Yeni Ekle</a><br>
            <br />
            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr>
                        <th>İsim</th>
                        <th>Mail</th>
                        <th>Yetki</th>
                        <th>Onay Durumu</th>
                        <th>Islemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr class="gradeA">
                        <td>{{$user->name}}</td>
                        <td class="center">{{$user->email}}</td>
                        <td class="center">{{$user->userType->name}}</td>
                        <td class="center">
                            @if($user->approved == 0)
                                <a href="/yonetici/activate/{{$user->id}}"><button class="btn btn-oblong btn-danger btn-block mg-b-10">Onaylanmadı</button></a>
                            @else()
                                <a href="/yonetici/deactivate/{{$user->id}}"><button class="btn btn-oblong btn-primary btn-block mg-b-10">Onaylandı</button></a>
                            @endif()

                        </td>
                        <td class="center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">
                                    İşlemler <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/yonetici/change_password/{{$user->id}}">Şifre Değiştir</a></li>

                                <!---<li><a href="/yonetici/users/{{$user->id}}/edit" disabled="disabled">Düzenle</a></li> ---->
                                    @if($user->approved == 0)
                                        <li><a href="/yonetici/activate/{{$user->id}}">Üyeliği Aktifleştir</a></li>

                                    @else()
                                        <li><a href="/yonetici/deactivate/{{$user->id}}">Üyeliği Pasifleştir</a></li>

                                    @endif()
                                    {!! Form::open(array('action' => array('Admin\UserController@destroy', $user->id), 'method'=> 'delete')) !!}
                                    <input type="submit" value="Sil" style="border: none;background-color: transparent;outline: none;display: block !important;width: 100%;text-align: left;" id="delBtn">
                                    {!! Form::close() !!}
                                </ul>
                            </div><!-- btn-group -->

                            </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- table-responsive -->

        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




