@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Haber Düzenle</h3>
        </div>
        <div class="panel-body">
            <br />
            {!! Form::open(array('action' => array('Admin\NewsController@update', $news->id), 'method'=> 'patch','files'=> true)) !!}
            <div class="form-group">
                <label class="col-sm-5 control-label">Başlık</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="title" class="form-control" value="{{$news->title}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Kısa İsim</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="slug" class="form-control" value="{{$news->slug}}">
                    <span class="help-block">Lütfen Türkçe karakter ve boşluk kullanmayınız <br>
                            Örnek : Haber İsmi -> <i>haber-ismi</i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Haber Kişisi</label>
                <div class="col-sm-5">

                    <select name="user" class="select2" data-placeholder="Lütfen Seçiniz...">
                        <option value=""></option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}" @if($user->id == $news->user_id) selected="selected" @endif>{{$user->name}}</option>

                    @endforeach
                </div>
                </select>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Resim</label>
                <div class="col-sm-7 control-label">
                    <input type="file" name="image" >
                    <span class="help-block">Kapak Resmi</span>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">İçerik</label>
                <div class="col-sm-7 control-label">
                    <textarea class="form-control" rows="5" name="text">{{$news->content}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Anahtar Kelimeler</label>
                <div class="col-sm-7 control-label">
                    <input  name="keywords" class="form-control" id="tags" value="{{$news->keywords}}">
                    <span class="help-block">Kelimeleri virgül ile ayırınız.</span>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Kısa Açıklama</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="meta_description" class="form-control" value="{{$news->meta_description}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Aktiflik Durumu</label>
                <div class="col-sm-7 control-label">
                    <div class="ckbox ckbox-success">
                        <input type="checkbox" id="checkboxSuccess" @if($news->isActive === 1) checked="checked" @endif name="isActive" />
                        <label for="checkboxSuccess"></label>
                    </div>

                </div>
            </div>
            <div class="form-group">

                <label class="col-sm-5 control-label">Anasayfada Gösterilsin mi?</label>
                <div class="col-sm-7 control-label">
                    <div class="ckbox ckbox-warning">
                        <input type="checkbox" id="checkboxWarning" @if($news->isFeatured === 1) checked="checked" @endif  name="isFeatured" />
                        <label for="checkboxWarning"></label>
                    </div>

                </div>
            </div>
            <input type="submit" value="Kaydet">
            {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




