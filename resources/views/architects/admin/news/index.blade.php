@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Röportaj Yönetimi</h3>
             </div>

        <div class="panel-body">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <a class="btn btn-success" href="/yonetici/news/create">Yeni Ekle</a><br>
            <br />
            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr>
                        <th>Başlık</th>
                        <th>Kişi</th>
                        <th>Resim</th>
                        <th>Kısa Açıklama</th>
                        <th>Aktiflik</th>
                        <th>Sitede Gösterim</th>
                        <th>Islemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($newsList as $new)
                    <tr class="gradeA">
                        <td>{{$new->title}}</td>
                        <td>{{$new->user->name}}</td>
                        <td><img src="{{$new->image}}" alt="" style="width: 150px;"></td>
                        <td>{{$new->meta_description}}</td>
                        <td>@if($new->isActive == true)
                                <span class="label label-success">Aktif</span>
                            @else()
                                <span class="label label-danger">Pasif</span>

                            @endif()</td>
                        <td>@if($new->isActive == true)
                                <span class="label label-success">Gösteriliyor</span>
                            @else()
                                <span class="label label-danger">Gösterilmiyor</span>

                            @endif()</td>
                        <td class="center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">
                                    İşlemler <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/yonetici/news/{{$new->id}}/edit">Düzenle</a></li>
                                    <li><a href="/news/{{$new->slug}}">Haberi Görüntüle</a></li>
                                    {!! Form::open(array('action' => array('Admin\NewsController@destroy', $new->id), 'method'=> 'delete')) !!}
                                    <input type="submit" value="Sil" style="border: none;background-color: transparent;outline: none;display: block !important;width: 100%;text-align: left;" id="delBtn">
                                    {!! Form::close() !!}
                                </ul>
                            </div><!-- btn-group -->

                            </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- table-responsive -->

        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




