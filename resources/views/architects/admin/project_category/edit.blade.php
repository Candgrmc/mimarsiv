@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Proje Kategorisi Düzenle</h3>
        </div>

        <div class="panel-body">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <br />
            {!! Form::open(array('action' => array('Admin\ProjectCategoryController@update', $projectCategory->id), 'method'=> 'patch','files'=> true)) !!}

                <div class="form-group">
                    <label class="col-sm-5 control-label">Üst Kategori</label>
                    <div class="col-sm-7 control-label">
                        <select name="category_id" id="category_id">
                            <option disabled="disabled"> Lütfen Seçim Yapınız</option>
                            @foreach($categories as $category)
                                @if($projectCategory->category_id == $category->id)
                                    <option value="{{$category->id}}" selected="selected" >{{$category->title}}</option>

                                @else()
                                    <option value="{{$category->id}}">{{$category->title}}</option>

                                @endif()
                            @endforeach
                        </select>
                    </div>
                </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">İsim</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="title" class="form-control" required="required" value="{{$projectCategory->title}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Kısa İsim</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="slug" class="form-control" value="{{$projectCategory->slug}}">
                </div>
            </div>

                <input type="submit" value="Kaydet">
                        {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




