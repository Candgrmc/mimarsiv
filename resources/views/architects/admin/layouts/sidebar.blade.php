<div class="leftpanel">

        <div class="logopanel">
            <h1><span>[</span> mimarsiv <span>]</span></h1>
        </div><!-- logopanel -->

        <div class="leftpanelinner">

            <h5 class="sidebartitle">Menü</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket">
                <li><a href="{{url("yonetici")}}"><i class='fa fa-dashboard'></i> <span>Kontrol Paneli</span></a></li>
                <li><a href="{{url("yonetici/sliders")}}"><i class='fa fa-image'></i> <span>Slider</span></a></li>
                <li class="nav-parent"><a href="#"><i class="fa fa-shopping-basket"></i> <span>Ürünler</span></a>
                    <ul class="children">
                        <li><a href="{{url("yonetici/products")}}"><i class='fa fa-shopping-basket'></i> <span>Ürünler</span></a></li>
                        <li><a href="{{url("yonetici/product_category")}}"><i class='fa fa-folder-open'></i> <span>Ürün Kategorileri</span></a></li>
                        <li><a href="{{url("yonetici/product_gallery")}}"><i class='fa fa-image'></i> <span>Ürün Galerileri</span></a></li>

                    </ul>
                </li>
                <li class="nav-parent"><a href="#"><i class="fa fa-building"></i> <span>Projeler</span></a>
                    <ul class="children">
                        <li><a href="{{url("yonetici/projects")}}"><i class='fa fa-shopping-basket'></i> <span>Projeler</span></a></li>
                        <li><a href="{{url("yonetici/project_category")}}"><i class='fa fa-folder-open'></i> <span>Proje Kategorileri</span></a></li>
                        <li><a href="{{url("yonetici/project_type")}}"><i class='fa fa-folder-open'></i> <span>Proje Tipleri</span></a></li>

                    </ul>
                </li>
                <li class="nav-parent"><a href="#"><i class="fa fa-users"></i> <span>Üyeler</span></a>
                    <ul class="children">
                        <li><a href="{{url("yonetici/users")}}"><i class='fa fa-users'></i> <span>Bütün Kullanıcılar</span></a></li>
                        <li><a href="{{url("yonetici/students")}}"><i class='fa fa-users'></i> <span>Öğrenciler</span></a></li>
                        <li><a href="{{url("yonetici/architects")}}"><i class='fa fa-users'></i> <span>Mimarlar</span></a></li>
                        <li><a href="{{url("yonetici/artisans")}}"><i class='fa fa-users'></i> <span>Zanaatkârlar</span></a></li>
                        <li><a href="{{url("yonetici/offices")}}"><i class='fa fa-users'></i> <span>Ofisler</span></a></li>
                        <li><a href="{{url("yonetici/investors")}}"><i class='fa fa-users'></i> <span>Yatırımcılar</span></a></li>
                        <li><a href="{{url("yonetici/consultants")}}"><i class='fa fa-users'></i> <span>Danışmanlar</span></a></li>
                    </ul>
                </li>
                <li class="nav-parent"><a href="#"><i class="fa fa-newspaper-o"></i> <span>İçerik Yönetimi</span></a>
                    <ul class="children">
                        <li><a href="{{url("yonetici/pages")}}"><i class='fa fa-newspaper-o'></i> <span>Sayfalar</span></a></li>
                        <li><a href="{{url("yonetici/news")}}"><i class='fa fa-newspaper-o'></i> <span>Haberler</span></a></li>
                        <li><a href="{{url("yonetici/articles")}}"><i class='fa fa-newspaper-o'></i> <span>Makaleler</span></a></li>
                        <li><a href="{{url("yonetici/bulletins")}}"><i class='fa fa-newspaper-o'></i> <span>Bültenler</span></a></li>
                        <li><a href="{{url("yonetici/interviews")}}"><i class='fa fa-user'></i> <span>Röportajlar</span></a></li>
                        <li><a href="{{url("yonetici/brands")}}"><i class='fa fa-suitcase'></i> <span>Markalar</span></a></li>
                        <li><a href="{{url("yonetici/partners")}}"><i class='fa fa-suitcase'></i> <span>Partnerler</span></a></li>
                    </ul>
                </li>

                </ul>


        </div><!-- leftpanelinner -->
    </div><!-- leftpanel -->
