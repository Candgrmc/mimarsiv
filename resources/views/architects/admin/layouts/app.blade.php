<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../admin/images/favicon.png" type="image/png">

    <title>Mimarsiv Yönetici Paneli</title>
    <link href="{{asset('admin/css/style.default.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{asset('admin/js/html5shiv.js')}}"></script>
    <script src="{{asset('admin/js/respond.min.js')}}"></script>
    <![endif]-->


    <link href="{{asset('admin/css/style.default.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/jquery.datatables.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/dropzone.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/jquery.tagsinput.css')}}" rel="stylesheet">


    <style>


        #delBtn:hover
        {
            background-color: #e7e7e7 !important;
            color: #333 !important;
            display: block !important;
        }
    </style>
</head>

<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<div class="mainpanel">
@yield("content")
</div>
</body>

<script src="{{asset('admin/js/jquery-1.11.1.min.js')}}"></script>

<script src="{{asset('admin/js/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('admin/js/jquery-ui-1.10.3.min.js')}}"></script>
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/modernizr.min.js')}}"></script>
<script src="{{asset('admin/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('admin/js/toggles.min.js')}}"></script>
<script src="{{asset('admin/js/retina.min.js')}}"></script>
<script src="{{asset('admin/js/jquery.cookies.js')}}"></script>

<script src="{{asset('admin/js/flot/jquery.flot.min.js')}}"></script>
<script src="{{asset('admin/js/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('admin/js/flot/jquery.flot.spline.min.js')}}"></script>
<script src="{{asset('admin/js/morris.min.js')}}"></script>
<script src="{{asset('admin/js/raphael-2.1.0.min.js')}}"></script>
<script src="{{asset('admin/js/dropzone.min.js')}}"></script>
<script src="{{asset('admin/js/jquery.tagsinput.min.js')}}"></script>
<script src="{{asset('admin/js/custom.js')}}"></script>

<script src="{{asset('admin/js/dashboard.js')}}"></script>


<script src="{{asset('admin/js/jquery.datatables.min.js')}}"></script>
<script src="{{asset('admin/js/select2.min.js')}}"></script>

<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script>
    $('textarea').ckeditor();
    // $('.textarea').ckeditor(); // if class is prefered.
</script>

</body>
</html>

