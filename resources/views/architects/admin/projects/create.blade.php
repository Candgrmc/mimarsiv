@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Proje Ekle</h3>
        </div>

        <div class="panel-body">
            <br />
            <form action="{{ URL::to('/yonetici/projects') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-5 control-label">Başlık</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="title" class="form-control" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Kısa İsim</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="slug" class="form-control">
                        <span class="help-block">Lütfen Türkçe karakter ve boşluk kullanmayınız <br>
                            Örnek : Proje İsmi -> <i>proje-ismi</i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Proje Tipi</label>
                    <div class="col-sm-5">
                        <select name="type" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                            <option value=""></option>
                            @foreach($types as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                        @endforeach
                    </div>
                    </select>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Proje Kategorisi</label>
                    <div class="col-sm-5">
                        <select name="category" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                            <option value=""></option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </div>
                    </select>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Mimari Ofis</label>
                    <div class="col-sm-5">
                        <select name="office" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                            <option value=""></option>
                            @foreach($offices as $office)
                                <option value="{{$office->id}}">{{$office->name}}</option>
                        @endforeach
                    </div>
                    </select>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Proje Yatırımcısı</label>
                    <div class="col-sm-5">
                        <select name="investor" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                            <option value=""></option>
                            @foreach($investors as $investor)
                                <option value="{{$investor->id}}">{{$investor->name}}</option>
                        @endforeach
                    </div>
                    </select>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Proje Danışmanları</label>
                    <div class="col-sm-5">
                        <select name="consultants[]" class="select2" multiple data-placeholder="Lütfen Seçiniz..." required="required">
                            <option value=""></option>
                            @foreach($consultants as $consultant)
                                <option value="{{$consultant->id}}">{{$consultant->name}}</option>
                        @endforeach
                    </div>
                    </select>
                </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Resimler</label>
            <div class="col-sm-7 control-label">
                <input type="file" name="image[]" multiple required="required">
                <span class="help-block">Birden fazla resim seçerek galeri oluşturabilirsiniz</span>

            </div>
        </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Proje Hikayesi</label>
                    <div class="col-sm-7 control-label">
                        <textarea class="form-control" rows="5" name="story" required="required"></textarea>
                    </div>
                </div>
        <div class="form-group">
            <label class="col-sm-5 control-label">Proje Yılı</label>
            <div class="col-sm-7 control-label">
                <input type="text" name="year" class="form-control">
            </div>
        </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Proje Ülkesi</label>
                    <div class="col-sm-7 control-label">
                        <input type="text" name="country" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Projede Kullanılan Ürünler</label>
                    <div class="col-sm-5">
                        <select name="products[]" class="select2" multiple data-placeholder="Lütfen Seçiniz..." required="required">
                            <option value=""></option>
                            @foreach($products as $product)
                                <option value="{{$product->id}}">{{$product->name}}</option>
                        @endforeach
                    </div>
                    </select>
                </div>
                <input type="submit" value="Kaydet">
            </form>
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




