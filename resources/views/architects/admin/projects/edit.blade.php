@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="" class="panel-close">&times;</a>
                <a href="" class="minimize">&minus;</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Röportaj Düzenle</h3>
        </div>

        <div class="panel-body">
            <br />
            {!! Form::open(array('action' => array('Admin\ProjectController@update', $project->id), 'method'=> 'patch','files'=> true)) !!}
            <div class="form-group">
                <label class="col-sm-5 control-label">Başlık</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="title" class="form-control" required="required" value="{{$project->name}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Kısa İsim</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="slug" class="form-control" value="{{$project->slug}}">
                    <span class="help-block">Lütfen Türkçe karakter ve boşluk kullanmayınız <br>
                            Örnek : Proje İsmi -> <i>proje-ismi</i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Tipi</label>
                <div class="col-sm-5">
                    <select name="type" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                        <option value=""></option>
                        @foreach($types as $type)
                            <option value="{{$type->id}}" @if($type->id == $project->project_type_id) selected="selected" @endif>{{$type->name}}</option>
                    @endforeach
                </div>
                </select>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Kategorisi</label>
                <div class="col-sm-5">
                    <select name="category" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                        <option value=""></option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" @if($category->id == $project->project_category_id) selected="selected" @endif>{{$category->title}}</option>
                    @endforeach
                </div>
                </select>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Mimari Ofis</label>
                <div class="col-sm-5">
                    <select name="investor" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                        <option value=""></option>

                       @if(count($offices))
                        @foreach($offices as $office)
                            <option value="{{$office->id}}" @if($office->id == $project->company_id) selected="selected" @endif>{{$office->name}}</option>
                    @endforeach
                    @endif()
                </div>
                </select>
            </div>

            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Yatırımcısı</label>
                <div class="col-sm-5">
                    <select name="investor" class="select2" data-placeholder="Lütfen Seçiniz..." required="required">
                        <option value=""></option>
                        @if($investors)
                        @foreach($investors as $investor)
                            <option value="{{$investor->id}}" @if($investor->id == $project->investor_id) selected="selected" @endif>{{$investor->name}}</option>
                    @endforeach
                    @endif()
                </div>
                </select>
            </div>

            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Danışmanları</label>
                <div class="col-sm-5">
                    <select name="investor" class="select2" multiple data-placeholder="Lütfen Seçiniz..." required="required">
                        <option value=""></option>
                        @foreach($consultants as $consultant)
                            @foreach($ProjectConsultants as $item)

                                <option value="{{$consultant->id}}" @if($consultant->id == $item->consultant->id) selected="selected" @endif>{{$consultant->name}}</option>
                            @endforeach
                        @endforeach

                </div>
                </select>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Resimler</label>
                <div class="col-sm-7 control-label">
                    <input type="file" name="image[]" multiple>
                    <span class="help-block">Birden fazla resim seçerek galeri oluşturabilirsiniz</span>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Hikayesi</label>
                <div class="col-sm-7 control-label">
                    <textarea class="form-control" rows="5" name="story" required="required">{{$project->story}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Yılı</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="year" class="form-control" value="{{$project->year}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Proje Ülkesi</label>
                <div class="col-sm-7 control-label">
                    <input type="text" name="country" class="form-control" value="{{$project->country}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Projede Kullanılan Ürünler</label>
                <div class="col-sm-5">
                    <select name="products" class="select2" multiple data-placeholder="Lütfen Seçiniz..." required="required">
                        <option value=""></option>
                        @foreach($products as $product)
                            @foreach($projectProducts as $item)
                                <option value="{{$product->id}}" @if($product->id == $item->product_id) selected="selected" @endif>{{$product->name}}</option>
                            @endforeach
                        @endforeach

                </div>
                </select>
            </div>
            <input type="submit" value="Kaydet">
            {!! Form::close() !!}
        </div><!-- panel-body -->
    </div><!-- panel -->

@endsection




