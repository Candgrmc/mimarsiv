@extends('admin.layouts.app')
<header id="header">
    @include("admin.layouts.sidebar")
    @include("admin.layouts.headerbar")
</header>
@section('content')
    <div class="contentpanel">

        <div class="row">

            <div class="col-sm-6 col-md-3">
                <div class="panel panel-success panel-stat">
                    <div class="panel-heading">

                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-users" aria-hidden="true"></i>

                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label">Toplam Üye Sayısı</small>
                                    <h1>{{$user}}</h1>
                                </div>
                            </div><!-- row -->

                            <div class="mb15"></div>

                            <div class="row">
                                <div class="col-xs-3">
                                    <small class="stat-label">Mimar</small>
                                    <h4>{{$architect}}</h4>
                                </div>

                                <div class="col-xs-3">
                                    <small class="stat-label">Öğrenci</small>
                                    <h4>{{$student}}</h4>
                                </div>
                                <div class="col-xs-6">
                                    <small class="stat-label">Onay Bekleyenler</small>
                                    <h4>{{$approval}}</h4>
                                </div>
                            </div><!-- row -->
                        </div><!-- stat -->

                    </div><!-- panel-heading -->
                </div><!-- panel -->
            </div><!-- col-sm-6 -->

            <div class="col-sm-6 col-md-3">
                <div class="panel panel-danger panel-stat">
                    <div class="panel-heading">

                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-shopping-basket" aria-hidden="true"></i>

                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label">Ürünler</small>
                                    <h1>{{$products}}</h1>
                                </div>
                            </div><!-- row -->

                            <div class="mb15"></div>

                            <small class="stat-label">Ürün Kategori Sayısı</small>
                            <h4>{{$productCategories}}</h4>

                        </div><!-- stat -->

                    </div><!-- panel-heading -->
                </div><!-- panel -->
            </div><!-- col-sm-6 -->

            <div class="col-sm-6 col-md-3">
                <div class="panel panel-primary panel-stat">
                    <div class="panel-heading">

                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-building" aria-hidden="true"></i>

                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label">Projeler</small>
                                    <h1>{{$projects}}</h1>
                                </div>
                            </div><!-- row -->

                            <div class="mb15"></div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">Proje Kategorileri</small>
                                    <h4>{{$projectCategories}}</h4>
                                </div>

                                <div class="col-xs-6">
                                    <small class="stat-label">Proje Tipleri</small>
                                    <h4>{{$projectTypes}}</h4>
                                </div>
                            </div>

                        </div><!-- stat -->

                    </div><!-- panel-heading -->
                </div><!-- panel -->
            </div><!-- col-sm-6 -->

            <div class="col-sm-6 col-md-3">
                <div class="panel panel-dark panel-stat">
                    <div class="panel-heading">

                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>

                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label">Haberler</small>
                                    <h1>{{$news}}</h1>
                                </div>
                            </div><!-- row -->

                            <div class="mb15"></div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">Makaleler</small>
                                    <h4>{{$articles}}</h4>
                                </div>

                                <div class="col-xs-6">
                                    <small class="stat-label">Röportajlar</small>
                                    <h4>{{$interviews}}</h4>
                                </div>
                            </div><!-- row -->

                        </div><!-- stat -->

                    </div><!-- panel-heading -->
                </div><!-- panel -->
            </div><!-- col-sm-6 -->
        </div><!-- row -->
        <div class="row">

            <div class="col-sm-6 col-md-6">
                <div class="panel panel-success panel-stat">
                    <div class="panel-heading">

                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i>


                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label">Sliderler</small>
                                    <h1>{{$sliders}}</h1>
                                </div>
                            </div><!-- row -->

                            <div class="mb15"></div>


                        </div><!-- stat -->

                    </div><!-- panel-heading -->
                </div><!-- panel -->
            </div><!-- col-sm-6 -->

            <div class="col-sm-6 col-md-3">
                <div class="panel panel-danger panel-stat">
                    <div class="panel-heading">

                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-suitcase" aria-hidden="true"></i>


                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label">Markalar</small>
                                    <h1>{{$brands}}</h1>
                                </div>
                            </div><!-- row -->

                            <div class="mb15"></div>



                        </div><!-- stat -->

                    </div><!-- panel-heading -->
                </div><!-- panel -->
            </div><!-- col-sm-6 -->

            <div class="col-sm-6 col-md-3">
                <div class="panel panel-danger panel-stat">
                    <div class="panel-heading">

                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-suitcase" aria-hidden="true"></i>


                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label">Partnerler</small>
                                    <h1>{{$partners}}</h1>
                                </div>
                            </div><!-- row -->

                            <div class="mb15"></div>



                        </div><!-- stat -->

                    </div><!-- panel-heading -->
                </div><!-- panel -->
            </div><!-- col-sm-6 -->

        </div><!-- row -->


    </div><!-- contentpanel -->
@endsection




