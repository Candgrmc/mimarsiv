@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="top-normal-banner">
                <div class="icon-box">
                    <img src="{{$company->logo}}" height="47" width="94" alt="{{$company->name}}">
                </div>
                <img src="{{$company->banner}}" height="960" width="1920" alt="{{$company->name}}">
            </div>
            <div class="buttons-list">
                <a href="/company" class="button-green"><span>MIMARSIVIM</span></a>
                <a href="#" class="button-green"><span>BILGILERIM</span></a>
                <a href="{{ route('logout') }}" class="button-green" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">ÇIKIŞ YAP</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </div>

            <ul class="default-accordion">
                <form action="{{ URL::to('/product/store') }}" method="post" enctype="multipart/form-data" class="simple-form">
                <li class="active">
                    <a href="#" class="opener">GENEL BİLGİLER</a>
                    <div class="slide-box">

                            <div class="form-group size3">
                                <div class="select-input">
                                    <select class="large straight" name="category">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                            @endforeach

                                    </select>
                                </div>
                            </div>
                            <br>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group size2">
                                <label for="form-element01">ÜRÜN ADI :</label>
                                <input type="text" id="form-element01" name="title">
                            </div>

                            <div class="form-group size5">
                                <label for="form-element02">ÜRÜN AÇIKLAMASI :</label>
                                <textarea class="form-control" rows="5" name="overview"></textarea>
                            </div>

                            <div class="form-group">
                                <label>MARKA :</label>
                                <br>
                                <select class="large straight" name="brand">
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group">
                                <label>VİDEO :</label>
                                <br>
                                <input type="text" id="form-element01" name="video">
                            </div>
                            <div class="form-group">
                                <label>KATALOG :</label>
                                <br>
                                <input type="text" id="form-element01" name="catalogue">

                            </div>
                        <div class="form-group">
                            <label>RENK :</label>
                            <br>
                            <input type="text" id="form-element01" name="color">

                        </div>

                    </div>
                </li>
                <li>
                    <a href="#" class="opener">BİM</a>
                    <div class="slide-box">
                        <textarea class="form-control" rows="5" name="bim"></textarea>
                    </div>
                </li>
                <li>
                    <a href="#" class="opener">Resimler</a>
                    <div class="slide-box">
                        <input type="file" name="image[]" multiple="multiple" required="required">
                        <span class="help-block">Birden fazla resim seçip ürün galerisi oluşturabilirsiniz</span>
                         </div>
                </li>
               {{-- <li>
                    <a href="#" class="opener">Sertifikalar</a>
                    <div class="slide-box">
                        <select multiple="multiple" name="certificates[]" id="certs" class="">
                        <div class="col-sm-5">
                            @foreach($certificates as $certificate)
                                <option value="{{$certificate->id}}">{{$certificate->name}}</option>
                            @endforeach
                        </div>
                        </select>
                         </div>
                </li>--}}

                <li>
                    <a href="#" class="opener">FİYAT ARALIĞI</a>
                    <div class="slide-box">
                        <input type="text" id="form-element01" name="price">
                    </div>
                </li>
                <br>
                <input type="submit" class="button-plain large" value="Gönder">
                </form>
            </ul>
        </div>
</div>
@endsection