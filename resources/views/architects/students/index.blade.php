@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="top-normal-banner">
                <div class="icon-box">
                    <img src="{{request()->user()->image}}" height="47" width="94" alt="">
                </div>
                <img src="{{asset('images/img-banner04.png')}}" height="960" width="1920" alt="">
            </div>
            <div class="buttons-list">
                <a href="/company" class="button-green"><span>MIMARSIVIM</span></a>
                <a href="#" class="button-green"><span>BILGILERIM</span></a>
                <a href="{{ route('logout') }}" class="button-green" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">ÇIKIŞ YAP</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </div>

            <div class="image-gallery-holder" >
                <h2 class="gallery-title">SEÇTİĞİM ÜRÜNLER</h2>
                <div class="image-gallery one-row-slider" >

                    @foreach(request()->user()->selectedProducts as $item)

                        <div class="gallery-item" style="height: 300px;">

                            <a href="/product/{{$item->slug}}" class="item" style="height: 155px!important; ">
                                <img src="{{$item->image}}" alt="image" class="img">
                                <span class="text">{{$item->name}}</span>
                            </a>
                            <textarea cols="30" rows="3" class="" placeholder="NOT YAZ:" style="margin-top:8px">{{$item->pivot->note}}</textarea>


                        </div>
                    @endforeach
                </div>



            </div>

            <div class="image-gallery-holder" >
                <h2 class="gallery-title">BEĞENDİĞİM PROJELER</h2>
                <div class="image-gallery one-row-slider" >

                    @foreach(request()->user()->likedProjects as $item)

                        <div class="gallery-item" style="height: 300px;" >

                            <a href="/product/{{$item->slug}}" class="item" style="height: 155px; ">
                                <img src="{{$item->image}}" alt="image" class="img">
                                <span class="text">{{$item->name}}</span>
                            </a>



                        </div>
                    @endforeach
                </div>



            </div>
            <div class="image-gallery-holder" >
                <h2 class="gallery-title">KATILDIĞIN ETKİNLİKLER</h2>
                <div class="image-gallery one-row-slider" >

                    @foreach(request()->user()->likedProjects as $item)

                        <div class="gallery-item" style="height: 300px;">

                            <a href="/product/{{$item->slug}}" class="item" style="height: 155px!important; ">
                                <img src="{{$item->image}}" alt="image" class="img">
                                <span class="text">{{$item->name}}</span>
                            </a>



                        </div>
                    @endforeach
                </div>



            </div>

            <div class="buttons-list">
                <a href="/company" class="button-green"><span>Ürün Öner</span></a>
                <a href="#" class="button-green"><span>Tasarımcı Öner</span></a>
                <a href="#" class="button-green"><span>Danışman Öner</span></a>
                <a href="http://mimarsiv.dev/logout" class="button-green" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">ÇIKIŞ YAP</a>

                <form id="logout-form" action="http://mimarsiv.dev/logout" method="POST" style="display: none;"><input type="hidden" name="_token" value="c8MwCOzvQD5JQV5YBJSuzZuzZRRbt6P6tAyghhNx"></form>
            </div>
        </div>
</div>
@endsection