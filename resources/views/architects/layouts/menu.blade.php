<div id="signinModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<ul class="nav nav-pills nav-justified">
					<li class="active">
						<a data-toggle="pill" href="#loginTab">{{trans('main.login')}}</a>
					</li>
					<li>
						<a data-toggle="pill" href="#registerTab">{{trans('main.register')}}</a>
					</li>
				</ul>
				<div class="tab-content">
					<div id="loginTab" class="tab-pane fade in active">
						<form method="POST" action="{{ route('login') }}">
							{{ csrf_field() }}
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label for="email">{{trans('main.e_mail_address')}}</label>
								<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus> @if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label for="password">{{trans('main.password')}}</label>
								<input id="password" type="password" class="form-control" name="password" required> @if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
								@endif
							</div>


							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember" {{ old( 'remember') ? 'checked' : '' }}> {{trans('main.remember_me')}}
								</label>
							</div>

							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary">
										{{trans('main.login')}}
									</button>

									<a class="btn btn-link pull-right" href="{{ route('password.request') }}">
										{{trans('main.lost_password')}}
									</a>
								</div>
							</div>
						</form>
					</div>
					<div id="registerTab" class="tab-pane fade">
						<form method="POST" action="{{ route('register') }}">
							{{ csrf_field() }}

							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name">{{trans('main.name_surname')}}</label>
								<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus> @if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group">
								<label for="account_type">{{trans('main.account_type')}}:</label>
								<select class="form-control" name="account_type">
									<option value="student" name="student">{{trans('main.student')}}</option>
									<option value="architect" name="architect">{{trans('main.architect')}}</option>
								</select>
							</div>

							<div class="form-group">
								<label for="account_type_value">{{trans('main.school_name')}}</label>
								<input type="text" class="form-control" name="account_type_value">
							</div>

							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label for="email">{{trans('main.e_mail_address')}}</label>
								<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required> @if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label for="password">{{trans('main.password')}}</label>
								<input id="password" type="password" class="form-control" name="password" required> @if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group">
								<label for="password-confirm">{{trans('main.re_password')}}</label>
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
							</div>

							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary">
										{{trans('main.register')}}
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	if (document.querySelector("#account_type").value == "student") {
		document.querySelector('label[for="account_type_value"]').innerHTML = "{{trans('main.school_name')}}"
	} else if (document.querySelector("#account_type") == "architect") {
		document.querySelector('label[for="account_type_value"]').innerHTML = "{{trans('main.company_name')}}"
	}
	document.querySelector("#account_type").addEventListener("change", function (e) {
		if (e.currentTarget.value == "student") {
			document.querySelector('label[for="account_type_value"]').innerHTML = "{{trans('main.school_name')}}"
		} else if (e.currentTarget.value == "architect") {
			document.querySelector('label[for="account_type_value"]').innerHTML = "{{trans('main.company_name')}}"
		}
	})
</script>