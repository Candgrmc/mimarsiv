
@extends('layouts.app')
<div id="wrapper" class="common-page roportaj-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-md-4">
                    @if(count($interview->user) > 1 || count($interview->user->company) > 1)
                    <div class="sidebar-box margin-bottom-20">
                        <div class="icon-box">
                            <img src="{{$interview->user->company->logo}}" alt="client" width="122" height="46">
                        </div>
                        <p>{{$interview->user->company->name}}</p>
                        <address>
                            {{$interview->user->company->phone}} <br>
                            {{$interview->user->company->address}}
                            Web : {{$interview->user->company->url}}
                        </address>
                    </div>
                    @endif
                        @if(count($interview->user) > 1 || count($interview->user->company) > 1)

                        <div class="sidebar-box">
                        <h2 class="sidebar-title">REFERENCE PROJECTS</h2>
                        <div class="image-gallery gallery-type-5 padding-bottom-15">
                            @foreach($interview->user->company->projects as $project)
                            <div class="gallery-item" style="width: 188px; height: 141px;">
                                <a href="/project_detail/{{$project->slug}}" class="item">
                                    <img src="{{$project->image}}" alt="image" width="188" height="141">
                                    <span class="head">{{$project->name}}</span>
                                    @if($project->company)
                                    <span class="text">
                                    {{$project->company->name}} <br>
                                        {{$project->category->title}} <br>
                                </span>
                                        @endif()
                                </a>
                            </div>
                                @endforeach
                        </div>
                    </div>
                            @endif()
                </div>
                <div class="col-sm-7 col-md-8">
                    <div class="top-normal-banner margin-bottom-5">
                        <img src="{{$interview->image}}" alt="image banner" width="770" height="399">
                    </div>
                    <article class="article padding-bottom-30">
                     {!! $interview->content !!}
                    </article>
                    <h3 class="article-h3">DİĞER RÖPORTAJLAR</h3>

                    <div class="image-gallery gallery-type-2">
                        @foreach($interviews as $item)
                        <div class="gallery-item">
                            <a href="/interviews/{{$item->slug}}" class="item">
                                <img src="images/img01.jpg" alt="image" width="188" height="141">
                                <span class="head">{{$item->title}}</span>
                                <span class="text">

                                </span>
                            </a>
                        </div>
                       @endforeach
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection