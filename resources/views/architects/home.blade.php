@extends('layouts.app')
<header id="header">
    @include("layouts.top")
    @include("layouts.menu")
</header>
@section('content')
    <main id="main">
        <div class="main-banner">
            <div class="banner-carousel">
                <div class="mask">
                    <div class="slideset">
                        @foreach($sliders as $slider)
                        <div class="slide">
                            <a href="@if($slider->redirectTo){{$slider->redirectTo}}@else javascript:void(0) @endif"><img src="{{asset($slider->image)}}" alt="banner image"></a>
                        </div>
                        @endforeach

                    </div>
                </div>
                <div class="pagination"></div>
            </div>
        </div>

        <div class="showcase-block">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.new_arrivals')}}</h1>
                <div class="row">
                    @foreach($products as $product)
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <a href="product/{{$product->slug}}" class="img-block" style="background-size: cover; width: 378px; height: 264px; margin: 10px;">
                            <img src="{{asset($product->image)}}" alt="{{$product->name}}">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="title">{{$product->name}}</span>
                                    <span class="text">

                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>

        <div class="showcase-block green">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.latest_news')}}</h1>
                <div class="row">
                    @foreach($news as $new)
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <a href="news/{{$new->slug}}" class="img-block" style="background-size: cover;  width: 378px; height: 264px; margin: 10px">
                            <img src="{{asset($new->image)}}" alt="{{$new->title}}">
                        </a>
                    </div>
                    @endforeach()

                </div>
            </div>
        </div>

        <div class="showcase-block alternate">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.interview')}}</h1>
                <div class="row">
                    @foreach($interviews as $interview)
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <a href="interviews/{{$interview->slug}}" class="img-block" style="background-size: cover;  width: 378px; height: 264px; margin: 10px">
                            <img src="{{asset($interview->image)}}" alt="image">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="title">{{$interview->title}}</span>
                                    <span class="text">
                                        {{$interview->meta_description}}
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                            @endforeach

                </div>
            </div>
        </div>

        <div class="showcase-block green">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.mimworld')}}</h1>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <a href="#" class="img-block">
                            <img src="images/img22.jpg" alt="image" width="518" height="346">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="title">MIM SHOTS</span>
                                    <span class="text">
                                        <i class="fa fa-fw fa-video-camera fa-3x"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <a href="#" class="img-block">
                            <img src="images/img23.jpg" alt="image" width="518" height="346">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="title">{{trans('main.mim_insta')}}</span>
                                    <span class="text">
                                        <i class="fa fa-fw fa-heart fa-3x"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        @foreach($bulletins as $bulletin)
                            <a href="bulletin/{{$bulletin->slug}}" class="img-block" style="background-size: cover;">
                                <img src="{{$bulletin->image}}" alt="image" width="518" height="346">
                                <div class="overlay">
                                    <div class="inner">
                                        <span class="title">{{$bulletin->name}}</span>
                                        <span class="text">
                                        <i class="fa fa-fw fa-address-book fa-flip-horizontal fa-3x"></i>
                                    </span>
                                    </div>
                                </div>
                            </a>
                        @endforeach

                    </div>
                    <div class="col-xs-12 col-md-6">
                    @foreach($projects as $project)
                        <a href="project_detail/{{$project->slug}}" class="img-block" style="background-size: cover;">
                            <img src="{{$project->image}}" alt="image" width="518" height="346">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="title">{{$project->name}}</span>
                                    <span class="text">
                                        <i class="fa fa-fw fa-address-book fa-flip-horizontal fa-3x"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="share-block">
            <div class="container">
                <h2>#share</h2>
                <h3>{{trans('main.this_month_concept')}}<strong>library</strong></h3>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <a href="#" class="img-block">
                            <img src="images/img27.jpg" alt="image" width="254" height="169">
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <a href="#" class="img-block">
                            <img src="images/img28.jpg" alt="image" width="252" height="168">
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <a href="#" class="img-block">
                            <img src="images/img29.jpg" alt="image" width="252" height="169">
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <a href="#" class="img-block">
                            <img src="images/img30.jpg" alt="image" width="253" height="169">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="showcase-block alternate green">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.consultants')}}</h1>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <a href="#" class="img-block">
                            <img src="images/img26.jpg" alt="image" width="458" height="306">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="text">
                                        <i class="fa fa-fw fa-address-book fa-flip-horizontal fa-3x"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <article class="article">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</strong> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. <span class="red">It was popularised in the 1960s with the release of Letraset sheets.</span></p>
                        </article>
                    </div>
                </div>
            </div>
        </div>

        <div class="showcase-block">
            <div class="container">
                <h1 class="showcase-title red"><i class="fa fa-bullhorn fa-fw"></i> {{trans('main.art_speaks')}}</h1>
                <div class="row">
                    <div class="col-xs-12 wide">
                        <a href="#" class="img-block">
                            <img src="images/img31.jpg" alt="image" width="1092" height="404">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="text">
                                        <i class="fa fa-fw fa-thumbs-up fa-3x"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="showcase-block alternate green">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.trends')}}</h1>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <a href="#" class="img-block">
                            <img src="images/img32.jpg" alt="image" width="513" height="346">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="title">ARCHITECT EYE</span>
                                    <span class="text">
                                        <i class="fa fa-fw fa-eye fa-3x"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <a href="#" class="img-block">
                            <img src="images/img33.jpg" alt="image" width="518" height="341">
                            <div class="overlay">
                                <div class="inner">
                                    <span class="title">MATERIAL EYE</span>
                                    <span class="text">
                                        <i class="fa fa-fw fa-camera-retro fa-3x"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="showcase-block bordered">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.partners')}}</h1>
                <ul class="partners-list">
                    @foreach($partners as $partner)
                        <li><img src="{{$partner->image}}" alt="brand 01" width="129" height="47"></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="showcase-block">
            <div class="container">
                <h1 class="showcase-title">{{trans('main.brands')}}</h1>
                <ul class="brands-list">
                    @foreach($brands as $brand)
                        <li><img src="{{$brand->image}}" alt="brand 01" width="129" height="47"></li>
                    @endforeach

                </ul>
            </div>
        </div>
    </main>
@endsection
