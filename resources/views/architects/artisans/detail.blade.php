@extends('layouts.app')
<div id="wrapper" class="common-page proje-sayfasi"><br>
<header id="header">
    @include("layouts.top")
    @include("layouts.menu")
</header>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-5 col-md-4">
                <div class="sidebar-box margin-bottom-30">
                    <div class="icon-box width-auto">
                        <img src="{{$user->image}}" alt="client" width="145" height="142">
                    </div>
                    <h2 class="padding-top-20 padding-bottom-30">{{$user->name}}</h2>
                    <form action="#" class="find-retailers">
                        <div class="input-field-wrapper">
                            <div class="input-field">
                                <input type="text" placeholder="REQUEST PRICE / QUOTE">
                                <span class="icon-hold icon-tag"></span>
                            </div>
                            <div class="input-field">
                                <input type="text" placeholder="REQUEST CATALOGOUES">
                                <span class="icon-hold icon-upload"></span>
                            </div>
                            <div class="input-field">
                                <input type="text" placeholder="REQUEST SAMPLE">
                                <span class="icon-hold icon-bin"></span>
                            </div>
                            <div class="input-field">
                                <input type="text" placeholder="REQUEST VİSİT">
                                <span class="icon-hold icon-run"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="sidebar-box">
                    <h2>find retailers</h2>
                    <form action="#" class="find-retailers">
                        <div class="select-inputs-wrap">
                            <div class="select-input">
                                <select class="large">
                                    <option>COUNTRY</option>
                                    <option>NEPAL</option>
                                    <option>USA</option>
                                    <option>CHINA</option>
                                </select>
                            </div>
                            <div class="select-input">
                                <select class="large">
                                    <option>STATE</option>
                                    <option>STATE 1</option>
                                    <option>STATE 2</option>
                                    <option>STATE 3</option>
                                </select>
                            </div>
                            <div class="select-input inline">
                                <select class="large">
                                    <option>CITY</option>
                                    <option>CITY 1</option>
                                    <option>CITY 2</option>
                                    <option>CITY 3</option>
                                </select>
                            </div>
                            <div class="select-input inline button-wrap">
                                <button type="submit"><span>search</span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-7 col-md-8">

@if($gallery)
                <div class="linked-gallery-head">
                    @foreach($gallery as $item)
                    <div class="image-item">
                        <img src="{{$item->image}}" width="100%" height="100%" alt="gallery-item">
                    </div>
                    @endforeach()
                </div>

                <div class="linked-gallery-pager">
                    @foreach($gallery as $item)
                    <div class="pager-item">
                        <img src="{{$item->image}}" height="93" width="114" alt="pager item">
                    </div>
                    @endforeach()
                </div>
@endif()

                <article class="article padding-bottom-20">{!! $user->bio!!}</article>
                <h3 class="article-h3">Reference Projects</h3>

    @if($artisanProjects)
                <div class="image-gallery gallery-type-2 padding-bottom-15">
                    <div class="gallery-item">
                        @foreach($artisanProjects as $item2)
                        <a href="#" class="item" style="height: 141px;">
                            <img src="{{$item2->product->image}}" alt="image" width="188" height="141">
                            <span class="head">{{$item2->product->name}}</span>
                            <span class="text">
                                    {{$item2->product->brand}} <br>
                                </span>
                        </a>
                        @endforeach()
                    </div>
                </div>
        @endif()
            </div>
        </div>
    </div>
    </div>
@endsection