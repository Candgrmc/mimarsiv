
@extends('layouts.app')
<div id="wrapper" class="common-page haber-detay-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="row main-row">
                <div class="col-sm-4">
                    <div class="sidebar-box">
                        @if(count($bulletin->user) > 1 || count($bulletin->user->company) > 1)
                            <div class="icon-box">
                                <img src="{{$bulletin->user->company->logo}}" alt="client" width="122" height="46">
                            </div>
                            <p>{{$bulletin->user->company->name}}</p>
                            <address>
                                {{$bulletin->user->company->phone}} <br>
                                {{$bulletin->user->company->address}}
                                Web : {{$bulletin->user->company->url}}
                            </address>
                        @endif()
                    </div>
                </div>
                <div class="col-sm-8" style="width: 787px; height: 343px; ">
                    <div style="width: 787px; height: 343px; background-size: cover;">
                    <img src="{{$bulletin->image}}" height="343" width="787" alt="banner" >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <article class="article">
                    {!! $bulletin->content !!}
                    </article>
                    <h3 class="article-h3 padding-bottom-5">DİĞER RÖPORTAJLAR</h3>
                    <div class="image-gallery gallery-type-3">
                        @foreach($bulletins as $item)
                        <div class="gallery-item">
                            <a href="/bulletin/{{$item->slug}}" class="item">
                                <img src="{{$item->image}}" alt="image" width="188" height="141">
                                <span class="head">{{$item->title}}</span>

                            </a>
                        </div>
@endforeach
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection