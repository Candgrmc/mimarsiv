@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
@section('content')
        <div class="container">
            @include('layouts.profileTop')
            @include('architects.buttons')
            <form action="{{route('student.update')}}" method="post" >
                {{csrf_field()}}
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" value="{{request()->user()->email}}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="name">İsim</label>
                        <input type="text" class="form-control" name="name" value="{{request()->user()->name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="university">{{trans('main.company_name')}}</label>
                        <input type="company_name" class="form-control" value="@isset($architect) {{$architect->company_name}} @endisset" disabled>
                    </div>
                    <div class="form-group">
                        <label for="department">{{trans('main.company_phone')}}</label>
                        <input type="company_phone" class="form-control" value="@isset($architect) {{$architect->company_phone}} @endisset" disabled>
                    </div>
                    <div class="form-group">
                        <label for="phone">Telefon</label>
                        <input type="phone" class="form-control" name="phone" value="{{request()->user()->phone}}">
                    </div>
                    <div class="form-group">
                        <label for="password">Şifre</label>
                        <input type="password" name="password" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="password-repeat">Şifre Tekrar</label>
                        <input type="password" name="password-repeat" class="form-control" >
                    </div>

                    <button type="submit" class="button-green" style="margin-bottom:20px">Güncelle</button>
                </div>


            </form>
        </div>
</div>
@endsection
