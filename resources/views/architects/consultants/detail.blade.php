
@extends('layouts.app')
<div id="wrapper" class="common-page danishman-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')

        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="sidebar-box">
                        <div class="icon-box">
                            <img src="{{$consultant->company->logo}}" alt="client" width="130" height="87">
                        </div>

                        <p>{{$consultant->company->name}}</p>
                        <address>
                            Telefon :{{$consultant->company->phone}} <br>
                            Adres :{{$consultant->company->address}} <br>
                            Web :{{$consultant->company->url}}
                        </address>

                    </div>

                   {{-- <div class="sidebar-box">
                        <h2>find retailers</h2>
                        <form action="#" class="find-retailers">
                            <div class="select-inputs-wrap">
                                <div class="select-input">
                                    <select class="large">
                                        <option>COUNTRY</option>
                                        <option>NEPAL</option>
                                        <option>USA</option>
                                        <option>CHINA</option>
                                    </select>
                                </div>
                                <div class="select-input">
                                    <select class="large">
                                        <option>STATE</option>
                                        <option>STATE 1</option>
                                        <option>STATE 2</option>
                                        <option>STATE 3</option>
                                    </select>
                                </div>
                                <div class="select-input inline">
                                    <select class="large">
                                        <option>CITY</option>
                                        <option>CITY 1</option>
                                        <option>CITY 2</option>
                                        <option>CITY 3</option>
                                    </select>
                                </div>
                                <div class="select-input inline button-wrap">
                                    <button type="submit"><span>search</span></button>
                                </div>
                            </div>
                        </form>
                    </div>--}}
                </div>
                <div class="col-sm-8">
                    <article class="article">
                        <p>ZKLD Aydınlatma Tasarımı Stüdyosu, kurucusu Zeki Kadirbeyoğlu’nun sektördeki 28 yıllık tecrübesiyle İstanbul’da yer alan ve bağımsız aydınlatma tasarımı yapan bir firmadır. Stüdyo; ofis, alışveriş merkezleri, mağaza, otel, müze, cephe, peyzaj, konut ve restoran projelerine mimari aydınlatma tasarımı, ürün tasarımı ve danışmanlık hizmeti vermektedir.</p>
                        <p>Stüdyo’nun birçok farklı meslek grubundan gelen genç bir ekibi vardır. Mimar, iç mimar, peyzaj mimari, ürün tasarımcısı, şehir bölge planlamacısı ve elektrik mühendisi olan ekip üyelerinin, aydınlatma tasarımı alanında yüksek lisansları ve/veya iş tecrübeleri bulunmaktadır.</p>
                        <p>Farklı disiplinlerin bir araya gelmesiyle oluşan yaratıcı bir ortama sahip olan ZKLD, projenin tasarım bütünlüğü kapsamında işverene ve mimara karşı sorumluluklarını Zeki Kadirbeyoğlu’nun profesyonel üyesi olduğu IALD (International Association of Lighting Designers) etik kurallarına uygun olarak gerçekleştirmekle yükümlüdür. Ayrıca, projenin gereksinimine göre, Leed ve Breeam gibi yeşil bina sertifikasyon sistemlerinin getirdiği yükümlülüklere uygun şekilde çalışmalarını yürütmektedir.</p>
                    </article>
                    <h3 class="article-h3 padding-bottom-5">REFERENCE PROJECTS</h3>
                    <div class="image-gallery gallery-type-2">
                        @foreach($consultant->projects as $project)
                            <div class="gallery-item" style="width: 248px; height: 188px;">
                                <a href="project_detail/{{$project->project->slug}}" class="item">
                                    <img src="{{$project->project->image}}" alt="image" width="248" height="188">
                                    <span class="head">{{$project->project->name}}</span>
                                    <span class="text">
                                    {{$project->project->company->name}} <br>
                                        {{$project->project->category->title}} <br>
                                </span>
                                </a>
                            </div>
                            @endforeach
                    </div>
                    <h3 class="article-h3 padding-bottom-5">DANIŞMAN YAYINLARI</h3>
                    <ul class="linked-list">
                        @foreach($consultant->press as $press)
                                <a href="/press/{{$press->slug}}"></a><li>{{$press->title}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
</div>
@endsection