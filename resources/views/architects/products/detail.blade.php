
@extends('layouts.app')
<div id="wrapper" class="common-page proje-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-md-4">
                    @if($company)
                    <div class="sidebar-box margin-bottom-30">
                        <div class="icon-box">
                            <img src="{{$company->logo}}" alt="client" width="94" height="47">
                        </div>
                        <p>{{$company->name}}</p>
                        <address>
                            {{$company->address}} <br>
                            {{$company->phone}}<br>
                            {{$company->url}}
                        </address>
                    </div>
                    @endif()
                    <div class="sidebar-box">
                        <form action="#" class="find-retailers">
                            <div class="input-field-wrapper">
                                <div class="input-field">
                                    <input type="text" placeholder="REQUEST PRICE / QUOTE">
                                    <span class="icon-hold icon-tag"></span>
                                </div>
                                <div class="input-field">
                                    <input type="text" placeholder="REQUEST CATALOGOUES">
                                    <span class="icon-hold icon-upload"></span>
                                </div>
                                <div class="input-field">
                                    <input type="text" placeholder="REQUEST SAMPLE">
                                    <span class="icon-hold icon-bin"></span>
                                </div>
                                <div class="input-field">
                                    <input type="text" placeholder="REQUEST VİSİT">
                                    <span class="icon-hold icon-run"></span>
                                </div>
                            </div>
                        </form>
                    </div>

                        @if($company)
                    <div class="sidebar-box">
                        <span class="title">OTHER PRODUCTS OF THE COMPANY</span>
                        <ul class="list-items">
                        @php $products = \App\Http\Controllers\ProductsController::getCompanyProducts($company->id) @endphp
                        @foreach($products as $product)
                                <li><a href="/product/{{$product->slug}}">{{$product->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                            @endif()
                </div>
                <div class="col-sm-7 col-md-8">
                    <h1 class="top-title">{{$product->name}}</h1>
                    <div class="linked-gallery-head">
                        @foreach($images as $image)
                            <div class="image-item">
                                <img src="{{$image->image}}" height="388" width="477" alt="gallery-item">
                            </div>
                            @endforeach


                    </div>

                    <div class="linked-gallery-pager">
                        @foreach($images as $image)
                        <div class="pager-item">
                            <img src="{{$image->image}}" height="93" width="114" alt="pager item">
                        </div>
                        @endforeach
                    </div>

                    <ul class="tab-list">
                        <li class="active"><a href="#tab-item-01" data-toggle="tab">Overview</a></li>
                        <li><a href="#tab-item-02" data-toggle="tab">Price</a></li>
                        <li><a href="#tab-item-03" data-toggle="tab">Bim</a></li>
                        <li><a href="#tab-item-04" data-toggle="tab">Spec</a></li>
                        <li><a href="#tab-item-05" data-toggle="tab">Certificate</a></li>
                        <li><a href="#tab-item-06" data-toggle="tab">Catalog</a></li>
                        <li><a href="#tab-item-07" data-toggle="tab">Video</a></li>
                        <li><a href="#tab-item-08" data-toggle="tab">Events</a></li>
                    </ul>
                    <div class="tab-content">
                        <article id="tab-item-01" class="tab-pane active article padding-bottom-15">
                            {!! $product->overview !!}
                        </article>
                        <article id="tab-item-02" class="tab-pane article">
                            {{$product->price}}
                        </article>
                        <article id="tab-item-03" class="tab-pane article">
                           {!! $product->bim !!}
                            </article>
                        <article id="tab-item-04" class="tab-pane article">

                           </article>
                        <article id="tab-item-05" class="tab-pane article">

                           </article>
                        <article id="tab-item-06" class="tab-pane article">
                            <iframe id="ytplayer" type="text/html" width="640" height="360"
                                    src="http://www.youtube.com/embed/{{$product->video}}?autoplay=0"
                                    frameborder="0"/>
                        </article>
                        <article id="tab-item-07" class="tab-pane article">
                            <object data="{{$product->catalogue}}" type="application/pdf">
                                <iframe src="https://docs.google.com/viewer?url={{$product->catalogue}}&embedded=true"></iframe>
                            </object>
                         </article>
                        <article id="tab-item-08" class="tab-pane article">

                         </article>
                    </div>

                    <h3 class="article-h3">REFERENCE PROJECTS</h3>
                    <div class="image-gallery gallery-type-2 padding-bottom-15">

                        @foreach($projects as $project)
                            <div class="gallery-item" style="width: 248px; height: 188px;">
                                <a href="/project_detail/{{$project->project->slug}}" class="item">
                                    <img src="{{$project->project->image}}" alt="image" width="248" height="188">
                                    <span class="head">{{$project->project->name}}</span>
                                    <span class="text">
                                        {{$project->project->company->name}} <br>
                                        {{$project->project->category->title}} <br>
                                </span>
                                </a>
                            </div>
                            @endforeach()

                    </div>
                </div>
            </div>
        </div>
</div>
@endsection