
@extends('layouts.app')
<div id="wrapper" class="common-page proje-listeleme">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="breadcrumb-area">
                        <ol class="breadcrumb">
                            <li><a href="/">ANASAYFA</a></li>
                            <li class="active">ÜRÜNLER</li>
                        </ol>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="search-filter-area">
                        <div class="select-inputs-wrap">
                            <div class="select-input size1">
                                <select>
                                    <option>MARKA</option>
                                    <option>MARKA 1</option>
                                    <option>MARKA 2</option>
                                    <option>MARKA 3</option>
                                </select>
                            </div>
                            <div class="select-input size2">
                                <select>
                                    <option>ÜLKE</option>
                                    <option>ÜLKE 1</option>
                                    <option>ÜLKE 2</option>
                                    <option>ÜLKE 3</option>
                                </select>
                            </div>
                            <div class="select-input size1">
                                <select>
                                    <option>SERTİFİKA</option>
                                    <option>SERTİFİKA 1</option>
                                    <option>SERTİFİKA 2</option>
                                    <option>SERTİFİKA 3</option>
                                </select>
                            </div>
                            <br>
                            <div class="select-input size2">
                                <select>
                                    <option>PROJE</option>
                                    <option>PROJE 1</option>
                                    <option>PROJE 2</option>
                                    <option>PROJE 3</option>
                                </select>
                            </div>
                            <div class="select-input size2">
                                <select>
                                    <option>FİYAT</option>
                                    <option>FİYAT 1</option>
                                    <option>FİYAT 2</option>
                                    <option>FİYAT 3</option>
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="search-result-info">
            <div class="container">
                <div class="results-info">
                    <p>Results: {{count($products)}}</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <h1 class="sidebar-title">KATEGORİLER</h1>
                    <ul class="sidebar-nav text-capitalise">
                        @foreach($categories as $category)
                            @if($category->category_id == null)
                                <li>
                                    <a href="#">{{$category->title}}</a>
                                    <ul>
                                        @foreach($categories as $subcategory)
                                            @if($subcategory->category_id == $category->id)
                                            <li><a href="#">{{$subcategory->title}}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @endif

                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="image-gallery gallery-type-4">
                        @foreach($products as $product)
                        <div class="gallery-item" style="width: 248px; height: 188px;">
                            <a href="/product/{{$product->slug}}" class="item">
                                <img src="{{$product->image}}" alt="image" width="188" height="141">
                                <span class="head">{{$product->name}}</span>
                                <span class="text">
                                    @if($product->user->company)
                                  {{$product->user->company->name}} <br>
                                    @endif()
                                   {{$product->category->title}} <br>
@if($product->brand)
                                   {{$product->brand->name}}
    @endif()

                                </span>
                            </a>
                        </div>
                            @endforeach()
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection