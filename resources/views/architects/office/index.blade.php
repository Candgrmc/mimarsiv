@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="top-normal-banner">
                <div class="icon-box">
                    <img src="{{$company->logo}}" height="47" width="94" alt="{{$company->name}}">
                </div>
                <img src="{{$company->banner}}" height="960" width="1920" alt="{{$company->name}}">
            </div>
            <div class="buttons-list">
                <a href="/office" class="button-green"><span>MIMARSIVIM</span></a>
                <a href="#" class="button-green"><span>BILGILERIM</span></a>
                <a href="{{ route('logout') }}" class="button-green" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">ÇIKIŞ YAP</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </div>

            <div class="image-gallery-holder">
                <h2 class="gallery-title">ÜRÜNLER</h2>
                <div class="image-gallery one-row-slider">
                    @foreach($user->products as $item)
                        <div class="gallery-item" style="height: 141px;">
                            <a href="/product/{{$item->slug}}" class="item">
                                <img src="{{$item->image}}" alt="image" width="188" height="141">
                                <span class="text">{{$item->name}}</span>
                            </a>
                        </div>
                        @endforeach
                </div>

                <div class="text-right">

                    <a href="/product/add" class="button-plain">ÜRÜN EKLE</a>
                </div>
            </div>

            <div class="image-gallery-holder">
                <h2 class="gallery-title">PROJELER</h2>
                <div class="image-gallery one-row-slider">

                    @foreach($company->projects as $project)

                        <div class="gallery-item" style="height: 141px;">
                            <a href="/project_detail/{{$project->slug}}" class="item">
                            <img src="{{asset($project->image)}}" alt="image" width="188" height="141">
                                <span class="text">{{$project->name}}</span>
                            </a>
                        </div>
                        @endforeach
                </div>
                <div class="text-right">
                    <a href="/project/add" class="button-plain">REFERANS PROJE EKLE</a>
                </div>
            </div>
        </div>
</div>
@endsection