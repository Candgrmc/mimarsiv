@extends('layouts.app')
<div id="wrapper" class="common-page proje-sayfasi"><br>
<header id="header">
    @include("layouts.top")
    @include("layouts.menu")
</header>
@section('content')
    <div class="container">
        <div class="row">
            @if($project->investor_id)
            <div class="col-sm-5 col-md-4">
                <div class="sidebar-box margin-bottom-20">

                        <div class="icon-box">
                            <img src="{{$project->investors->company->logo}}" alt="client" width="130" height="87">
                            <span class="text">YATIRIMCI</span>
                        </div>


                    <p>{{$project->investors->company->name}}</p>
                    <address>
                        Telefon :{{$project->investors->company->phone}} <br>
                        Adres :{{$project->investors->company->address}} <br>
                        Web :{{$project->investors->company->url}}
                    </address>

                    <div class="icon-box">
                        <img src="{{$project->company->logo}}" alt="client" width="116" height="10">
                        <span class="text">MİMARİ OFİS</span>
                    </div>
                    <p>{{$project->company->name}}</p>
                    <address>
                        Telefon :{{$project->company->phone}} <br>
                        Adres :{{$project->company->address}} <br>
                        Web :{{$project->company->url}}
                    </address>
                </div>
            </div>
            @endif
            <div class="col-sm-7 col-md-8">
                @if($project->investor_id)
                <h1 class="top-title">{{$project->investors->company->name}}</h1>
                @endif

                <div class="linked-gallery-head">
                    @foreach($gallery as $item)
                    <div class="image-item">
                        <img src="{{asset($item->image)}}" width="100%" height="100%" alt="gallery-item">
                    </div>
                    @endforeach()
                </div>

                <div class="linked-gallery-pager">
                    @foreach($gallery as $item)
                    <div class="pager-item">
                        <img src="{{asset($item->image)}}" height="93" width="114" alt="pager item">
                    </div>
                    @endforeach()
                </div>

                <h2 class="article-title"><span>Proje Hikayesi</span></h2>
                <article class="article padding-bottom-20">{!! $project->story !!}</article>
                <!--<h3 class="article-h3">PROJEDE KULLANILAN ÜRÜNLER</h3>

                <div class="image-gallery gallery-type-2 padding-bottom-15">
                    <div class="gallery-item">
                        @if($projectProducts)
                        @foreach($projectProducts as $item2)
                           @if($item2->product)
                        <a href="#" class="item" style="height: 141px;">
                            <img src="{{$item2->product->image}}" alt="image" width="188" height="141">
                            <span class="head">{{$item2->product->name}}</span>
                            <span class="text">
                                    {{$item2->product->brand->name}} <br>
                                </span>
                        </a>
                                @endif()
                        @endforeach()
                            @endif()
                    </div>
                </div> -->
                @if($consultants->count())
                <h3 class="article-h3">PROJEDE ÇALIŞILAN DANIŞMANLAR</h3>
                <div class="image-gallery gallery-type-2">
                    <div class="gallery-item">
                        @foreach($consultants as $consultant)
                            <a href="#" class="item" style="height: 141px;">
                                <img src="{{$consultant->consultant->user->image}}" alt="image" width="188" height="141">
                                <span class="head">{{$consultant->consultant->user->name}}</span>
                                <span class="text">
                                   {{$consultant->consultant->company->name}}<br>
                                </span>
                            </a>
                            @endforeach

                    </div>

                </div>
               @endif
            </div>
        </div>
    </div>
    </div>
@endsection