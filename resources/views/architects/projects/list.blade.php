@extends('layouts.app')
<header id="header">
    @include("layouts.top")
    @include("layouts.menu")
</header>
@section('content')
    <main id="main">
        <div class="main-banner">
            <div class="banner-carousel">
                <div class="mask">
                    <div class="slideset">
                        @foreach($sliders as $slider)
                            <div class="slide">
                                <img src="{{$slider->image}}" alt="banner image">
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="pagination"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    <div class="breadcrumb-area">
                        <ol class="breadcrumb">
                            <li><a href="/">ANASAYFA</a></li>
                            <li><a href="#">PROJELER</a></li>

                        </ol>
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="search-filter-area">
                        <div class="select-inputs-wrap">
                            <div class="select-input size1">
                                <select>
                                    <option>MİMARİ OFİS</option>
                                    <option>MİMARİ OFİS 1</option>
                                    <option>MİMARİ OFİS 2</option>
                                    <option>MİMARİ OFİS 3</option>
                                </select>
                            </div>
                            <div class="select-input size2">
                                <select>
                                    <option>TİP</option>
                                    <option>TİP 1</option>
                                    <option>TİP 2</option>
                                    <option>TİP 3</option>
                                </select>
                            </div>
                            <div class="select-input size2">
                                <select>
                                    <option>YIL</option>
                                    <option>YIL 1</option>
                                    <option>YIL 2</option>
                                    <option>YIL 3</option>
                                </select>
                            </div>

                            <div class="select-input size1">
                                <select>
                                    <option>DANIŞMAN</option>
                                    <option>DANIŞMAN 1</option>
                                    <option>DANIŞMAN 2</option>
                                    <option>DANIŞMAN 3</option>
                                </select>
                            </div>
                            <div class="select-input size1">
                                <select>
                                    <option>DANIŞMAN</option>
                                    <option>DANIŞMAN 1</option>
                                    <option>DANIŞMAN 2</option>
                                    <option>DANIŞMAN 3</option>
                                </select>
                            </div>
                            <div class="select-input size1">
                                <select>
                                    <option>ÜLKE</option>
                                    <option>ÜLKE 1</option>
                                    <option>ÜLKE 2</option>
                                    <option>ÜLKE 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="search-result-info">
            <div class="container">
                <div class="results-info">
                    <p>Results: 24</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <ul class="sidebar-nav">
                        @foreach($categories as $category)
                        <li><a href="project_category/{{$category->slug}}">{{$category->title}}</a></li>
@endforeach
                    </ul>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="image-gallery gallery-type-4">
                        @foreach($projects as $project)
                        <div class="gallery-item" style="width: 248px; height: 188px;">
                            <a href="project_detail/{{$project->slug}}" class="item">
                                <img src="{{$project->image}}" alt="image" width="248" height="188">
                                <span class="head">{{$project->name}}</span>
                                <span class="text">
                                    {{$project->company->name}} <br>
                                    {{$project->category->title}} <br>
                                </span>
                            </a>
                        </div>
@endforeach

                </div>
            </div>
        </div>
    </main>
@endsection