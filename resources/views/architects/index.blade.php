@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            @include('layouts.profileTop')
            @include('architects.buttons')


            <div class="image-gallery-holder" >
                <h2 class="gallery-title">SEÇTİĞİM ÜRÜNLER</h2>
                <div class="image-gallery one-row-slider" >

                    @foreach(request()->user()->selectedProducts as $item)

                    <div class="gallery-item" style="height: 300px;">

                        <a href="/product/{{$item->slug}}" class="item" style="height: 155px!important; ">
                            <img src="{{$item->image}}" alt="image" class="img">
                            <span class="text">{{$item->name}}</span>
                        </a>
                        <form class="" action="{{url('update_product_pivot')}}" method="post">
                          {!! csrf_field() !!}
                          <input type="hidden" name="product_id" value="{{$item->id}}">
                          <textarea  rows="3" class="col-md-12" placeholder="NOT YAZ:" style="margin-top:1px" name="product_note">{{$item->pivot->note}}</textarea>
                          <button type="submit" class="pull-left btn-sm btn" style="border-radius:0;background-color:#c2b59b;color:#fff">Send</button>
                        </form>
                        <a href="{{url('unlike_product/'.$item->id)}}" style="color:#d86464;font-size:18px;" class="pull-right"><i class="fa fa-thumbs-down"></i></a> <br>


                    </div>
                    @endforeach
                </div>



            </div>

            <div class="image-gallery-holder" >
                <h2 class="gallery-title">BEĞENDİĞİM PROJELER</h2>
                <div class="image-gallery one-row-slider" >

                    @foreach(request()->user()->likedProjects as $item)

                    <div class="gallery-item" style="height: 300px;" >

                        <a href="/project_detail/{{$item->slug}}" class="item" style="height: 155px; ">
                            <img src="{{$item->image}}" alt="image" class="img">
                            <span class="text">{{$item->name}}</span>
                        </a>

                          <form class="" action="{{url('update_project_pivot')}}" method="post">
                              {!! csrf_field() !!}

                            <input type="hidden" name="project_id" value="{{$item->id}}">
                            <textarea  rows="3" class="col-md-12" placeholder="NOT YAZ:" style="margin-top:1px" name="project_note">{{$item->pivot->note}}</textarea>
                            <button type="submit" class="pull-left btn-sm btn" style="border-radius:0;background-color:#c2b59b;color:#fff">Send</button>
                          </form>

                          <a href="{{url('unlike_project/'.$item->id)}}" style="color:#d86464;font-size:18px;" class="pull-right"><i class="fa fa-thumbs-down"></i></a> <br>




                    </div>
                    @endforeach
                </div>



            </div>
            <div class="image-gallery-holder" >
                <h2 class="gallery-title">KATILDIĞIN ETKİNLİKLER</h2>
                <div class="image-gallery one-row-slider" >

                    @foreach(request()->user()->likedProjects as $item)

                        <div class="gallery-item" style="height: 300px;">

                            <a href="/product/{{$item->slug}}" class="item" style="height: 155px!important; ">
                                <img src="{{$item->image}}" alt="image" class="img">
                                <span class="text">{{$item->name}}</span>
                            </a>



                        </div>
                    @endforeach
                </div>



            </div>


        </div>
</div>
@endsection
