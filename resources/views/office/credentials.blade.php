@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
@section('content')
        <div class="container">
            @include('layouts.profileTop')
            @include('office.buttons')
            <form action="{{url('update_credentials')}}" method="post" >
                {{csrf_field()}}
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" value="{{request()->user()->email}}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="name">{{trans('main.name')}}</label>
                        <input type="text" class="form-control" name="name" value="{{request()->user()->name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="office_name">{{trans('main.office_name')}}</label>
                        <input type="text" name="office_name" class="form-control" value="@isset($office){{$office->name}} @endisset" disabled>
                    </div>
                    <div class="form-group">
                        <label for="address">{{trans('main.office_address')}}</label>
                        <input type="text" name="address" class="form-control" value="@isset($office){{$office->address}} @endisset" disabled>
                    </div>
                    <div class="form-group">
                        <label for="phone">{{trans('main.phone')}}</label>
                        <input type="phone" name="phone" class="form-control" value="{{$office->phone}}">
                    </div>
                    <div class="form-group">
                        <label for="url">Website</label>
                        <input type="url" name="url" class="form-control" value="{{$office->url}}">
                    </div>
                    <div class="form-group">
                        <label for="password">{{trans('main.password')}}</label>
                        <input type="password" name="password" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="password_repeat">{{trans('main.re_password')}}</label>
                        <input type="password" name="password_repeat" class="form-control" >
                    </div>

                    <button type="submit" class="button-green" style="margin-bottom:20px">Güncelle</button>
                </div>


            </form>
        </div>
</div>
@endsection
