@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <style>
        .listItem a{
            text-decoration: none;
            color: #000;

        }
        .listItem{
            list-style: none;
            border:none !important;
            display: inline-block;
            margin : 5px;
        }
        .listItem a:hover{
            text-decoration: none;
            color:silver;

        }
        .selectedItem{

            box-shadow: 2px 2px 2px 2px #e70700;

        }
        .listProduct{
            list-style: none;
            border:none !important;
        }
    </style>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
                @include('layouts.profileTop')
                @include('office.buttons')

            <ul class="default-accordion">
                <form action="{{ URL::to('office/project/store') }}" method="post" enctype="multipart/form-data" class="simple-form">
                    {{csrf_field()}}
                    <li class="active">
                        <a href="#" class="opener">GENEL BİLGİLER</a>
                        <div class="slide-box">

                            <div class="form-group size3">
                                <div class="select-input">
                                    <label for="form-element01">Proje Tipi :</label>
                                    <select class="large straight form-control" name="category">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <br>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group size2">
                                <label for="form-element01">Proje ADI :</label>
                                <input type="text" id="form-element01" name="title">
                            </div>
                            <div class="form-group size5">
                                <label for="form-element02">Proje KÜNYESİ :</label>
                                <textarea class="form-control" rows="5" name="tag"></textarea>
                            </div>


                            <div class="form-group size5">
                                <label for="form-element02">Proje HİKAYESİ :</label>
                                <textarea class="form-control" rows="5" name="story"></textarea>
                            </div>

                            <div class="form-group size5">
                                <label for="form-element02">Proje YILI :</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            <select name="year" id="">
                                                <option value="2000">2000</option>
                                                <option value="2001">2001</option>
                                                <option value="2002">2002</option>
                                                <option value="2003">2003</option>
                                                <option value="2004">2004</option>
                                                <option value="2005">2005</option>
                                                <option value="2006">2006</option>
                                                <option value="2007">2007</option>
                                                <option value="2008">2008</option>
                                                <option value="2009">2009</option>
                                                <option value="2010">2010</option>
                                                <option value="2011">2011</option>
                                                <option value="2012">2012</option>
                                                <option value="2013">2013</option>
                                                <option value="2014">2014</option>
                                                <option value="2015">2015</option>
                                                <option value="2016">2016</option>
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                @php
                                $countries = DB::table('countries')->get();
                                @endphp
                            </div>
                            <div class="form-group size5">
                                <label for="form-element02">Proje Konumu :</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            <select class="" name="country">
                                                <option value="">Seçiniz..</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country->country}}">{{$country->country}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            @php
                            $prices = DB::table('project_settings')->get();
                            @endphp
                            <div class="form-group size5">
                                <label for="form-element02">Proje Fiyat Aralığı :</label> <br>
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <select name="price" id="" class="form-control">
                                            <option value="">Seçiniz</option>
                                            @foreach($prices as $price)
                                                <option value="{{$price->price}}">{{$price->price}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </li>
                    <!--<li>
                        <a href="#" class="opener">Marka</a>
                        <div class="slide-box">
                            <input type="text" placeholder="Marka Ara.." id="brandSearch">  <button type="button" class="button-plain" onclick="$('#new_brand').slideToggle('fast')">Yeni Ekle</button>
                            <ul id="brands_list" style="border:none">

                            </ul>
                        </div>
                        <div id="new_brand" style="margin-bottom:5px; display:none">
                            <input type="hidden" name="brand_id">
                            <input type="text" placeholder="Marka Adı.." name="brand_name">
                            <input type="text" placeholder="Web Sitesi.." name="brand_site">
                            <label for="brand_img">Resim</label>
                            <input type="file" name="brand_img">

                        </div>

                    </li> -->
                    <li>
                        <a href="#" class="opener">Ürünler</a>
                        <div class="slide-box">
                            <input type="text" name="products" placeholder="Ürün Ara.." id="productSearch">
                            <ul id="products_list" style="border:none">

                            </ul>
                        </div>


                    </li>

                    <li>
                        <a href="javascript:void(0)" class="opener">Resimler</a>
                        <div class="slide-box">
                            <input type="file" name="image[]" multiple="multiple" required="required">
                            <span class="help-block">Birden fazla resim seçip ürün galerisi oluşturabilirsiniz</span>
                        </div>
                    </li>
                    {{-- <li>
                         <a href="#" class="opener">Sertifikalar</a>
                         <div class="slide-box">
                             <select multiple="multiple" name="certificates[]" id="certs" class="">
                             <div class="col-sm-5">
                                 @foreach($certificates as $certificate)
                                     <option value="{{$certificate->id}}">{{$certificate->name}}</option>
                                 @endforeach
                             </div>
                             </select>
                              </div>
                     </li>--}}

                    <br>
                    <input type="submit" class="button-plain large" value="Gönder">
                </form>
            </ul>
        </div>
</div>
<script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
<script>
    var brands = []
    var products = []

    @foreach($brands as $brand)
    brands.push($.parseJSON('{!! $brand !!}'))

    @endforeach
    @foreach($products as $product)
    products.push('{{$product->name}}-{{$product->id}}')

    @endforeach
            console.log(products)

    $('#brandSearch').on('keyup',function(){
        var value = $(this).val();
        $('#brands_list').html('')
        count = 0;
        brands.forEach(function(item){
            if(value.length && item.name.toLowerCase().indexOf(value.toLowerCase()) != -1 && count <5){

                $('#brands_list').append('<li class="listItem"><a href="javascript:void(0)" onclick="selectItem($(this))" data-id="'+item.id+'" data-site="'+item.website+'"><img src="'+item.image+'" height="70" class="img-rounded"> </a></li>')

                count++
            }
        })
    })
    $( "#productSearch" ).autocomplete({
        source: function( request, response ) {
            response( $.ui.autocomplete.filter(
                products, extractLast( request.term ) ) );
        },
        select: function( event, ui ) {
            var terms = split( this.value );
            terms.pop();
            terms.push( ui.item.value );
            terms.push( "" );
            this.value = terms.join( ", " );
            return false;
        }
    });
    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }


    function selectItem(el){
        if(el.attr('data-id') != $('input[name="brand_id"]').val()){
            $('input[name="brand_id"]').val(el.attr('data-id'))
            $('.selectedItem').removeClass('selectedItem')
            el.children('img').addClass('selectedItem')
        }else{
            $('input[name="brand_id"]').val('')
            $('.selectedItem').removeClass('selectedItem')
        }

    }
</script>
@endsection
