@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container" style="min-height: 600px">
            <div class="top-normal-banner">
                <div class="icon-box">
                    <img src="{{($office->logo) ? $office->logo:'https://www.breakthroughforlife.com/wp-content/uploads/2017/02/Life-Coach-Insights-Life-Architect.jpg'}}" height="47" width="94" alt="{{$office->name}}">
                </div>
                <img src="{{($office->cover) ? $office->cover : 'https://media.architecturaldigest.com/photos/59b04f5ddd88d32f9bd9c633/master/pass/AJN_HW_Abu_Dhabi_Louvre_04%20%C2%A9%20TDIC,%20Architect%20Ateliers%20Jean%20Nouvel.jpg'}}" height="960" width="1920" alt="{{$office->name}}">
            </div>


            <div class="image-gallery-holder">
                <h2 class="gallery-title">Ürünler</h2>
                <div class="image-gallery one-row-slider">
                    @if($user->products)
                        @foreach($user->products as $item)
                            <div class="gallery-item" style="height: 141px;">
                                <a href="/product/{{$item->slug}}" class="item">
                                    <img src="{{asset($item->image)}}" alt="image" width="188" height="141">
                                    <span class="text">{{$item->name}}</span>
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>


            </div>


        </div>
</div>
@endsection