@extends('layouts.app')
<div id="wrapper" class="common-page firma-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container" style="min-height: 600px">

            @include('layouts.profileTop')
            @include('office.buttons')

            <div class="image-gallery-holder">
                <h2 class="gallery-title">Projeler</h2>
                <div class="image-gallery one-row-slider">
                   @if(request()->user()->products)
                        @foreach(request()->user()->projects as $item)
                            <div class="gallery-item" style="height: 141px;">
                                <a href="/project_detail/{{$item->slug}}" class="item">
                                    <img src="{{asset($item->image)}}" alt="image" width="188" height="141">
                                    <span class="text">{{$item->name}}</span>
                                </a>
                            </div>
                        @endforeach
                   @endif
                </div>

                    <div class="text-right">

                        <a href="/office/project/add" class="button-plain">Proje EKLE</a>
                    </div>

            </div>


        </div>
</div>
@endsection