
@extends('layouts.app')
<div id="wrapper" class="common-page haber-detay-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="row main-row">

                <div class="col-sm-12 col-xs-12 col-md-12" style="height: 343px; ">
                    <div style="height: 343px; background-size: cover;">
                    <img src="{{$news->image}}" height="343" class="col-md-12" alt="banner" >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <article class="article">
                    {!! $news->content !!}
                    </article>
                    <h3 class="article-h3 padding-bottom-5">DİĞER HABERLER</h3>
                    <div class="image-gallery gallery-type-3 col-md-12 col-xs-12 col-sm-12">
                        @foreach($otherNews as $item)
                        <div class="gallery-item" style="height:240px">
                            <a href="/news/{{$item->slug}}" class="item col-md-3 col-xs-3 col-sm-3" style="object-fit: cover;">
                                <img src="{{asset($item->main_image)}}" alt="image" >
                                <span class="head">{{$item->title}}</span>

                            </a>
                        </div>
@endforeach
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
