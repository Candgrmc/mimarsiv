
@extends('layouts.app')
<div id="wrapper" class="common-page roportaj-sayfasi">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-md-4">
                    @if($user && $office)
                    <div class="sidebar-box margin-bottom-20">
                        <div class="icon-box">
                            <img src="@if($user->image){{asset($user->image)}} @else {{asset('images/default.jpeg')}} @endif" alt="client" width="122" height="46">
                        </div>
                        <p>{{$office->name}}</p>
                        <address>
                            <strong>Telefon : </strong>{{$office->phone}} <br>
                            <strong>Adres : </strong>{{$office->address}} <br>
                            <strong>Web : </strong>{{$office->url}}
                        </address>
                    </div>
                    @endif

                        @if($projects)

                        <div class="sidebar-box">
                        <h2 class="sidebar-title">REFERENCE PROJECTS</h2>
                        <div class="image-gallery gallery-type-5 padding-bottom-15">
                            @foreach($projects as $project)
                            <div class="gallery-item" style="height: 200px;width:200px ">
                                <a href="/project_detail/{{$project->slug}}" class="item" style="object-fit:contain">
                                    <img src="{{asset($project->image)}}" alt="image" height="141" >
                                    <span class="head">{{$project->name}}</span>
                                    @if($project->company)
                                    <span class="text">
                                    {{$project->company->name}} <br>
                                        {{$project->category->title}} <br>
                                </span>
                                        @endif()
                                </a>
                            </div>
                                @endforeach
                        </div>
                    </div>
                            @endif()
                </div>
                <div class="col-sm-7 col-md-8">
                    <div class="top-normal-banner margin-bottom-5">
                        <img src="{{$interview->image}}" alt="image banner" width="770" height="399">
                    </div>
                    <article class="article padding-bottom-30">
                     {!! $interview->content !!}
                    </article>
                    <h3 class="article-h3">DİĞER RÖPORTAJLAR</h3>

                    <div class="image-gallery gallery-type-2">
                        @foreach($interviews as $item)
                        <div class="gallery-item col-md-3 col-sm-3 col-xs-3" style="height:200px">
                            <a href="/interviews/{{$item->slug}}" class="item" style="object-fit:contain">
                                <img src="{{asset($item->image)}}" alt="image" width="188" height="141">
                                <span class="head">{{$item->title}}</span>
                                <span class="text">

                                </span>
                            </a>
                        </div>
                       @endforeach
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
