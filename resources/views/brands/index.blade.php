
@extends('layouts.app')
<div id="wrapper" class="common-page proje-listeleme">
    <header id="header">
        @include("layouts.top")
        @include("layouts.menu")
    </header>
    @section('content')
    <div class="main-banner">
        <div class="banner-carousel">
            <div class="mask">
                <div class="slideset">
                    @foreach($sliders as $slider)
                    <div class="slide">
                        <a href="@if($slider->redirectTo){{$slider->redirectTo}}@else javascript:void(0) @endif"><img src="{{asset($slider->image)}}" alt="banner image"></a>
                    </div>
                    @endforeach

                </div>
            </div>
            <div class="pagination"></div>
        </div>
    </div>
        <div class="search-result-info">
            <div class="container">
              <div class="row">
                <div class="col-md-3">
                    <div class="breadcrumb-area">
                        <ol class="breadcrumb">
                            <li><a href="/">ANASAYFA</a></li>
                            <li><a href="#">MARKALAR</a></li>

                        </ol>
                    </div>
                </div>
              </div>
                <div class="results-info">
                    <p>Results: {{count($brands)}}</p>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="image-gallery gallery-type-4 col-md-12">
                        @foreach($brands as $brand)
                        <div class="gallery-item col-md-3 col-sm-3 col-xs-3" style="height: 188px;">
                            <a href="/products/{{$brand->slug}}" class="item" style="background-size: contain ; background-repeat: no-repeat;">
                                <img src="{{asset($brand->image)}}" alt="image"  style="max-height:100%; object-fit: contain !important">
                                <span class="head">{{$brand->name}}</span>

                            </a>
                        </div>
                            @endforeach()
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
